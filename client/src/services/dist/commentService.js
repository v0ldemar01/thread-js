// import callWebApi from '../helpers/webApiHelper';
// export const getComments = async (postId: string) => {
//   const response = await callWebApi({
//     endpoint: '/api/comments/',
//     type: 'GET',
//     query: { postId }
//   });
//   return response.json();
// };
// export const addComment = async request => {
//   const response = await callWebApi({
//     endpoint: '/api/comments',
//     type: 'POST',
//     request
//   });
//   return response.json();
// };
// export const getComment = async (id: string) => {
//   const response = await callWebApi({
//     endpoint: `/api/comments/${id}`,
//     type: 'GET'
//   });
//   return response.json();
// };
// export const editComment = async (id: string, request) => {
//   const response = await callWebApi({
//     endpoint: `/api/comments/${id}`,
//     type: 'PUT',
//     request
//   });
//   return response.json();
// };
// export const likeComment = async commentId => {
//   const response = await callWebApi({
//     endpoint: '/api/comments/react',
//     type: 'PUT',
//     request: {
//       commentId,
//       isLike: true
//     }
//   });
//   return response.json();
// };
// export const dislikeComment = async commentId => {
//   const response = await callWebApi({
//     endpoint: '/api/comments/react',
//     type: 'PUT',
//     request: {
//       commentId,
//       isDisLike: true
//     }
//   });
//   return response.json();
// };
// export const getReactionUsers = async (id, isLike) => {
//   const response = await callWebApi({
//     endpoint: `/api/comments/info/${id}`,
//     type: 'GET',
//     query: { isLike }
//   });
//   return response.json();
// };
