// import callWebApi from '../helpers/webApiHelper';
// export const getAllPosts = async filter => {
//   const response = await callWebApi({
//     endpoint: '/api/posts',
//     type: 'GET',
//     query: filter
//   });
//   return response.json();
// };
// export const addPost = async request => {
//   const response = await callWebApi({
//     endpoint: '/api/posts',
//     type: 'POST',
//     request
//   });
//   return response.json();
// };
// export const editPost = async (id, request) => {
//   const response = await callWebApi({
//     endpoint: `/api/posts/${id}`,
//     type: 'PUT',
//     request
//   });
//   return response.json();
// };
// export const getPost = async (id: string) => {
//   const response = await callWebApi({
//     endpoint: `/api/posts/${id}`,
//     type: 'GET'
//   });
//   return response.json();
// };
// export const likePost = async (postId: string) => {
//   const response = await callWebApi({
//     endpoint: '/api/posts/react',
//     type: 'PUT',
//     request: {
//       postId,
//       isLike: true
//     }
//   });
//   return response.json();
// };
// export const dislikePost = async (postId: string) => {
//   const response = await callWebApi({
//     endpoint: '/api/posts/react',
//     type: 'PUT',
//     request: {
//       postId,
//       isDisLike: true
//     }
//   });
//   return response.json();
// };
// export const getReactionUsers = async (id: string, isLike: boolean) => {
//   const response = await callWebApi({
//     endpoint: `/api/posts/info/${id}`,
//     type: 'GET',
//     query: { isLike }
//   });
//   return response.json();
// };
// // should be replaced by approppriate function
// export const getPostByHash = async (hash: string) => getPost(hash);
