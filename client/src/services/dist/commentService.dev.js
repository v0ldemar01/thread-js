"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getReactionUsers = exports.dislikeComment = exports.likeComment = exports.editComment = exports.getComment = exports.addComment = exports.getComments = void 0;

var _src = _interopRequireDefault(require("src"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var getComments = function getComments(postId) {
  var response;
  return regeneratorRuntime.async(function getComments$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/comments/',
            type: 'GET',
            query: {
              postId: postId
            }
          }));

        case 2:
          response = _context.sent;
          return _context.abrupt("return", response.json());

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.getComments = getComments;

var addComment = function addComment(request) {
  var response;
  return regeneratorRuntime.async(function addComment$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/comments',
            type: 'POST',
            request: request
          }));

        case 2:
          response = _context2.sent;
          return _context2.abrupt("return", response.json());

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.addComment = addComment;

var getComment = function getComment(id) {
  var response;
  return regeneratorRuntime.async(function getComment$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/comments/".concat(id),
            type: 'GET'
          }));

        case 2:
          response = _context3.sent;
          return _context3.abrupt("return", response.json());

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
};

exports.getComment = getComment;

var editComment = function editComment(id, request) {
  var response;
  return regeneratorRuntime.async(function editComment$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/comments/".concat(id),
            type: 'PUT',
            request: request
          }));

        case 2:
          response = _context4.sent;
          return _context4.abrupt("return", response.json());

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
};

exports.editComment = editComment;

var likeComment = function likeComment(commentId) {
  var response;
  return regeneratorRuntime.async(function likeComment$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/comments/react',
            type: 'PUT',
            request: {
              commentId: commentId,
              isLike: true
            }
          }));

        case 2:
          response = _context5.sent;
          return _context5.abrupt("return", response.json());

        case 4:
        case "end":
          return _context5.stop();
      }
    }
  });
};

exports.likeComment = likeComment;

var dislikeComment = function dislikeComment(commentId) {
  var response;
  return regeneratorRuntime.async(function dislikeComment$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/comments/react',
            type: 'PUT',
            request: {
              commentId: commentId,
              isDisLike: true
            }
          }));

        case 2:
          response = _context6.sent;
          return _context6.abrupt("return", response.json());

        case 4:
        case "end":
          return _context6.stop();
      }
    }
  });
};

exports.dislikeComment = dislikeComment;

var getReactionUsers = function getReactionUsers(id, isLike) {
  var response;
  return regeneratorRuntime.async(function getReactionUsers$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/comments/info/".concat(id),
            type: 'GET',
            query: {
              isLike: isLike
            }
          }));

        case 2:
          response = _context7.sent;
          return _context7.abrupt("return", response.json());

        case 4:
        case "end":
          return _context7.stop();
      }
    }
  });
};

exports.getReactionUsers = getReactionUsers;