"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendSharedToEmail = exports.notifyUser = exports.editUser = exports.getUser = void 0;

var _src = _interopRequireDefault(require("src"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* eslint-disable linebreak-style */
var getUser = function getUser(id) {
  var response;
  return regeneratorRuntime.async(function getUser$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/users/".concat(id),
            type: 'GET'
          }));

        case 2:
          response = _context.sent;
          return _context.abrupt("return", response.json());

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.getUser = getUser;

var editUser = function editUser(id, request) {
  var response;
  return regeneratorRuntime.async(function editUser$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/users/".concat(id),
            type: 'PUT',
            request: request
          }));

        case 2:
          response = _context2.sent;
          return _context2.abrupt("return", response.json());

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.editUser = editUser;

var notifyUser = function notifyUser(request) {
  var response;
  return regeneratorRuntime.async(function notifyUser$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/users/email',
            type: 'POST',
            request: request
          }));

        case 2:
          response = _context3.sent;
          return _context3.abrupt("return", response.json());

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
};

exports.notifyUser = notifyUser;

var sendSharedToEmail = function sendSharedToEmail(request) {
  var response;
  return regeneratorRuntime.async(function sendSharedToEmail$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/users/shareEmail',
            type: 'POST',
            request: request
          }));

        case 2:
          response = _context4.sent;
          return _context4.abrupt("return", response.json());

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
};

exports.sendSharedToEmail = sendSharedToEmail;