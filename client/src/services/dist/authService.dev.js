"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.changePassword = exports.sendEmail = exports.resetPassword = exports.getCurrentUser = exports.registration = exports.login = void 0;

var _src = _interopRequireDefault(require("src"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var login = function login(request) {
  var response;
  return regeneratorRuntime.async(function login$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          console.log('request', request);
          _context.next = 3;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/auth/login',
            type: 'POST',
            request: request
          }));

        case 3:
          response = _context.sent;
          return _context.abrupt("return", response.json());

        case 5:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.login = login;

var registration = function registration(request) {
  var response;
  return regeneratorRuntime.async(function registration$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/auth/register',
            type: 'POST',
            request: request
          }));

        case 2:
          response = _context2.sent;
          return _context2.abrupt("return", response.json());

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.registration = registration;

var getCurrentUser = function getCurrentUser() {
  var response;
  return regeneratorRuntime.async(function getCurrentUser$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/auth/user',
            type: 'GET'
          }));

        case 3:
          response = _context3.sent;
          return _context3.abrupt("return", response.json());

        case 7:
          _context3.prev = 7;
          _context3.t0 = _context3["catch"](0);
          return _context3.abrupt("return", null);

        case 10:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.getCurrentUser = getCurrentUser;

var resetPassword = function resetPassword(request) {
  var response;
  return regeneratorRuntime.async(function resetPassword$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/auth/resetPassword',
            type: 'POST',
            request: request
          }));

        case 2:
          response = _context4.sent;
          return _context4.abrupt("return", response.json());

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
};

exports.resetPassword = resetPassword;

var sendEmail = function sendEmail(request) {
  var response;
  return regeneratorRuntime.async(function sendEmail$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/auth/forgotPassword',
            type: 'POST',
            request: request
          }));

        case 2:
          response = _context5.sent;
          return _context5.abrupt("return", response.json());

        case 4:
        case "end":
          return _context5.stop();
      }
    }
  });
};

exports.sendEmail = sendEmail;

var changePassword = function changePassword(token, request) {
  var response;
  return regeneratorRuntime.async(function changePassword$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/auth/changePassword/".concat(token),
            type: 'PUT',
            request: request
          }));

        case 2:
          response = _context6.sent;
          return _context6.abrupt("return", response.json());

        case 4:
        case "end":
          return _context6.stop();
      }
    }
  });
};

exports.changePassword = changePassword;