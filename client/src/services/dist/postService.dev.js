"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPostByHash = exports.getReactionUsers = exports.dislikePost = exports.likePost = exports.getPost = exports.editPost = exports.addPost = exports.getAllPosts = void 0;

var _src = _interopRequireDefault(require("src"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var getAllPosts = function getAllPosts(filter) {
  var response;
  return regeneratorRuntime.async(function getAllPosts$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/posts',
            type: 'GET',
            query: filter
          }));

        case 2:
          response = _context.sent;
          return _context.abrupt("return", response.json());

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.getAllPosts = getAllPosts;

var addPost = function addPost(request) {
  var response;
  return regeneratorRuntime.async(function addPost$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/posts',
            type: 'POST',
            request: request
          }));

        case 2:
          response = _context2.sent;
          return _context2.abrupt("return", response.json());

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.addPost = addPost;

var editPost = function editPost(id, request) {
  var response;
  return regeneratorRuntime.async(function editPost$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/posts/".concat(id),
            type: 'PUT',
            request: request
          }));

        case 2:
          response = _context3.sent;
          return _context3.abrupt("return", response.json());

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
};

exports.editPost = editPost;

var getPost = function getPost(id) {
  var response;
  return regeneratorRuntime.async(function getPost$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/posts/".concat(id),
            type: 'GET'
          }));

        case 2:
          response = _context4.sent;
          return _context4.abrupt("return", response.json());

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
};

exports.getPost = getPost;

var likePost = function likePost(postId) {
  var response;
  return regeneratorRuntime.async(function likePost$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/posts/react',
            type: 'PUT',
            request: {
              postId: postId,
              isLike: true
            }
          }));

        case 2:
          response = _context5.sent;
          return _context5.abrupt("return", response.json());

        case 4:
        case "end":
          return _context5.stop();
      }
    }
  });
};

exports.likePost = likePost;

var dislikePost = function dislikePost(postId) {
  var response;
  return regeneratorRuntime.async(function dislikePost$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: '/api/posts/react',
            type: 'PUT',
            request: {
              postId: postId,
              isDisLike: true
            }
          }));

        case 2:
          response = _context6.sent;
          return _context6.abrupt("return", response.json());

        case 4:
        case "end":
          return _context6.stop();
      }
    }
  });
};

exports.dislikePost = dislikePost;

var getReactionUsers = function getReactionUsers(id, isLike) {
  var response;
  return regeneratorRuntime.async(function getReactionUsers$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return regeneratorRuntime.awrap((0, _src["default"])({
            endpoint: "/api/posts/info/".concat(id),
            type: 'GET',
            query: {
              isLike: isLike
            }
          }));

        case 2:
          response = _context7.sent;
          return _context7.abrupt("return", response.json());

        case 4:
        case "end":
          return _context7.stop();
      }
    }
  });
}; // should be replaced by approppriate function


exports.getReactionUsers = getReactionUsers;

var getPostByHash = function getPostByHash(hash) {
  return regeneratorRuntime.async(function getPostByHash$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          return _context8.abrupt("return", getPost(hash));

        case 1:
        case "end":
          return _context8.stop();
      }
    }
  });
};

exports.getPostByHash = getPostByHash;