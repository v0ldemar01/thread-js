import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps, Redirect } from 'react-router-dom';
import { setExpandedPost } from '../../actions/postActions';

interface RouteParams {
  postHash: string
}

const SharedPost: React.FunctionComponent<RouteComponentProps<RouteParams> & PropsFromRedux> = ({ 
  match, 
  setExpandedPost: setExpanded 
}: RouteComponentProps<RouteParams> & PropsFromRedux) => {  
  useEffect(() => {    
    setExpanded(match.params.postHash);
  });
  return <Redirect to={{ pathname: "/"}} />
  
};

const mapDispatchToProps = { 
  setExpandedPost 
};

const connector = connect(null, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(SharedPost);
