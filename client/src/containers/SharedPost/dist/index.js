"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var react_router_dom_1 = require("react-router-dom");
var postActions_1 = require("../../actions/postActions");
var SharedPost = function (_a) {
    var match = _a.match, setExpanded = _a.setExpandedPost;
    react_1.useEffect(function () {
        setExpanded(match.params.postHash);
    });
    return react_1["default"].createElement(react_router_dom_1.Redirect, { to: { pathname: "/" } });
};
var mapDispatchToProps = {
    setExpandedPost: postActions_1.setExpandedPost
};
var connector = react_redux_1.connect(null, mapDispatchToProps);
exports["default"] = connector(SharedPost);
