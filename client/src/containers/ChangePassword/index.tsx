import React, { useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import Logo from '../../components/Logo';
import IState from '../../models/modelState';
import {changePassword} from '../../actions/userActions';
import { Grid, Header, Input, Button } from 'semantic-ui-react';

interface RouteParams {
  token: string
}

const ChangePassword : React.FunctionComponent<RouteComponentProps<RouteParams> & PropsFromRedux> = ({
  changePassword: change,
  changedStatus,
  match,
  history  
}: RouteComponentProps<RouteParams> & PropsFromRedux) => {
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const token = match.params.token;

  const redirect = () => {
    history.push('/login');
  };

  const handleSendNewPassword = async () => {
    if (
      !password
      || password !== confirmPassword
      || !confirmPassword
    ) {
      return;
    }
    change({token, newUser: {password}});
    setPassword('');
    setConfirmPassword('');
  };
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          <p>
            Update your password
          </p>
        </Header>
        <br />
        {changedStatus ? (
          <Button
            color="red"
            type="submit"
            onClick={redirect}
          >
            Go to Login
          </Button>
        ) : (
          <>
            <Input
              focus
              icon="user secret"
              iconPosition="left"
              placeholder="New password"
              type="password"
              value={password}
              onChange={event => setPassword(event.target.value)}
            />
            <br />
            <br />
            <Input
              icon="user secret"
              iconPosition="left"
              placeholder="Confirm new password"
              type="password"
              value={confirmPassword}
              onChange={event => setConfirmPassword(event.target.value)}
            />
            <br />
            <br />
            <Button
              color="green"
              type="submit"
              onClick={handleSendNewPassword}
            >
              Change password
            </Button>
          </>
        )}
      </Grid.Column>
    </Grid>
  );
};

const mapStateToProps = (rootState: IState) => ({
  changedStatus: rootState.user.changedStatus
});

const mapDispatchToProps = {
  changePassword
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(ChangePassword);
