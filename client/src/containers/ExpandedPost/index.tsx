import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  cancelExpandedPost,   
  sendSharedPost
} from '../../actions/postActions';
import {
  addComment,
  editComment,
  deleteComment,
  likeComment,
  dislikeComment,
  setEditedComment,
  cancelEditedComment,
  showUserComment,
  hideUserComment
} from '../../actions/commentActions';
import Post from '../../components/Post';
import Comment from '../../components/Comment';
import EditComment from '../../components/EditComment';
import AddComment from '../../components/AddComment';
import Spinner from '../../components/Spinner';
import ShowUser from '../ShowUser';
import IState from '../../models/modelState';

const ExpandedPost: React.FunctionComponent<PropsFromRedux>  = ({
  postId,
  posts,
  comments,
  showUsers,
  showUserByReaction,
  userId,
  editedComment,
  sendSharedPost: sharePost,
  likePost: like,
  dislikePost: dislike,
  cancelExpandedPost: cancelExpanded,
  setEditedComment: setEdited,
  cancelEditedComment: cancelEdited,
  addComment: add,
  editComment: changeComment,
  deleteComment: cut,
  likeComment: commentlike,
  dislikeComment: commentdislike,
  showUserComment: showUser,
  hideUserComment: hideUser
}) => {
  const thisPost = posts.find(post => post.id === postId)
  return (
    <>
      <Modal dimmer="blurring" centered={false} open onClose={() => cancelExpanded()}>
        {thisPost
          ? (
            <Modal.Content>
              <Post
                post={thisPost}
                likePost={like}
                dislikePost={dislike}
                toggleExpandedPost={() => {}}
                sharePost={sharePost}
                key={postId}
                userId={userId as string}
                toggleShowUser={() => {}}
                toggleEditedPost={() => {}}
                deletePost={() => {}}
              />
              <CommentUI.Group style={{ maxWidth: '100%' }}>
                <Header as="h3" dividing>
                  Comments
                </Header>
                {comments && comments
                  .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                  .map(comment => (
                    editedComment && editedComment === comment.id
                      ? (
                        <EditComment
                          key={comment.id}
                          comment={comment}
                          editComment={changeComment}
                          toggleEditedComment={cancelEdited}
                        />
                      )
                      : (
                        <Comment
                          key={comment.id}
                          userId={userId as string}
                          comment={comment}
                          toggleEditedComment={setEdited}
                          deleteComment={cut}
                          likeComment={commentlike}
                          dislikeComment={commentdislike}
                          toggleShowUser={showUser}
                        />
                      )
                  ))}
                <AddComment postId={postId as string} addComment={add} />
              </CommentUI.Group>
            </Modal.Content>
          )
          : <Spinner />}
      </Modal>
      {showUserByReaction && <ShowUser users={showUsers} toggleShowUser={hideUser} />}
    </>
  );
} 

const mapStateToProps = (rootState: IState) => ({
  posts: rootState.post.posts,
  postId: rootState.post.expandingPost,
  comments: rootState.comment.comments,
  editedComment: rootState.comment.editingComment,
  userId: rootState.user.currentUser.id,
  showUsers: rootState.user.users,
  showUserByReaction: rootState.post.showUserByReaction,
});

const mapDispatchToProps = {
  likePost,
  dislikePost,
  addComment,
  editComment,
  deleteComment,
  likeComment,
  dislikeComment,
  cancelExpandedPost,
  setEditedComment,
  cancelEditedComment,
  showUserComment,
  hideUserComment,
  sendSharedPost
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(ExpandedPost);
