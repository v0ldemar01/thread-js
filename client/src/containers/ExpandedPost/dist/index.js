"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var semantic_ui_react_1 = require("semantic-ui-react");
var moment_1 = require("moment");
var postActions_1 = require("../../actions/postActions");
var commentActions_1 = require("../../actions/commentActions");
var Post_1 = require("../../components/Post");
var Comment_1 = require("../../components/Comment");
var EditComment_1 = require("../../components/EditComment");
var AddComment_1 = require("../../components/AddComment");
var Spinner_1 = require("../../components/Spinner");
var ShowUser_1 = require("../ShowUser");
var ExpandedPost = function (_a) {
    var postId = _a.postId, posts = _a.posts, comments = _a.comments, showUsers = _a.showUsers, showUserByReaction = _a.showUserByReaction, userId = _a.userId, editedComment = _a.editedComment, sharePost = _a.sendSharedPost, like = _a.likePost, dislike = _a.dislikePost, cancelExpanded = _a.cancelExpandedPost, setEdited = _a.setEditedComment, cancelEdited = _a.cancelEditedComment, add = _a.addComment, changeComment = _a.editComment, cut = _a.deleteComment, commentlike = _a.likeComment, commentdislike = _a.dislikeComment, showUser = _a.showUserComment, hideUser = _a.hideUserComment;
    var thisPost = posts.find(function (post) { return post.id === postId; });
    return (react_1["default"].createElement(react_1["default"].Fragment, null,
        react_1["default"].createElement(semantic_ui_react_1.Modal, { dimmer: "blurring", centered: false, open: true, onClose: function () { return cancelExpanded(); } }, thisPost
            ? (react_1["default"].createElement(semantic_ui_react_1.Modal.Content, null,
                react_1["default"].createElement(Post_1["default"], { post: thisPost, likePost: like, dislikePost: dislike, toggleExpandedPost: function () { }, sharePost: sharePost, key: postId, userId: userId, toggleShowUser: function () { }, toggleEditedPost: function () { }, deletePost: function () { } }),
                react_1["default"].createElement(semantic_ui_react_1.Comment.Group, { style: { maxWidth: '100%' } },
                    react_1["default"].createElement(semantic_ui_react_1.Header, { as: "h3", dividing: true }, "Comments"),
                    comments && comments
                        .sort(function (c1, c2) { return moment_1["default"](c1.createdAt).diff(c2.createdAt); })
                        .map(function (comment) { return (editedComment && editedComment === comment.id
                        ? (react_1["default"].createElement(EditComment_1["default"], { key: comment.id, comment: comment, editComment: changeComment, toggleEditedComment: cancelEdited }))
                        : (react_1["default"].createElement(Comment_1["default"], { key: comment.id, userId: userId, comment: comment, toggleEditedComment: setEdited, deleteComment: cut, likeComment: commentlike, dislikeComment: commentdislike, toggleShowUser: showUser }))); }),
                    react_1["default"].createElement(AddComment_1["default"], { postId: postId, addComment: add }))))
            : react_1["default"].createElement(Spinner_1["default"], null)),
        showUserByReaction && react_1["default"].createElement(ShowUser_1["default"], { users: showUsers, toggleShowUser: hideUser })));
};
var mapStateToProps = function (rootState) { return ({
    posts: rootState.post.posts,
    postId: rootState.post.expandingPost,
    comments: rootState.comment.comments,
    editedComment: rootState.comment.editingComment,
    userId: rootState.user.currentUser.id,
    showUsers: rootState.user.users,
    showUserByReaction: rootState.post.showUserByReaction
}); };
var mapDispatchToProps = {
    likePost: postActions_1.likePost,
    dislikePost: postActions_1.dislikePost,
    addComment: commentActions_1.addComment,
    editComment: commentActions_1.editComment,
    deleteComment: commentActions_1.deleteComment,
    likeComment: commentActions_1.likeComment,
    dislikeComment: commentActions_1.dislikeComment,
    cancelExpandedPost: postActions_1.cancelExpandedPost,
    setEditedComment: commentActions_1.setEditedComment,
    cancelEditedComment: commentActions_1.cancelEditedComment,
    showUserComment: commentActions_1.showUserComment,
    hideUserComment: commentActions_1.hideUserComment,
    sendSharedPost: postActions_1.sendSharedPost
};
var connector = react_redux_1.connect(mapStateToProps, mapDispatchToProps);
exports["default"] = connector(ExpandedPost);
