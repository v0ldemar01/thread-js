import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import IRouteProps from '../../models/modelProps/routeProps';
import IState from '../../models/modelState';

const PublicRoute: React.FunctionComponent<IRouteProps & PropsFromRedux> = ({ 
  component: Component, 
  isAuthorized, 
  ...rest 
}: IRouteProps & PropsFromRedux) => (
  <Route
    {...rest}
    render={props => (isAuthorized
      ? <Redirect to={{ pathname: '/', state: { from: props.location } }} />
      : <Component {...props} />)}
  />
);

const mapStateToProps = (rootState: IState) => ({
  isAuthorized: rootState.user.isLoggedIn
});

const connector = connect(mapStateToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(PublicRoute);
