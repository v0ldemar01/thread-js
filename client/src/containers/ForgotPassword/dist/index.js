"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var Logo_1 = require("../../components/Logo");
var semantic_ui_react_1 = require("semantic-ui-react");
var userActions_1 = require("../../actions/userActions");
var ForgotPassword = function (_a) {
    var reset = _a.resetPassword;
    var _b = react_1.useState(''), email = _b[0], setEmail = _b[1];
    var handleSendEmail = function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            if (!email) {
                return [2 /*return*/];
            }
            reset({ email: email });
            setEmail('');
            return [2 /*return*/];
        });
    }); };
    return (react_1["default"].createElement(semantic_ui_react_1.Grid, { textAlign: "center", verticalAlign: "middle", className: "fill" },
        react_1["default"].createElement(semantic_ui_react_1.Grid.Column, { style: { maxWidth: 450 } },
            react_1["default"].createElement(Logo_1["default"], null),
            react_1["default"].createElement(semantic_ui_react_1.Header, { as: "h2", color: "teal", textAlign: "center" },
                react_1["default"].createElement("p", null, "Forgot the password")),
            react_1["default"].createElement("h3", null, "Enter your email address below"),
            react_1["default"].createElement("br", null),
            react_1["default"].createElement(semantic_ui_react_1.Input, { icon: "unlock", iconPosition: "left", placeholder: "Enter email...", type: "text", value: email, onChange: function (event) { return setEmail(event.target.value); } }),
            react_1["default"].createElement(semantic_ui_react_1.Button, { color: "green", type: "submit", onClick: handleSendEmail }, "Send email"))));
};
var mapDispatchToProps = {
    resetPassword: userActions_1.resetPassword
};
var connector = react_redux_1.connect(null, mapDispatchToProps);
exports["default"] = connector(ForgotPassword);
