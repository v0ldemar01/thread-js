import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import Logo from '../../components/Logo';
import { Grid, Header, Input, Button } from 'semantic-ui-react';
import { resetPassword } from '../../actions/userActions';

const ForgotPassword: React.FunctionComponent<PropsFromRedux> = ({ 
  resetPassword: reset 
}: PropsFromRedux) => {
  const [email, setEmail] = useState('');
  const handleSendEmail = async () => {
    if (!email) {
      return;
    }    
    reset({email});
    setEmail('');
  };
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          <p>
            Forgot the password
          </p>
        </Header>
        <h3>Enter your email address below</h3>
        <br />
        <Input
          icon="unlock"
          iconPosition="left"
          placeholder="Enter email..."
          type="text"
          value={email}
          onChange={event => setEmail(event.target.value)}
        />
        <Button
          color="green"
          type="submit"
          onClick={handleSendEmail}
        >
          Send email
        </Button>
      </Grid.Column>
    </Grid>
  );
};

const mapDispatchToProps = {
  resetPassword
};

const connector = connect(null, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(ForgotPassword);
