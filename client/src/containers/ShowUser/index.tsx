import React from 'react';
import Spinner from '../../components/Spinner';
import IShowUserProps from '../../models/modelProps/showUserProps';
import { getUserImgLink } from '../../helpers/imageHelper';
import { Modal, Image } from 'semantic-ui-react';

const ShowUser: React.FunctionComponent<IShowUserProps> = ({
  toggleShowUser: toggle,
  users
}: IShowUserProps) => (
  <Modal
    size="tiny"
    dimmer="blurring"
    centered
    open
    onClose={() => toggle()}
  >
    {users
      ? (
        <Modal.Content scrolling>
          {users.map(user => (
            <div key={user.id} style={{ marginBottom: '5px' }}>
              <Image
                avatar
                src={getUserImgLink(user.image ? user.image : null)}
              />
              <span>{user.username}</span>
            </div>
          ))}
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

export default ShowUser;

