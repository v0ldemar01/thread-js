"use strict";
exports.__esModule = true;
var react_1 = require("react");
var Spinner_1 = require("../../components/Spinner");
var imageHelper_1 = require("../../helpers/imageHelper");
var semantic_ui_react_1 = require("semantic-ui-react");
var ShowUser = function (_a) {
    var toggle = _a.toggleShowUser, users = _a.users;
    return (react_1["default"].createElement(semantic_ui_react_1.Modal, { size: "tiny", dimmer: "blurring", centered: true, open: true, onClose: function () { return toggle(); } }, users
        ? (react_1["default"].createElement(semantic_ui_react_1.Modal.Content, { scrolling: true }, users.map(function (user) { return (react_1["default"].createElement("div", { key: user.id, style: { marginBottom: '5px' } },
            react_1["default"].createElement(semantic_ui_react_1.Image, { avatar: true, src: imageHelper_1.getUserImgLink(user.image ? user.image : null) }),
            react_1["default"].createElement("span", null, user.username))); })))
        : react_1["default"].createElement(Spinner_1["default"], null)));
};
exports["default"] = ShowUser;
