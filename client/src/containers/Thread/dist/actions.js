// /* eslint-disable consistent-return */
// import * as postService from 'src';
// import * as commentService from 'src';
// import * as userService from 'src';
// import {
//   ADD_POST,
//   EDIT_POST,
//   DELETE_POST,
//   LOAD_MORE_POSTS,
//   SET_ALL_POSTS,
//   SET_EXPANDED_POST,
//   SET_EDITED_POST,
//   SET_EDITED_COMMENT,
//   SET_SHOW_USER_BY_ACTION,
//   SEND_SHARED_POST
// } from './actionTypes';
// const setPostsAction = posts => ({
//   type: SET_ALL_POSTS,
//   posts
// });
// const addMorePostsAction = posts => ({
//   type: LOAD_MORE_POSTS,
//   posts
// });
// const addPostAction = post => ({
//   type: ADD_POST,
//   post
// });
// const editPostAction = post => ({
//   type: EDIT_POST,
//   post
// });
// const deletePostAction = post => ({
//   type: DELETE_POST,
//   post
// });
// const setExpandedPostAction = post => ({
//   type: SET_EXPANDED_POST,
//   post
// });
// const setShowUserbyAction = users => ({
//   type: SET_SHOW_USER_BY_ACTION,
//   users
// });
// const setEditedPostAction = post => ({
//   type: SET_EDITED_POST,
//   post
// });
// const setEditedCommentAction = comment => ({
//   type: SET_EDITED_COMMENT,
//   comment
// });
// const setSendSharedToEmailAction = sendedEmail => ({
//   type: SEND_SHARED_POST,
//   sendedEmail
// });
// export const loadPosts = filter => async dispatch => {
//   const posts = await postService.getAllPosts(filter);
//   dispatch(setPostsAction(posts));
// };
// export const loadMorePosts = filter => async (dispatch, getRootState) => {
//   const { posts: { posts } } = getRootState();
//   const loadedPosts = await postService.getAllPosts(filter);
//   const filteredPosts = loadedPosts
//     .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
//   dispatch(addMorePostsAction(filteredPosts));
// };
// export const applyPost = postId => async dispatch => {
//   const post = await postService.getPost(postId);
//   dispatch(addPostAction(post));
// };
// export const addPost = post => async dispatch => {
//   const { id } = await postService.addPost(post);
//   const newPost = await postService.getPost(id);
//   dispatch(addPostAction(newPost));
// };
// export const editPost = (postId, post) => async dispatch => {
//   const { id } = await postService.editPost(postId, post);
//   const newPost = await postService.getPost(id);
//   dispatch(editPostAction(newPost));
// };
// export const deletePost = postId => async dispatch => {
//   const post = await postService.getPost(postId);
//   dispatch(deletePostAction(post));
// };
// export const toggleExpandedPost = postId => async dispatch => {
//   const post = postId ? await postService.getPost(postId) : undefined;
//   if (post) {
//     const comments = await commentService.getComments(postId);
//     const mapComments = thisPost => ({
//       ...thisPost,
//       comments: [
//         ...(thisPost.comments || [])
//           .map(postComment => ({ ...postComment, ...comments.find(comment => comment.id === postComment.id) }))
//       ]
//     });
//     return dispatch(setExpandedPostAction(mapComments(post)));
//   }
//   dispatch(setExpandedPostAction(post));
// };
// export const toggleEditedPost = postId => async dispatch => {
//   const post = postId ? await postService.getPost(postId) : undefined;
//   dispatch(setEditedPostAction(post));
// };
// export const toggleShowUser = (postId, isLike) => async dispatch => {
//   const users = postId ? await postService.getReactionUsers(postId, isLike) : undefined;
//   dispatch(setShowUserbyAction(users));
// };
// export const toggleShowUserComment = (commentId, isLike) => async dispatch => {
//   const users = commentId ? await commentService.getReactionUsers(commentId, isLike) : undefined;
//   dispatch(setShowUserbyAction(users));
// };
// export const likePost = postId => async (dispatch, getRootState) => {
//   const { id, action, userId } = await postService.likePost(postId);
//   const diff = id || action === 'switch' ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
//   const diffOther = action === 'switch' ? -1 : 0;
//   const mapLikes = post => ({
//     ...post,
//     likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
//     dislikeCount: Number(post.dislikeCount) + diffOther
//   });
//   const { posts: { posts, expandedPost }, profile: { user: { username } } } = getRootState();
//   const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
//   dispatch(setPostsAction(updated));
//   if (expandedPost && expandedPost.id === postId) {
//     dispatch(setExpandedPostAction(mapLikes(expandedPost)));
//   }
//   if (userId) await userService.notifyUser({ userId, username, isLike: true });
// };
// export const dislikePost = postId => async (dispatch, getRootState) => {
//   const { id, action } = await postService.dislikePost(postId);
//   const diff = id || action === 'switch' ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
//   const diffOther = action === 'switch' ? -1 : 0;
//   const mapDisLikes = post => ({
//     ...post,
//     likeCount: Number(post.likeCount) + diffOther,
//     dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
//   });
//   const { posts: { posts, expandedPost } } = getRootState();
//   const updated = posts.map(post => (post.id !== postId ? post : mapDisLikes(post)));
//   dispatch(setPostsAction(updated));
//   if (expandedPost && expandedPost.id === postId) {
//     dispatch(setExpandedPostAction(mapDisLikes(expandedPost)));
//   }
// };
// export const addComment = request => async (dispatch, getRootState) => {
//   const { id } = await commentService.addComment(request);
//   const comment = await commentService.getComment(id);
//   const mapComments = post => ({
//     ...post,
//     commentCount: Number(post.commentCount) + 1,
//     comments: [...(post.comments || []), comment] // comment is taken from the current closure
//   });
//   const { posts: { posts, expandedPost } } = getRootState();
//   const updated = posts.map(post => (post.id !== comment.postId
//     ? post
//     : mapComments(post)));
//   dispatch(setPostsAction(updated));
//   if (expandedPost && expandedPost.id === comment.postId) {
//     dispatch(setExpandedPostAction(mapComments(expandedPost)));
//   }
// };
// export const toggleEditedComment = commentId => async dispatch => {
//   const comment = commentId ? await commentService.getComment(commentId) : undefined;
//   dispatch(setEditedCommentAction(comment));
// };
// export const editComment = (commentId, comment) => async (dispatch, getRootState) => {
//   const { id } = await commentService.editComment(commentId, comment);
//   const newComment = await commentService.getComment(id);
//   const mapComments = post => ({
//     ...post,
//     comments: [
//       ...(post.comments || []).filter(commentEL => commentEL.id !== id),
//       newComment] // comment is taken from the current closure
//   });
//   const { posts: { posts, expandedPost } } = getRootState();
//   const updated = posts.map(post => (post.id !== comment.postId
//     ? post
//     : mapComments(post)));
//   dispatch(setPostsAction(updated));
//   if (expandedPost && expandedPost.id === newComment.postId) {
//     dispatch(setExpandedPostAction(mapComments(expandedPost)));
//   }
// };
// export const deleteComment = commentId => async (dispatch, getRootState) => {
//   const comment = await commentService.getComment(commentId);
//   const mapComments = post => ({
//     ...post,
//     comments: [
//       ...(post.comments || []).filter(commentEL => commentEL.id !== commentId)
//     ] // comment is taken from the current closure
//   });
//   const { posts: { posts, expandedPost } } = getRootState();
//   const updated = posts.map(post => (post.id !== comment.postId
//     ? post
//     : mapComments(post)));
//   dispatch(setPostsAction(updated));
//   if (expandedPost && expandedPost.id === comment.postId) {
//     dispatch(setExpandedPostAction(mapComments(expandedPost)));
//   }
// };
// export const likeComment = commentId => async (dispatch, getRootState) => {
//   const { id, action } = await commentService.likeComment(commentId);
//   const diff = id || action === 'switch' ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
//   const diffOther = action === 'switch' ? -1 : 0;
//   const mapLikes = comment => ({
//     ...comment,
//     likeCount: Number(comment.likeCount) + diff, // diff is taken from the current closure
//     dislikeCount: Number(comment.dislikeCount) + diffOther
//   });
//   const { posts: { posts, expandedPost } } = getRootState();
//   const updated = posts
//     .map(post => {
//       (post.comments || [])
//         .map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));
//       return post;
//     });
//   dispatch(setPostsAction(updated));
//   if (expandedPost && expandedPost.comments.some(comment => comment.id === commentId)) {
//     const updateExpandedPost = {
//       ...expandedPost,
//       comments: expandedPost.comments
//         .map(comment => (comment.id === commentId ? mapLikes(comment) : comment))
//     };
//     dispatch(setExpandedPostAction(updateExpandedPost));
//   }
// };
// export const dislikeComment = commentId => async (dispatch, getRootState) => {
//   const { id, action } = await commentService.dislikeComment(commentId);
//   const diff = id || action === 'switch' ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
//   const diffOther = action === 'switch' ? -1 : 0;
//   const mapDisLikes = comment => ({
//     ...comment,
//     likeCount: Number(comment.likeCount) + diffOther,
//     dislikeCount: Number(comment.dislikeCount) + diff // diff is taken from the current closure
//   });
//   const { posts: { posts, expandedPost } } = getRootState();
//   const updated = posts
//     .map(post => {
//       (post.comments || [])
//         .map(comment => (comment.id !== commentId ? comment : mapDisLikes(comment)));
//       return post;
//     });
//   dispatch(setPostsAction(updated));
//   if (expandedPost && expandedPost.comments.some(comment => comment.id === commentId)) {
//     const updateExpandedPost = {
//       ...expandedPost,
//       comments: expandedPost.comments
//         .map(comment => (comment.id === commentId ? mapDisLikes(comment) : comment))
//     };
//     dispatch(setExpandedPostAction(updateExpandedPost));
//   }
// };
// export const sendSharedToEmail = (email, username, url) => async dispatch => {
//   await userService.sendSharedToEmail(email, username, url);
//   dispatch(setSendSharedToEmailAction(true));
// };
