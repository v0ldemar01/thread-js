"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var imageService = require("../../services/imageService");
var ExpandedPost_1 = require("../ExpandedPost");
var ShowUser_1 = require("../ShowUser");
var Post_1 = require("../../components/Post");
var PostForm_1 = require("../../components/PostForm");
var SharedPostLink_1 = require("../../components/SharedPostLink");
var semantic_ui_react_1 = require("semantic-ui-react");
var react_infinite_scroller_1 = require("react-infinite-scroller");
var postActions_1 = require("../../actions/postActions");
var styles_module_scss_1 = require("./styles.module.scss");
var postsFilter = {
    userId: undefined,
    from: 0,
    count: 10,
    include: undefined,
    additional: undefined
};
var Thread = function (_a) {
    var userId = _a.userId, username = _a.username, load = _a.fetchPosts, _b = _a.posts, posts = _b === void 0 ? [] : _b, _c = _a.filterPosts, filterPosts = _c === void 0 ? [] : _c, showUsers = _a.showUsers, showUserByReaction = _a.showUserByReaction, expandedPost = _a.expandedPost, editedPost = _a.editedPost, hasMorePosts = _a.hasMorePosts, createPost = _a.addPost, changePost = _a.editPost, like = _a.likePost, dislike = _a.dislikePost, toggleExpanded = _a.setExpandedPost, toggleEdit = _a.setEditedPost, showUser = _a.showUserPost, hideUser = _a.hideUserPost, erasePost = _a.deletePost, send = _a.sendSharedPost;
    var _d = react_1.useState(''), sharedPostId = _d[0], setSharedPostId = _d[1];
    var _e = react_1.useState(false), showOwnPosts = _e[0], setShowOwnPosts = _e[1];
    var _f = react_1.useState(false), showForeignPosts = _f[0], setShowForeignPosts = _f[1];
    var _g = react_1.useState(false), showFavouriteMe = _g[0], setShowFavouriteMe = _g[1];
    var toggleShowOwnPosts = function () {
        setShowOwnPosts(!showOwnPosts);
        postsFilter.userId = showOwnPosts ? undefined : userId;
        postsFilter.include = showOwnPosts ? undefined : true;
        postsFilter.from = 0;
        load(postsFilter);
        postsFilter.from = postsFilter.count; // for the next scroll
    };
    var toggleShowForeignPosts = function () {
        setShowForeignPosts(!showForeignPosts);
        postsFilter.userId = showForeignPosts ? undefined : userId;
        postsFilter.include = showOwnPosts ? undefined : false;
        postsFilter.from = 0;
        load(postsFilter);
        postsFilter.from = postsFilter.count; // for the next scroll
    };
    var toggleShowFavouriteMe = function () {
        setShowFavouriteMe(!showFavouriteMe);
        postsFilter.userId = showFavouriteMe ? undefined : userId;
        postsFilter.additional = showFavouriteMe ? undefined : 'like';
        postsFilter.from = 0;
        load(postsFilter);
        postsFilter.from = postsFilter.count; // for the next scroll
    };
    var getMorePosts = function () {
        load(postsFilter);
        var from = postsFilter.from, count = postsFilter.count;
        postsFilter.from = from + count;
    };
    var sharePost = function (id) {
        setSharedPostId(id);
    };
    var uploadImage = function (file) { return imageService.uploadImage(file); };
    var thisPosts = showOwnPosts || showForeignPosts || showFavouriteMe ? filterPosts : posts;
    return (react_1["default"].createElement("div", { className: styles_module_scss_1["default"].threadContent },
        react_1["default"].createElement("div", { className: styles_module_scss_1["default"].addPostForm },
            react_1["default"].createElement(PostForm_1["default"], { actionPost: createPost, uploadImage: uploadImage })),
        react_1["default"].createElement("div", { className: styles_module_scss_1["default"].toolbar },
            react_1["default"].createElement(semantic_ui_react_1.Checkbox, { toggle: true, label: "Show only my posts", checked: showOwnPosts, onChange: toggleShowOwnPosts }),
            react_1["default"].createElement("br", null),
            react_1["default"].createElement(semantic_ui_react_1.Checkbox, { toggle: true, label: "Show only foreign posts", checked: showForeignPosts, onChange: toggleShowForeignPosts }),
            react_1["default"].createElement("br", null),
            react_1["default"].createElement(semantic_ui_react_1.Checkbox, { toggle: true, label: "Show only liked me posts", checked: showFavouriteMe, onChange: toggleShowFavouriteMe })),
        react_1["default"].createElement(react_infinite_scroller_1["default"], { pageStart: 0, loadMore: getMorePosts, hasMore: hasMorePosts, loader: react_1["default"].createElement(semantic_ui_react_1.Loader, { active: true, inline: "centered", key: 0 }) }, thisPosts.map(function (post) { return (editedPost && editedPost === post.id ? (react_1["default"].createElement(PostForm_1["default"], { key: post.id, post: post, actionPost: changePost, uploadImage: uploadImage })) : (react_1["default"].createElement(Post_1["default"], { post: post, likePost: like, dislikePost: dislike, toggleExpandedPost: toggleExpanded, toggleShowUser: showUser, sharePost: sharePost, key: post.id, userId: userId, toggleEditedPost: toggleEdit, deletePost: erasePost }))); })),
        showUserByReaction && react_1["default"].createElement(ShowUser_1["default"], { users: showUsers, toggleShowUser: hideUser }),
        expandedPost && react_1["default"].createElement(ExpandedPost_1["default"], null),
        sharedPostId && (react_1["default"].createElement(SharedPostLink_1["default"], { postId: sharedPostId, close: function () { return setSharedPostId(''); }, sendSharedToEmail: send, username: username }))));
};
var mapStateToProps = function (rootState) { return ({
    posts: rootState.post.posts,
    filterPosts: rootState.post.filterPosts,
    showUsers: rootState.user.users,
    showUserByReaction: rootState.post.showUserByReaction,
    hasMorePosts: rootState.post.hasMorePosts,
    expandedPost: rootState.post.expandingPost,
    editedPost: rootState.post.editingPost,
    userId: rootState.user.currentUser.id,
    username: rootState.user.currentUser.username
}); };
var mapDispatchToProps = {
    likePost: postActions_1.likePost,
    dislikePost: postActions_1.dislikePost,
    addPost: postActions_1.addPost,
    editPost: postActions_1.editPost,
    deletePost: postActions_1.deletePost,
    fetchPosts: postActions_1.fetchPosts,
    setExpandedPost: postActions_1.setExpandedPost,
    setEditedPost: postActions_1.setEditedPost,
    showUserPost: postActions_1.showUserPost,
    hideUserPost: postActions_1.hideUserPost,
    sendSharedPost: postActions_1.sendSharedPost
};
var connector = react_redux_1.connect(mapStateToProps, mapDispatchToProps);
exports["default"] = connector(Thread);
