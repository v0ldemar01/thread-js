import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import * as imageService from '../../services/imageService';
import ExpandedPost from '../ExpandedPost';
import ShowUser from '../ShowUser';
import Post from '../../components/Post';
import PostForm from '../../components/PostForm';
import SharedPostLink from '../../components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  likePost,
  dislikePost,
  addPost,
  editPost,
  deletePost,
  fetchPosts,
  setExpandedPost,
  setEditedPost,
  showUserPost,
  hideUserPost,
  sendSharedPost
} from '../../actions/postActions';
import IState from '../../models/modelState';
import IFilter from '../../models/postFilter';

import styles from './styles.module.scss';

const postsFilter: IFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  include: undefined,
  additional: undefined
};

const Thread: React.FunctionComponent<PropsFromRedux>  = ({
  userId,
  username,
  fetchPosts: load,  
  posts = [],
  filterPosts = [],
  showUsers,
  showUserByReaction,
  expandedPost,
  editedPost,
  hasMorePosts,
  addPost: createPost,
  editPost: changePost,
  likePost: like,
  dislikePost: dislike,
  setExpandedPost: toggleExpanded,
  setEditedPost: toggleEdit,
  showUserPost: showUser,
  hideUserPost: hideUser,
  deletePost: erasePost,
  sendSharedPost: send
}: PropsFromRedux) => {
  const [sharedPostId, setSharedPostId] = useState('');
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showForeignPosts, setShowForeignPosts] = useState(false);
  const [showFavouriteMe, setShowFavouriteMe] = useState(false);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.include = showOwnPosts ? undefined : true;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowForeignPosts = () => {
    setShowForeignPosts(!showForeignPosts);
    postsFilter.userId = showForeignPosts ? undefined : userId;
    postsFilter.include = showOwnPosts ? undefined : false;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowFavouriteMe = () => {
    setShowFavouriteMe(!showFavouriteMe);
    postsFilter.userId = showFavouriteMe ? undefined : userId;
    postsFilter.additional = showFavouriteMe ? undefined : 'like';
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    load(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = (from as number) + (count as number);
  };

  const sharePost = (id: string) => {
    setSharedPostId(id);
  };

  const uploadImage = (file: File) => imageService.uploadImage(file);
  const thisPosts = showOwnPosts || showForeignPosts || showFavouriteMe ? filterPosts : posts;
  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <PostForm actionPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <br />
        <Checkbox
          toggle
          label="Show only foreign posts"
          checked={showForeignPosts}
          onChange={toggleShowForeignPosts}
        />
        <br />
        <Checkbox
          toggle
          label="Show only liked me posts"
          checked={showFavouriteMe}
          onChange={toggleShowFavouriteMe}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {thisPosts.map(post => (
          editedPost && editedPost === post.id ? (
            <PostForm
              key={post.id}
              post={post}
              actionPost={changePost}              
              uploadImage={uploadImage}              
            />
          ) : (
            <Post
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggleExpanded}
              toggleShowUser={showUser}
              sharePost={sharePost}
              key={post.id}
              userId={userId as string}
              toggleEditedPost={toggleEdit}
              deletePost={erasePost}
            />
          )
        ))}
      </InfiniteScroll>
      {showUserByReaction && <ShowUser users={showUsers} toggleShowUser={hideUser}/>}
      {expandedPost && <ExpandedPost />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId('')}
          sendSharedToEmail={send}
          username={username as string}
        />
      )}
    </div>
  );
};

const mapStateToProps = (rootState: IState) => ({
  posts: rootState.post.posts,
  filterPosts: rootState.post.filterPosts,
  showUsers: rootState.user.users,
  showUserByReaction: rootState.post.showUserByReaction,
  hasMorePosts: rootState.post.hasMorePosts,
  expandedPost: rootState.post.expandingPost,
  editedPost: rootState.post.editingPost,
  userId: rootState.user.currentUser.id,
  username: rootState.user.currentUser.username
});

const mapDispatchToProps = {  
  likePost,
  dislikePost,  
  addPost,
  editPost,
  deletePost,
  fetchPosts,
  setExpandedPost,
  setEditedPost,
  showUserPost,
  hideUserPost,
  sendSharedPost
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(Thread);
