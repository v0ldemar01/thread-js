"use strict";

exports.__esModule = true;

var react_1 = require("react");

var react_router_dom_1 = require("react-router-dom");

var react_redux_1 = require("react-redux");

var Thread_1 = require("../Thread");

var LoginPage_1 = require("../LoginPage");

var RegistrationPage_1 = require("../RegistrationPage");

var ForgotPassword_1 = require("../ForgotPassword");

var ChangePassword_1 = require("../ChangePassword");

var Profile_1 = require("../Profile");

var Header_1 = require("../../components/Header");

var SharedPost_1 = require("../SharedPost");

var Spinner_1 = require("../../components/Spinner");

var NotFound_1 = require("../../scenes/NotFound");

var PrivateRoute_1 = require("../PrivateRoute");

var PublicRoute_1 = require("../PublicRoute"); // import Notifications from '../../components/Notifications';
// import { loadCurrentUser, logout } from '../Profile/actions';


var userActions_1 = require("../../actions/userActions");

var Routing = function Routing(_a) {
  var user = _a.user,
      isAuthorized = _a.isAuthorized,
      signOut = _a.logoutUser,
      loadCurrentUser = _a.loadUser,
      isLoading = _a.isLoading;
  react_1.useEffect(function () {
    if (!isAuthorized) {
      loadCurrentUser();
    }
  });
  return isLoading ? react_1["default"].createElement(Spinner_1["default"], null) : react_1["default"].createElement("div", {
    className: "fill"
  }, isAuthorized && react_1["default"].createElement("header", null, react_1["default"].createElement(Header_1["default"], {
    user: user,
    logout: signOut
  })), react_1["default"].createElement("main", {
    className: "fill"
  }, react_1["default"].createElement(react_router_dom_1.Switch, null, react_1["default"].createElement(PublicRoute_1["default"], {
    exact: true,
    path: "/login",
    component: LoginPage_1["default"]
  }), react_1["default"].createElement(PublicRoute_1["default"], {
    exact: true,
    path: "/registration",
    component: RegistrationPage_1["default"]
  }), react_1["default"].createElement(PublicRoute_1["default"], {
    exact: true,
    path: "/forgotPassword",
    component: ForgotPassword_1["default"]
  }), react_1["default"].createElement(PublicRoute_1["default"], {
    exact: true,
    path: "/reset/:token",
    component: ChangePassword_1["default"]
  }), react_1["default"].createElement(PrivateRoute_1["default"], {
    exact: true,
    path: "/",
    component: Thread_1["default"]
  }), react_1["default"].createElement(PrivateRoute_1["default"], {
    exact: true,
    path: "/profile",
    component: Profile_1["default"]
  }), react_1["default"].createElement(PrivateRoute_1["default"], {
    path: "/share/:postHash",
    component: SharedPost_1["default"]
  }), react_1["default"].createElement(react_router_dom_1.Route, {
    path: "*",
    exact: true,
    component: NotFound_1["default"]
  }))));
};

var mapStateToProps = function mapStateToProps(rootState) {
  return {
    isAuthorized: rootState.user.isLoggedIn,
    user: rootState.user.currentUser,
    isLoading: rootState.main.isLoading
  };
};

var mapDispatchToProps = {
  loadUser: userActions_1.loadUser,
  logoutUser: userActions_1.logoutUser
};
var connector = react_redux_1.connect(mapStateToProps, mapDispatchToProps);
exports["default"] = connector(Routing);