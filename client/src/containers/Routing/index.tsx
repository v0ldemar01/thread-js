import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import Thread from '../Thread';
import LoginPage from '../LoginPage';
import RegistrationPage from '../RegistrationPage';
import ForgotPassword from '../ForgotPassword';
import ChangePost from '../ChangePassword';
import Profile from '../Profile';
import Header from '../../components/Header';
import SharedPost from '../SharedPost';
import Spinner from '../../components/Spinner';
import NotFound from '../../scenes/NotFound';
import PrivateRoute from '../PrivateRoute';
import PublicRoute from '../PublicRoute';
// import Notifications from '../../components/Notifications';
import { loadUser, logoutUser } from '../../actions/userActions';
import IState from '../../models/modelState';

const Routing: React.FunctionComponent<PropsFromRedux> = ({
  user,
  isAuthorized,
  logoutUser: signOut,
  loadUser: loadCurrentUser,
  isLoading
}: PropsFromRedux) => {

  useEffect(() => {
    if (!isAuthorized) {
      loadCurrentUser();
    }
  }, []);

  return (
    isLoading
      ? <Spinner />
      : (
        <div className="fill">
          {isAuthorized && (
            <header>
              <Header user={user} logout={signOut} />
            </header>
          )}
          <main className="fill">
            <Switch>
              <PublicRoute exact path="/login" component={LoginPage} />
              <PublicRoute exact path="/registration" component={RegistrationPage} />
              <PublicRoute exact path="/forgotPassword" component={ForgotPassword} />
              <PublicRoute exact path="/reset/:token" component={ChangePost} />
              <PrivateRoute exact path="/" component={Thread} />
              <PrivateRoute exact path="/profile" component={Profile} />
              <PrivateRoute path="/share/:postHash" component={SharedPost} />
              <Route path="*" exact component={NotFound} />
            </Switch>
          </main>
          {/* <Notifications applyPost={newPost} user={user} /> */}
        </div>
      )
  );
};

const mapStateToProps = (rootState: IState) => ({
  isAuthorized: rootState.user.isLoggedIn,
  user: rootState.user.currentUser,
  isLoading: rootState.main.isLoading
});

const mapDispatchToProps = { 
  loadUser, 
  logoutUser   
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(Routing);
