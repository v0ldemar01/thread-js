import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { getUserImgLink } from '../../helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Button
} from 'semantic-ui-react';
import {
  editUser,
  setEditUser,
  cancelEditUser
} from '../../actions/userActions';
import IState from '../../models/modelState';
import IIMage from '../../models/image';

const Profile: React.FunctionComponent<PropsFromRedux> = ({
  user,
  setEditUser: setEdit,
  cancelEditUser: cancelEdit,
  editUser: edit,
  editedUser
}: PropsFromRedux) => {
  const [username, setUsername] = useState(user.username);
  const [email, setEmail] = useState(user.email);
  const [userStatus, setUserStatus] = useState(user.userStatus || '');

  const handleEditUser = async () => {
    edit({ ...user, username, email, userStatus });
    cancelEdit();
  };
  const isEditedUser = Boolean(editedUser);
  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={getUserImgLink(user.image as IIMage)} size="medium" circular />
        <br />
        <Input
          icon="info circle"
          iconPosition="left"
          placeholder="User status..."
          type="text"
          disabled={!isEditedUser}
          value={userStatus}
          onChange={event => setUserStatus(event.target.value)}
        />
        <br />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled={!isEditedUser}
          value={username}
          onChange={event => setUsername(event.target.value)}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled={!isEditedUser}
          value={email}
          onChange={event => setEmail(event.target.value)}
        />
        <br />
        <br />
        {editedUser ? (
          <Button
            color="purple"
            type="submit"
            onClick={() => handleEditUser()}
          >
            Confirm changes
          </Button>
        ) : (
          <Button
            color="purple"
            type="submit"
            onClick={() => setEdit(user)}
          >
            Edit
          </Button>
        )}
      </Grid.Column>
    </Grid>
  );
};

const mapStateToProps = (rootState: IState) => ({
  user: rootState.user.currentUser,
  editedUser: rootState.user.editingUser
});

const mapDispatchToProps = {
  editUser,
  setEditUser,
  cancelEditUser
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(Profile);
