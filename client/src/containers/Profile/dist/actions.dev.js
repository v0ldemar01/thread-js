// import * as authService from 'src';
// import * as userService from 'src';
// import {
//   SET_USER,
//   SET_EDITED_USER,
//   SEND_EMAIL,
//   RESET_CHANGE_PASSWORD
// } from './actionTypes';
// const setToken = token => localStorage.setItem('token', token);
// const setUser = user => async dispatch => dispatch({
//   type: SET_USER,
//   user
// });
// const setEditedUser = user => ({
//   type: SET_EDITED_USER,
//   user
// });
// const setCurrentEmail = email => ({
//   type: SEND_EMAIL,
//   email
// });
// const setChangePassword = status => ({
//   type: RESET_CHANGE_PASSWORD,
//   status
// });
// const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
//   setToken(token); // token should be set first before user
//   setUser(user)(dispatch, getRootState);
// };
// const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
//   const { user, token } = await authResponsePromise;
//   setAuthData(user, token)(dispatch, getRootState);
// };
// export const login = request => handleAuthResponse(authService.login(request));
// export const register = request => handleAuthResponse(authService.registration(request));
// export const logout = () => setAuthData();
// export const loadCurrentUser = () => async (dispatch, getRootState) => {
//   const user = await authService.getCurrentUser();
//   setUser(user)(dispatch, getRootState);
// };
// export const toggleEditedUser = userId => async dispatch => {
//   const user = userId ? await userService.getUser(userId) : undefined;
//   dispatch(setEditedUser(user));
// };
// export const editUser = (userId, user) => async dispatch => {
//   const { id } = await userService.editUser(userId, user);
//   const newUser = await userService.getUser(id);
//   dispatch(setUser(newUser));
// };
// export const sendEmail = email => async dispatch => {
//   await authService.sendEmail({ email });
//   dispatch(setCurrentEmail(email));
// };
// export const changePassword = (token, password) => async dispatch => {
//   await authService.changePassword(token, { password });
//   dispatch(setChangePassword(true));
// };
"use strict";