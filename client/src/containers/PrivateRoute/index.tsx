import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import IRouteProps from '../../models/modelProps/routeProps';
import IState from '../../models/modelState';

const PrivateRoute: React.FunctionComponent<IRouteProps & PropsFromRedux> = ({
  component: Component, 
  isLoggedIn, 
  ...rest 
}: IRouteProps & PropsFromRedux) => (
  <Route
    {...rest}
    render={props => (isLoggedIn
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
  />
);

const mapStateToProps = (rootState: IState) => ({
  isLoggedIn: rootState.user.isLoggedIn
});

const connector = connect(mapStateToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(PrivateRoute);
