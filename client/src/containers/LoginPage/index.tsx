import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { loginInit } from '../../actions/userActions';
import Logo from '../../components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import LoginForm from '../../components/LoginForm';

const LoginPage: React.FunctionComponent<PropsFromRedux> = ({ 
  loginInit: login 
}: PropsFromRedux) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Login to your account
      </Header>
      <LoginForm login={login} />
      <Message>
        New to us?
        {' '}
        <NavLink exact to="/registration">Sign Up</NavLink>
      </Message>
      <Message>
        <NavLink exact to="/forgotPassword">Forgot password?</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

const mapDispatchToProps = {
  loginInit
}

const connector = connect(null, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(LoginPage);
