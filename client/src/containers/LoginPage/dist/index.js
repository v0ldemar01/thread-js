"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var userActions_1 = require("../../actions/userActions");
var Logo_1 = require("../../components/Logo");
var semantic_ui_react_1 = require("semantic-ui-react");
var react_router_dom_1 = require("react-router-dom");
var LoginForm_1 = require("../../components/LoginForm");
var LoginPage = function (_a) {
    var login = _a.loginInit;
    return (react_1["default"].createElement(semantic_ui_react_1.Grid, { textAlign: "center", verticalAlign: "middle", className: "fill" },
        react_1["default"].createElement(semantic_ui_react_1.Grid.Column, { style: { maxWidth: 450 } },
            react_1["default"].createElement(Logo_1["default"], null),
            react_1["default"].createElement(semantic_ui_react_1.Header, { as: "h2", color: "teal", textAlign: "center" }, "Login to your account"),
            react_1["default"].createElement(LoginForm_1["default"], { login: login }),
            react_1["default"].createElement(semantic_ui_react_1.Message, null,
                "New to us?",
                ' ',
                react_1["default"].createElement(react_router_dom_1.NavLink, { exact: true, to: "/registration" }, "Sign Up")),
            react_1["default"].createElement(semantic_ui_react_1.Message, null,
                react_1["default"].createElement(react_router_dom_1.NavLink, { exact: true, to: "/forgotPassword" }, "Forgot password?")))));
};
var mapDispatchToProps = {
    loginInit: userActions_1.loginInit
};
var connector = react_redux_1.connect(null, mapDispatchToProps);
exports["default"] = connector(LoginPage);
