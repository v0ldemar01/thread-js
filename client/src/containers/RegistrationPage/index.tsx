import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import {registerInit} from '../../actions/userActions'
import Logo from '../../components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import RegistrationForm from '../../components/RegistrationForm';

const RegistrationPage: React.FunctionComponent<PropsFromRedux> = ({ 
  registerInit: signOn 
}: PropsFromRedux) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Register for free account
      </Header>
      <RegistrationForm register={signOn} />
      <Message>
        Already with us?
        {' '}
        <NavLink exact to="/login">Sign In</NavLink>
      </Message>
      <Message>
        <NavLink exact to="/forgotPassword">Forgot password?</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);


const mapDispatchToProps = { 
  registerInit 
};

const connector = connect(null, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(RegistrationPage);