"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var userActions_1 = require("../../actions/userActions");
var Logo_1 = require("../../components/Logo");
var semantic_ui_react_1 = require("semantic-ui-react");
var react_router_dom_1 = require("react-router-dom");
var RegistrationForm_1 = require("../../components/RegistrationForm");
var RegistrationPage = function (_a) {
    var signOn = _a.registerInit;
    return (react_1["default"].createElement(semantic_ui_react_1.Grid, { textAlign: "center", verticalAlign: "middle", className: "fill" },
        react_1["default"].createElement(semantic_ui_react_1.Grid.Column, { style: { maxWidth: 450 } },
            react_1["default"].createElement(Logo_1["default"], null),
            react_1["default"].createElement(semantic_ui_react_1.Header, { as: "h2", color: "teal", textAlign: "center" }, "Register for free account"),
            react_1["default"].createElement(RegistrationForm_1["default"], { register: signOn }),
            react_1["default"].createElement(semantic_ui_react_1.Message, null,
                "Already with us?",
                ' ',
                react_1["default"].createElement(react_router_dom_1.NavLink, { exact: true, to: "/login" }, "Sign In")),
            react_1["default"].createElement(semantic_ui_react_1.Message, null,
                react_1["default"].createElement(react_router_dom_1.NavLink, { exact: true, to: "/forgotPassword" }, "Forgot password?")))));
};
var mapDispatchToProps = {
    registerInit: userActions_1.registerInit
};
var connector = react_redux_1.connect(null, mapDispatchToProps);
exports["default"] = connector(RegistrationPage);
