import IIMage from "./image";

export default interface IUser {
  id?: string;
  email?: string;
  username?: string;
  password?: string;
  image?: IIMage;
  userStatus?: string;
  resetPasswordToken?: string;
  createdAt?: Date;
  updatedAt?: Date;
}