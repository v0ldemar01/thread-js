export default interface IShareEmailUser {
  email: string;
  username: string;
  url: string;
}