export default interface ISearchElement {
  additional?: boolean;
  userId?: string;
  count?: number;
  from?: number;
}