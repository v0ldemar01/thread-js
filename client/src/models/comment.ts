import IUser from './user';

export default interface IComment {
  id: string;
  body: string;
  createdAt: string;
  user: IUser
  likeCount: string;
  dislikeCount: string;
}