import IUser from "./user";

export default interface IChangePassword {
  token: string;
  newUser: IUser;
}