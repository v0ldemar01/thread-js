export default interface IInfoReact {
  type: string;
  elementId: string;
  isLike: boolean;
}