import IPost from '../post';
import ISearchElement from '../searchElement';

export default interface IPostState {  
  posts: IPost[];
  filterPosts: IPost[];
  search: ISearchElement;
  editingPost: string;
  expandingPost: string;
  hasMorePosts: boolean;
  showUserByReaction?: boolean;
  error?: string;
}