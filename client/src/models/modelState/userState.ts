import IUser from '../user';

export default interface IUserState {  
  currentUser: IUser;
  isLoggedIn: boolean;
  users: IUser[];
  editingUser: boolean;
  changedStatus?: boolean;
  error?: string;
}