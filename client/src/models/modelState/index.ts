import ICommentState from './commentState';
import IPostState from './postState';
import IMainState from './mainState';
import IUserState from './userState';

export default interface IState {
  post: IPostState;
  comment: ICommentState;
  main: IMainState;
  user: IUserState;
}

