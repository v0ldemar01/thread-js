import IComment from '../comment';

export default interface ICommentState {  
  comments: IComment[];
  postId: string;  
  showUserByReaction?: boolean;
  editingComment?: string;
  error?: string 
}