export default interface IHeader extends Headers {
  ['Content-Type']?: string;
  Accept?: string;
  Authorization?: string;
}