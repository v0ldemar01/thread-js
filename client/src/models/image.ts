export default interface IIMage {
  id?: string;
  link: string;
  deleteHash?: string;
  createdAt?: Date;
  updatedAt?: Date;
}