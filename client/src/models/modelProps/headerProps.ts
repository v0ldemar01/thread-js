import IUser from '../user';

export default interface IHeader {
  user: IUser;
  logout: Function;
}