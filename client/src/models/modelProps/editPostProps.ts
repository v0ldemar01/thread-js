import IPost from '../post';

export default interface IEditPostProps {
  post: IPost;
  editPost: Function;
  uploadImage: Function;
  toggleEditedPost: Function;
}