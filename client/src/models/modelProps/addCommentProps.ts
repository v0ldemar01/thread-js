export default interface IAddCommentProps {
  postId: string;
  addComment: Function;
}