import IPost from '../post';

export default interface IPostFormProps {
  actionPost: Function;
  uploadImage: Function;
  post?: IPost;
}