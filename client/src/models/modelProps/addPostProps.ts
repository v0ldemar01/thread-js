export default interface IAddPostProps {
  addPost: Function;
  uploadImage: Function;
}