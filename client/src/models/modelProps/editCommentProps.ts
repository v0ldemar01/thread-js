import IComment from '../comment';

export default interface IEditCommentProps {
  comment: IComment;
  editComment: Function;
  toggleEditedComment: Function;
}