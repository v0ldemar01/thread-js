import IUser from "../user";

export default interface IShowUserProps {
  users: IUser[];
  toggleShowUser: Function;
}