import IPost from "../post";

export default interface postProps {
  post: IPost;
  likePost: Function;
  dislikePost: Function;
  toggleExpandedPost: Function;
  toggleShowUser: Function;
  toggleEditedPost: Function;
  deletePost: Function;
  sharePost: Function;
  userId: string;
}