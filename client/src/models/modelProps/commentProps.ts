import IComment from '../comment';

export default interface ICommentProps {
  comment: IComment;
  userId: string;
  toggleEditedComment: Function;
  deleteComment: Function;
  likeComment: Function;
  dislikeComment: Function;
  toggleShowUser: Function;
}