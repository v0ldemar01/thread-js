export default interface ISharedPostLinkProps {
  postId: string;
  username: string;
  close: Function;
  sendSharedToEmail: Function;
}