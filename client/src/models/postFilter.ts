export default interface IFilterPosts {
  userId?: string,
  from?: number,
  count?: number,
  include?: boolean,
  additional?: string
}