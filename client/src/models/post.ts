import IIMage from "./image";
import IUser from "./user";

export default interface IPost {
  id?: string;
  body: string;
  image?: IIMage;
  user: IUser;
  likeCount: string;
  dislikeCount: string;
  commentCount: string;
  createdAt?: Date;
  updatedAt?: Date;
}