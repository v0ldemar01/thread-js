"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var redux_1 = require("redux");
var connected_react_router_1 = require("connected-react-router");
var redux_saga_1 = require("redux-saga");
var redux_devtools_extension_1 = require("redux-devtools-extension");
var history_1 = require("../history");
var reducers_1 = require("./reducers");
var sagas_1 = require("../sagas");
var sagaMiddleware = redux_saga_1["default"]();
var middlewares = [
    sagaMiddleware,
    connected_react_router_1.routerMiddleware(history_1["default"])
];
var rootReducer = redux_1.combineReducers(__assign({ router: connected_react_router_1.connectRouter(history_1["default"]) }, reducers_1["default"]));
var store = redux_1.createStore(rootReducer, redux_1.compose(redux_1.applyMiddleware.apply(void 0, middlewares), redux_devtools_extension_1.composeWithDevTools()));
sagaMiddleware.run(sagas_1["default"]);
exports["default"] = store;
