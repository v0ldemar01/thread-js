
import { 
  FETCH_COMMENTS_SUCCESS,
  FETCH_COMMENTS_FAILURE,
  ADD_COMMENT_SUCCESS,
  ADD_COMMENT_FAILURE,  
  EDIT_COMMENT_SUCCESS,
  EDIT_COMMENT_FAILURE,
  SET_EDITED_COMMENT,
  CANCEL_EDITED_COMMENT,
  DELETE_COMMENT_SUCCESS,
  DELETE_COMMENT_FAILURE,
  LIKE_COMMENT_SUCCESS,
  LIKE_COMMENT_FAILURE,
  DISLIKE_COMMENT_SUCCESS,
  DISLIKE_COMMENT_FAILURE,
  SHOW_USER_BY_COMMENT,
  HIDE_USER_BY_COMMENT,
  CommentActionTypes 
} from '../../actions/types/commentTypes';
import IComment from '../../models/comment';
import ICommentState from '../../models/modelState/commentState';

const initialState: ICommentState = { 
  comments: [] as IComment[],  
  postId: '',
  showUserByReaction: false,
  editingComment: '',
  error: '' 
}

export const commentReducer = (
  state = initialState,
  action: CommentActionTypes
) => {
  switch (action.type) {
    case FETCH_COMMENTS_SUCCESS: {
      return {
        ...state, comments: action.payload        
      }
    }

    case ADD_COMMENT_SUCCESS: {
      return {
        ...state,
        comments: [...state.comments, action.payload]
      };
    }

    case EDIT_COMMENT_SUCCESS: 
    case LIKE_COMMENT_SUCCESS: 
    case DISLIKE_COMMENT_SUCCESS: {
      return {
        ...state,
        comments: [...state.comments
          .map(comment => comment.id === action.payload.id ? action.payload : comment)
        ]
      }
    }

    case DELETE_COMMENT_SUCCESS: {
      return {
        ...state,
        comments: [...state.comments
          .filter(comment => comment.id !== action.payload)
        ]
      }
    }

    case SET_EDITED_COMMENT: {
      return {
        ...state,
        editingComment: action.payload
      }
    } 
    
    case CANCEL_EDITED_COMMENT: {
      return {
        ...state,
        editingComment: ''
      }
    }

    case SHOW_USER_BY_COMMENT: {
      return {
        ...state,
        showUserByReaction: true
      }
    }

    case HIDE_USER_BY_COMMENT: {
      return {
        ...state,
        showUserByReaction: false
      }
    }

    case FETCH_COMMENTS_FAILURE: 
    case ADD_COMMENT_FAILURE: 
    case EDIT_COMMENT_FAILURE: 
    case DELETE_COMMENT_FAILURE: 
    case LIKE_COMMENT_FAILURE: 
    case DISLIKE_COMMENT_FAILURE: {
      return {...state, error: action.payload};
    }   
        
    default:
      return state;
  }
}