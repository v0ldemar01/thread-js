"use strict";
exports.__esModule = true;
var postReducer_1 = require("./postReducer");
var commentReducer_1 = require("./commentReducer");
var mainReducer_1 = require("./mainReducer");
var userReducer_1 = require("./userReducer");
var rootReducer = {
    post: postReducer_1.postReducer,
    comment: commentReducer_1.commentReducer,
    main: mainReducer_1.mainReducer,
    user: userReducer_1.userReducer
};
exports["default"] = rootReducer;
