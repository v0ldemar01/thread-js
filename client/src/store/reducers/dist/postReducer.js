"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.postReducer = void 0;
var postTypes_1 = require("../../actions/types/postTypes");
var initialState = {
    posts: [],
    filterPosts: [],
    search: {},
    editingPost: '',
    expandingPost: '',
    hasMorePosts: true,
    showUserByReaction: false,
    error: ''
};
exports.postReducer = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case postTypes_1.FETCH_POSTS_SUCCESS: {
            return __assign(__assign({}, state), { posts: __spreadArrays((state.posts || []), action.payload).filter(function (post, index, self) {
                    return index === self.findIndex(function (p) { return (p.id === post.id); });
                }), hasMorePosts: Boolean(action.payload.length) });
        }
        case postTypes_1.FETCH_POSTS_FILTER_SUCCESS: {
            return __assign(__assign({}, state), { filterPosts: __spreadArrays((state.filterPosts || []), action.payload).filter(function (post, index, self) {
                    return index === self.findIndex(function (p) { return (p.id === post.id); });
                }), hasMorePosts: Boolean(action.payload.length) });
        }
        case postTypes_1.CLEAR_POSTS_FILTER: {
            return __assign(__assign({}, state), { filterPosts: [] });
        }
        case postTypes_1.ADD_POST_SUCCESS: {
            return __assign(__assign({}, state), { posts: __spreadArrays([action.payload], state.posts) });
        }
        case postTypes_1.EDIT_POST_SUCCESS:
        case postTypes_1.LIKE_POST_SUCCESS:
        case postTypes_1.DISLIKE_POST_SUCCESS: {
            return __assign(__assign({}, state), { posts: __spreadArrays(state.posts
                    .map(function (post) { return post.id === action.payload.id ? action.payload : post; })) });
        }
        case postTypes_1.DELETE_POST_SUCCESS: {
            return __assign(__assign({}, state), { posts: __spreadArrays(state.posts
                    .filter(function (post) { return post.id !== action.payload; })) });
        }
        case postTypes_1.SET_EDITED_POST: {
            return __assign(__assign({}, state), { editingPost: action.payload });
        }
        case postTypes_1.CANCEL_EDITED_POST: {
            return __assign(__assign({}, state), { editingPost: '' });
        }
        case postTypes_1.SET_EXPANDED_POST: {
            return __assign(__assign({}, state), { expandingPost: action.payload });
        }
        case postTypes_1.CANCEL_EXPANDED_POST: {
            return __assign(__assign({}, state), { expandingPost: '' });
        }
        case postTypes_1.SHOW_USER_BY_POST: {
            return __assign(__assign({}, state), { showUserByReaction: true });
        }
        case postTypes_1.HIDE_USER_BY_POST: {
            return __assign(__assign({}, state), { showUserByReaction: false });
        }
        case postTypes_1.FETCH_POSTS_FAILURE:
        case postTypes_1.ADD_POST_FAILURE:
        case postTypes_1.EDIT_POST_FAILURE:
        case postTypes_1.DELETE_POST_FAILURE:
        case postTypes_1.LIKE_POST_FAILURE:
        case postTypes_1.DISLIKE_POST_FAILURE:
        case postTypes_1.SEND_SHARED_POST_FAILURE: {
            return __assign(__assign({}, state), { error: action.payload });
        }
        default:
            return state;
    }
};
