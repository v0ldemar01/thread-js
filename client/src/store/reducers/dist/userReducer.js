"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.userReducer = void 0;
var userTypes_1 = require("../../actions/types/userTypes");
var initialState = {
    currentUser: {},
    isLoggedIn: false,
    users: [],
    editingUser: false,
    changedStatus: false,
    error: ''
};
exports.userReducer = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case userTypes_1.LOAD_USER_SUCCESS:
        case userTypes_1.LOGIN_SUCCESS:
        case userTypes_1.REGISTER_SUCCESS:
        case userTypes_1.EDIT_USER_SUCCESS: {
            return __assign(__assign({}, state), { currentUser: action.payload, isLoggedIn: true, changedStatus: false });
        }
        case userTypes_1.LOAD_USER_FAILURE:
        case userTypes_1.LOGIN_FAILURE:
        case userTypes_1.REGISTER_FAILURE:
        case userTypes_1.EDIT_USER_FAILURE:
        case userTypes_1.RESET_PASSWORD_FAILURE:
        case userTypes_1.CHANGE_PASSWORD_FAILURE: {
            return __assign(__assign({}, state), { error: action.payload, isLoggedIn: false });
        }
        case userTypes_1.LOGOUT_USER: {
            return __assign(__assign({}, state), { currentUser: {}, isLoggedIn: false });
        }
        case userTypes_1.FETCH_USERS_SUCCESS: {
            return __assign(__assign({}, state), { users: action.payload });
        }
        case userTypes_1.FETCH_USERS_FAILURE: {
            return __assign(__assign({}, state), { error: action.payload });
        }
        case userTypes_1.SET_EDITED_USER: {
            return __assign(__assign({}, state), { editingUser: true });
        }
        case userTypes_1.CANCEL_EDITED_USER: {
            return __assign(__assign({}, state), { editingUser: false });
        }
        case userTypes_1.CHANGE_PASSWORD_SUCCESS: {
            return __assign(__assign({}, state), { changedStatus: true });
        }
        default:
            return state;
    }
};
