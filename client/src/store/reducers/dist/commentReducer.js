"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.commentReducer = void 0;
var commentTypes_1 = require("../../actions/types/commentTypes");
var initialState = {
    comments: [],
    postId: '',
    showUserByReaction: false,
    editingComment: '',
    error: ''
};
exports.commentReducer = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case commentTypes_1.FETCH_COMMENTS_SUCCESS: {
            return __assign(__assign({}, state), { comments: action.payload });
        }
        case commentTypes_1.ADD_COMMENT_SUCCESS: {
            return __assign(__assign({}, state), { comments: __spreadArrays(state.comments, [action.payload]) });
        }
        case commentTypes_1.EDIT_COMMENT_SUCCESS:
        case commentTypes_1.LIKE_COMMENT_SUCCESS:
        case commentTypes_1.DISLIKE_COMMENT_SUCCESS: {
            return __assign(__assign({}, state), { comments: __spreadArrays(state.comments
                    .map(function (comment) { return comment.id === action.payload.id ? action.payload : comment; })) });
        }
        case commentTypes_1.DELETE_COMMENT_SUCCESS: {
            return __assign(__assign({}, state), { comments: __spreadArrays(state.comments
                    .filter(function (comment) { return comment.id !== action.payload; })) });
        }
        case commentTypes_1.SET_EDITED_COMMENT: {
            return __assign(__assign({}, state), { editingComment: action.payload });
        }
        case commentTypes_1.CANCEL_EDITED_COMMENT: {
            return __assign(__assign({}, state), { editingComment: '' });
        }
        case commentTypes_1.SHOW_USER_BY_COMMENT: {
            return __assign(__assign({}, state), { showUserByReaction: true });
        }
        case commentTypes_1.HIDE_USER_BY_COMMENT: {
            return __assign(__assign({}, state), { showUserByReaction: false });
        }
        case commentTypes_1.FETCH_COMMENTS_FAILURE:
        case commentTypes_1.ADD_COMMENT_FAILURE:
        case commentTypes_1.EDIT_COMMENT_FAILURE:
        case commentTypes_1.DELETE_COMMENT_FAILURE:
        case commentTypes_1.LIKE_COMMENT_FAILURE:
        case commentTypes_1.DISLIKE_COMMENT_FAILURE: {
            return __assign(__assign({}, state), { error: action.payload });
        }
        default:
            return state;
    }
};
