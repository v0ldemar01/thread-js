"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.mainReducer = void 0;
var mainTypes_1 = require("../../actions/types/mainTypes");
var initialState = {
    isLoading: false
};
exports.mainReducer = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case mainTypes_1.SHOW_LOADER: {
            return __assign(__assign({}, state), { isLoading: true });
        }
        case mainTypes_1.HIDE_LOADER: {
            return __assign(__assign({}, state), { isLoading: false });
        }
        default:
            return state;
    }
};
