
import { 
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,  
  LOGOUT_USER,
  UserActionTypes, 
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILURE,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  EDIT_USER_SUCCESS,
  EDIT_USER_FAILURE,
  SET_EDITED_USER,
  CANCEL_EDITED_USER,
  RESET_PASSWORD_FAILURE,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE
} from '../../actions/types/userTypes';
import IUser from '../../models/user';
import IUserState from '../../models/modelState/userState';

const initialState: IUserState = {
  currentUser: {} as IUser,
  isLoggedIn: false,
  users: [] as IUser[],
  editingUser: false,
  changedStatus: false,
  error: ''
}

export const userReducer = (
  state = initialState,
  action: UserActionTypes
) => {
  switch (action.type) {
    case LOAD_USER_SUCCESS:
    case LOGIN_SUCCESS: 
    case REGISTER_SUCCESS:
    case EDIT_USER_SUCCESS: {
      return {
        ...state, currentUser: action.payload, isLoggedIn: true, changedStatus: false
      };
    }

    case LOAD_USER_FAILURE:
    case LOGIN_FAILURE: 
    case REGISTER_FAILURE:
    case EDIT_USER_FAILURE: 
    case RESET_PASSWORD_FAILURE: 
    case CHANGE_PASSWORD_FAILURE: {
      return {
        ...state, error: action.payload, isLoggedIn: false
      };
    }    
    
    case LOGOUT_USER: {
      return {
        ...state, currentUser: {}, isLoggedIn: false
      }
    }

    case FETCH_USERS_SUCCESS: {
      return {
        ...state, users: action.payload
      };
    }

    case FETCH_USERS_FAILURE: {
      return {
        ...state, error: action.payload
      };
    } 

    case SET_EDITED_USER: {
      return {
        ...state, editingUser: true
      }
    }

    case CANCEL_EDITED_USER: {
      return {
        ...state, editingUser: false
      }
    }

    case CHANGE_PASSWORD_SUCCESS: {
      return {
        ...state, changedStatus: true
      }
    }

    default:
      return state;
  }
}