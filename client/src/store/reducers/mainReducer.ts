import { 
  SHOW_LOADER,
  HIDE_LOADER,
  MainActionTypes 
} from '../../actions/types/mainTypes';
import IMainState from '../../models/modelState/mainState';

const initialState: IMainState = {
  isLoading: false
};

export const mainReducer = (
  state = initialState, 
  action: MainActionTypes
) => {
  switch (action.type) {
    case SHOW_LOADER: {
      return {
        ...state,
        isLoading: true
      };
    }

    case HIDE_LOADER: {
      return {
        ...state,
        isLoading: false
      };
    }

    default:
      return state;
  }
}
