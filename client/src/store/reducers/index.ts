import {postReducer} from './postReducer';
import {commentReducer} from './commentReducer';
import {mainReducer} from './mainReducer';
import {userReducer} from './userReducer';

const rootReducer = {
  post: postReducer,
  comment: commentReducer,
  main: mainReducer,
  user: userReducer
};

export default rootReducer;
