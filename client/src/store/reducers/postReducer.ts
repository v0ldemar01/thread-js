
import { 
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FILTER_SUCCESS,
  CLEAR_POSTS_FILTER,
  FETCH_POSTS_FAILURE,
  ADD_POST_SUCCESS,
  ADD_POST_FAILURE,
  SET_EDITED_POST,
  CANCEL_EDITED_POST,
  SET_EXPANDED_POST,
  CANCEL_EXPANDED_POST,
  EDIT_POST_SUCCESS,
  EDIT_POST_FAILURE,
  DELETE_POST_SUCCESS,
  DELETE_POST_FAILURE,
  LIKE_POST_SUCCESS,
  LIKE_POST_FAILURE,
  DISLIKE_POST_SUCCESS,
  DISLIKE_POST_FAILURE,
  SHOW_USER_BY_POST,
  HIDE_USER_BY_POST,
  SEND_SHARED_POST_FAILURE, 
  PostActionTypes 
} from '../../actions/types/postTypes';
import IPost from '../../models/post';
import IPostState from '../../models/modelState/postState';
import ISearchElement from '../../models/searchElement';

const initialState: IPostState = { 
  posts: [] as IPost[],
  filterPosts: [] as IPost[],
  search: {} as ISearchElement,
  editingPost: '' as string,
  expandingPost: '' as string,
  hasMorePosts: true,
  showUserByReaction: false,
  error: ''   
}

export const postReducer = (
  state = initialState,
  action: PostActionTypes
) => {
  switch (action.type) {
    case FETCH_POSTS_SUCCESS: {
      return {
        ...state, 
        posts: [...(state.posts || []), ...action.payload]
          .filter((post, index, self) =>
            index === self.findIndex(p => (
              p.id === post.id
        ))),
        hasMorePosts: Boolean(action.payload.length)        
      };
    }  
   
    case FETCH_POSTS_FILTER_SUCCESS: {
      return {
        ...state, 
        filterPosts: [...(state.filterPosts || []), ...action.payload]
        .filter((post, index, self) =>
            index === self.findIndex(p => (
              p.id === post.id
        ))),
        hasMorePosts: Boolean(action.payload.length)        
      };
    }  

    case CLEAR_POSTS_FILTER: {
      return {
        ...state, filterPosts: []
      }
    }

    case ADD_POST_SUCCESS: {
      return {
        ...state,
        posts: [action.payload, ...state.posts]
      };
    }

    case EDIT_POST_SUCCESS: 
    case LIKE_POST_SUCCESS: 
    case DISLIKE_POST_SUCCESS: {
      return {
        ...state,
        posts: [...state.posts
          .map(post => post.id === action.payload.id ? action.payload : post)
        ]
      }
    }

    case DELETE_POST_SUCCESS: {
      return {
        ...state,
        posts: [...state.posts
          .filter(post => post.id !== action.payload)
        ]
      }
    }
    
    case SET_EDITED_POST: {
      return {
        ...state,
        editingPost: action.payload
      }
    } 
    
    case CANCEL_EDITED_POST: {
      return {
        ...state,
        editingPost: ''
      }
    } 

    case SET_EXPANDED_POST: {
      return {
        ...state,
        expandingPost: action.payload
      }
    } 
    
    case CANCEL_EXPANDED_POST: {
      return {
        ...state,
        expandingPost: ''
      }
    } 

    case SHOW_USER_BY_POST: {
      return {
        ...state,
        showUserByReaction: true
      }
    }

    case HIDE_USER_BY_POST: {
      return {
        ...state,
        showUserByReaction: false
      }
    }

    case FETCH_POSTS_FAILURE: 
    case ADD_POST_FAILURE: 
    case EDIT_POST_FAILURE: 
    case DELETE_POST_FAILURE: 
    case LIKE_POST_FAILURE: 
    case DISLIKE_POST_FAILURE: 
    case SEND_SHARED_POST_FAILURE: {
      return {...state, error: action.payload};
    }
        
    default:
      return state;
  }
}