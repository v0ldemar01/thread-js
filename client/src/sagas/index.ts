import { all } from 'redux-saga/effects';
import postsSagas from './postSagas';
import commentsSagas from './commentSagas';
import usersSagas from './userSagas';

export default function* rootSaga() {
  yield all([
    postsSagas(),
    commentsSagas(),
    usersSagas()
  ]);
};