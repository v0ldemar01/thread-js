import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { 
	LOAD_USER,
  LOGIN_INIT,
  REGISTER_INIT,  
  EDIT_USER,
  FETCH_USERS,
	NOTIFY_USER,
	RESET_PASSWORD,
	CHANGE_PASSWORD,
	ILoginInitAction,
	IRegisterInitAction,
	IEditUserAction,	
	IFetchUsersAction,
	INotifyUserAction,
	IChangePasswordAction
} from '../actions/types/userTypes';
import { showLoader, hideLoader } from '../actions/mainActions';
import {
	loadUserSuccess,
	loadUserFailure,
  loginSuccess,
  loginFailure,  
  registerSuccess,
  registerFailure,   
  editUserSuccess,
  editUserFailure,   
  fetchUsersSuccess,
  fetchUsersFailure,
	notifyUserSuccess,
	notifyUserFailure,
	resetPasswordSuccess,
	resetPasswordFailure,
	changePasswordSuccess,
	changePasswordFailure
} from '../actions/userActions';
import { fetchPosts } from '../actions/postActions';
import { getFetchBody, getFetchHeaders, buildURLQuery } from '../helpers/webApiHelper';
import IInfoReact from '../models/infoReact';

const setToken = (token: string) => localStorage.setItem('token', token);

function* loadUserWorker() {
	try {
		yield put(showLoader());		
		const {data} = yield call(axios.get, `/api/auth/user`, getFetchHeaders({}));	
		yield put(loadUserSuccess(data))	
		yield put(fetchPosts({}));		
	} catch (error) {
    yield put(loadUserFailure(error.message));				
	} finally {
		yield put(hideLoader());
	}
}

function* watchLoadUser() {
	yield takeEvery(LOAD_USER, loadUserWorker)
}

function* loginUserWorker(action: ILoginInitAction) {
	try {
		yield put(showLoader());
		const {data} = yield call(axios.post, `/api/auth/login`, action.payload);
		setToken(data.token);
		yield put(loginSuccess(data.user));		
		yield put(fetchPosts({}));		
	} catch (error) {
    yield put(loginFailure(error.message));		
	} finally {
		yield put(hideLoader());
	}
}

function* watchLoginUser() {
	yield takeEvery(LOGIN_INIT, loginUserWorker)
}

function* registerUserWorker(action: IRegisterInitAction) {
	try {
		yield put(showLoader());
		const {data} = yield call(axios.post, `/api/auth/register`, action.payload);
		setToken(data.token);
		yield put(registerSuccess(data.user));
		yield put(fetchPosts({}));		
		yield put(hideLoader());
	} catch (error) {
    yield put(registerFailure(error.message));		
	}
}

function* watchRegisterUser() {
	yield takeEvery(REGISTER_INIT, registerUserWorker)
}

function* fetchUsersWorker(action: IFetchUsersAction) {
	try {	
		const {type, elementId, isLike}: IInfoReact = action.payload;
		const {data} = yield call(axios.get, `/api/${type}s/info/${elementId}?${buildURLQuery({isLike})}`, getFetchHeaders({}));
		yield put(fetchUsersSuccess(data));		
	} catch (error) {
    yield put(fetchUsersFailure(error.message));		
	} 
}

function* watchFetchUsers() {
	yield takeEvery(FETCH_USERS, fetchUsersWorker);
}

export function* editUserWorker(action: IEditUserAction) {
	const id = action.payload.id;
	const updatedUser = { ...action.payload };	
	try {
		const {data} = yield call(axios.put, `/api/users/${id}`, getFetchBody({request: updatedUser}), getFetchHeaders({}));
    yield put(editUserSuccess(data[0]));
  } catch (error) {
    yield put(editUserFailure(error.message));				
	}
}

function* watchEditUser() {
	yield takeEvery(EDIT_USER, editUserWorker)
}

export function* notifyUserWorker(action: INotifyUserAction) {	
	try {		
		yield call(axios.post, `/api/users/email`, getFetchBody({request: action.payload}), getFetchHeaders({}));
    yield put(notifyUserSuccess());
  } catch (error) {
    yield put(notifyUserFailure(error.message));				
	}
}

function* watchNotifyUser() {
	yield takeEvery(NOTIFY_USER, notifyUserWorker)
}

export function* resetPasswordWorker(action: INotifyUserAction) {	
	try {		
		yield call(axios.post, `/api/auth/forgotPassword`, getFetchBody({request: action.payload}), getFetchHeaders({}));
		yield put(resetPasswordSuccess());
	} catch (error) {
    yield put(resetPasswordFailure(error.message));				
	}
}

function* watchResetPassword() {
	yield takeEvery(RESET_PASSWORD, resetPasswordWorker)
}

export function* changePasswordWorker(action: IChangePasswordAction) {
	const token = action.payload.token;
	const newUser = action.payload.newUser;	
	try {		
		const {data} = yield call(axios.put, `/api/auth/changePassword/${token}`, getFetchBody({request: newUser}), getFetchHeaders({}));
		yield put(changePasswordSuccess(data));
	} catch (error) {
    yield put(changePasswordFailure(error.message));				
	}
}

function* watchChangePassword() {
	yield takeEvery(CHANGE_PASSWORD, changePasswordWorker)
}

export default function* userSagas() {
	yield all([
		watchLoadUser(),
    watchLoginUser(),
    watchRegisterUser(),
		watchFetchUsers(),
		watchEditUser(),
		watchNotifyUser(),
		watchResetPassword(),
		watchChangePassword()
	])
};
