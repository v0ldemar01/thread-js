import { call, put, takeEvery, all } from 'redux-saga/effects';
import axios from 'axios';
import { 
  FETCH_POSTS,
  ADD_POST,
  EDIT_POST,
  DELETE_POST,
  LIKE_POST,
  DISLIKE_POST,
  SHOW_USER_BY_POST,
  SET_EXPANDED_POST,
  SEND_SHARED_POST,  
  IFetchPostsAction,
  IAddPostAction,
  IEditPostAction,
  IDeletePostAction,
  ILikePostAction,
  IDisLikePostAction,
  IShowUserAction,
  ISetExpandedPost
} from '../actions/types/postTypes';
import {
  fetchPostsSuccess,
  fetchPostsFilterSuccess,
  clearPostsFilter,
  fetchPostsFailure,  
  addPostSuccess,
  addPostFailure,  
  cancelEditedPost,  
  editPostSuccess,
  editPostFailure,  
  deletePostSuccess,
  deletePostFailure,
  likePostSuccess,
  likePostFailure,
	dislikePostSuccess,
	dislikePostFailure,  
  sendSharedPostSuccess,
  sendSharedPostFailure
} from '../actions/postActions';
import { FETCH_COMMENTS } from '../actions/types/commentTypes';
import { FETCH_USERS, NOTIFY_USER } from '../actions/types/userTypes';
import { getFetchHeaders, getFetchBody, buildURLQuery } from '../helpers/webApiHelper';

function* fetchPostsWorker(action: IFetchPostsAction) {
	try {			
    const request = Object.fromEntries(Object.entries(action.payload).filter(([key, value]) => value));
    const url = Object.keys(request).length ? `/api/posts?${buildURLQuery(request)}` : '/api/posts';
		const {data} = yield call(axios.get, url, getFetchHeaders({}));	
    const {additional, userId} = action.payload;
    if (additional || userId)	{      
      yield put(fetchPostsFilterSuccess(data));	         
    } else {
      yield put(fetchPostsSuccess(data));	
      yield(put(clearPostsFilter()));	
    }		
	} catch (error) {
		yield put(fetchPostsFailure(error.message));	
	}
}

function* watchFetchPosts() {
	yield takeEvery(FETCH_POSTS, fetchPostsWorker);
}

function* addPostWorker(action: IAddPostAction) {
	try {
    const {data} = yield call(axios.post, `/api/posts/`, getFetchBody({request: action.payload}), getFetchHeaders({}));
		yield put(addPostSuccess(data));    	
	} catch (error) {    
    yield put(addPostFailure(error.message));		
	}
}

function* watchAddPost() {
	yield takeEvery(ADD_POST, addPostWorker);
}

export function* editPostWorker(action: IEditPostAction) {
  const id = action.payload.id;
	const updatedUser = { ...action.payload };	
	try {
		const {data} = yield call(axios.put, `/api/posts/${id}`, getFetchBody({ request: updatedUser }), getFetchHeaders({}));
    yield put(editPostSuccess(data));	    
    yield put(cancelEditedPost());
	} catch (error) {
    yield put(editPostFailure(error.message));				
	}
}

function* watchEditPost() {
	yield takeEvery(EDIT_POST, editPostWorker);
}

export function* deletePostWorker(action: IDeletePostAction) {
	try {
		// const {data} = yield call(axios.delete, `/api/messages/${action.payload}`, getFetchHeaders({}));
    // yield put(deletePostSuccess(data));
    yield put(deletePostSuccess(action.payload));
	} catch (error) {
		yield put(deletePostFailure(error.message));
	}
}

function* watchDeletePost() {
	yield takeEvery(DELETE_POST, deletePostWorker);
}

export function* likePostWorker(action: ILikePostAction) {
	try {
		const {data} = yield call(axios.put, `/api/posts/react`, getFetchBody({request: {postId: action.payload, isLike: true}}), getFetchHeaders({}));
    yield put(likePostSuccess(data));
    yield put({type: NOTIFY_USER, payload: {postId: action.payload, isLike: true}});
	} catch (error) {
		yield put(likePostFailure(error.message));
	}
}

function* watchLikePost() {
	yield takeEvery(LIKE_POST, likePostWorker);
}

export function* dislikePostWorker(action: IDisLikePostAction) {
	try {
		const {data} = yield call(axios.put, `/api/posts/react`, getFetchBody({request: {postId: action.payload, isDisLike: true}}), getFetchHeaders({}));
    yield put(dislikePostSuccess(data));
    yield put({type: NOTIFY_USER, payload: {postId: action.payload, isLike: false}});
	} catch (error) {
		yield put(dislikePostFailure(error.message));
	}
}

function* watchDislikePost() {
	yield takeEvery(DISLIKE_POST, dislikePostWorker);
}

export function* showUserPostWorker(action: IShowUserAction) {
	yield put({type: FETCH_USERS, payload: {type: 'post', elementId: action.payload.postId, isLike: action.payload.isLike as boolean}});
}

function* watchShowUserPost() {
	yield takeEvery(SHOW_USER_BY_POST, showUserPostWorker);
}

export function* showCommentsPostWorker(action: ISetExpandedPost) {
	yield put({type: FETCH_COMMENTS, payload: action.payload});
}

function* watchShowCommentsPost() {
	yield takeEvery(SET_EXPANDED_POST, showCommentsPostWorker);
}

export function* sendSharedPostWorker(action: ISetExpandedPost) {
	try {
    yield call(axios.post, '/api/users/shareEmail', getFetchBody({request: action.payload}), getFetchHeaders({}));
    yield put(sendSharedPostSuccess());
  } catch (error) {
    yield put(sendSharedPostFailure(error.message));
  }
}

function* watchSendSharedPost() {
	yield takeEvery(SEND_SHARED_POST, sendSharedPostWorker);
}

export default function* postSagas() {
	yield all([
		watchFetchPosts(),
		watchAddPost(),
		watchEditPost(),
		watchDeletePost(),
		watchLikePost(),
		watchDislikePost(),
    watchShowUserPost(),
    watchShowCommentsPost(),
    watchSendSharedPost()
	]);
};