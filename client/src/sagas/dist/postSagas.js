"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.sendSharedPostWorker = exports.showCommentsPostWorker = exports.showUserPostWorker = exports.dislikePostWorker = exports.likePostWorker = exports.deletePostWorker = exports.editPostWorker = void 0;
var effects_1 = require("redux-saga/effects");
var axios_1 = require("axios");
var postTypes_1 = require("../actions/types/postTypes");
var postActions_1 = require("../actions/postActions");
var commentTypes_1 = require("../actions/types/commentTypes");
var userTypes_1 = require("../actions/types/userTypes");
var webApiHelper_1 = require("../helpers/webApiHelper");
function fetchPostsWorker(action) {
    var request, url, data, _a, additional, userId, error_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 7, , 9]);
                request = Object.fromEntries(Object.entries(action.payload).filter(function (_a) {
                    var key = _a[0], value = _a[1];
                    return value;
                }));
                url = Object.keys(request).length ? "/api/posts?" + webApiHelper_1.buildURLQuery(request) : '/api/posts';
                return [4 /*yield*/, effects_1.call(axios_1["default"].get, url, webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_b.sent()).data;
                _a = action.payload, additional = _a.additional, userId = _a.userId;
                if (!(additional || userId)) return [3 /*break*/, 3];
                return [4 /*yield*/, effects_1.put(postActions_1.fetchPostsFilterSuccess(data))];
            case 2:
                _b.sent();
                return [3 /*break*/, 6];
            case 3: return [4 /*yield*/, effects_1.put(postActions_1.fetchPostsSuccess(data))];
            case 4:
                _b.sent();
                return [4 /*yield*/, (effects_1.put(postActions_1.clearPostsFilter()))];
            case 5:
                _b.sent();
                _b.label = 6;
            case 6: return [3 /*break*/, 9];
            case 7:
                error_1 = _b.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.fetchPostsFailure(error_1.message))];
            case 8:
                _b.sent();
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/];
        }
    });
}
function watchFetchPosts() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.FETCH_POSTS, fetchPostsWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function addPostWorker(action) {
    var data, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 5]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].post, "/api/posts/", webApiHelper_1.getFetchBody({ request: action.payload }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(postActions_1.addPostSuccess(data))];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                error_2 = _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.addPostFailure(error_2.message))];
            case 4:
                _a.sent();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}
function watchAddPost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.ADD_POST, addPostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function editPostWorker(action) {
    var id, updatedUser, data, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = action.payload.id;
                updatedUser = __assign({}, action.payload);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 5, , 7]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/posts/" + id, webApiHelper_1.getFetchBody({ request: updatedUser }), webApiHelper_1.getFetchHeaders({}))];
            case 2:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(postActions_1.editPostSuccess(data))];
            case 3:
                _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.cancelEditedPost())];
            case 4:
                _a.sent();
                return [3 /*break*/, 7];
            case 5:
                error_3 = _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.editPostFailure(error_3.message))];
            case 6:
                _a.sent();
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}
exports.editPostWorker = editPostWorker;
function watchEditPost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.EDIT_POST, editPostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function deletePostWorker(action) {
    var error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 4]);
                // const {data} = yield call(axios.delete, `/api/messages/${action.payload}`, getFetchHeaders({}));
                // yield put(deletePostSuccess(data));
                return [4 /*yield*/, effects_1.put(postActions_1.deletePostSuccess(action.payload))];
            case 1:
                // const {data} = yield call(axios.delete, `/api/messages/${action.payload}`, getFetchHeaders({}));
                // yield put(deletePostSuccess(data));
                _a.sent();
                return [3 /*break*/, 4];
            case 2:
                error_4 = _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.deletePostFailure(error_4.message))];
            case 3:
                _a.sent();
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}
exports.deletePostWorker = deletePostWorker;
function watchDeletePost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.DELETE_POST, deletePostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function likePostWorker(action) {
    var data, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 6]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/posts/react", webApiHelper_1.getFetchBody({ request: { postId: action.payload, isLike: true } }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(postActions_1.likePostSuccess(data))];
            case 2:
                _a.sent();
                return [4 /*yield*/, effects_1.put({ type: userTypes_1.NOTIFY_USER, payload: { postId: action.payload, isLike: true } })];
            case 3:
                _a.sent();
                return [3 /*break*/, 6];
            case 4:
                error_5 = _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.likePostFailure(error_5.message))];
            case 5:
                _a.sent();
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}
exports.likePostWorker = likePostWorker;
function watchLikePost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.LIKE_POST, likePostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function dislikePostWorker(action) {
    var data, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 6]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/posts/react", webApiHelper_1.getFetchBody({ request: { postId: action.payload, isDisLike: true } }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(postActions_1.dislikePostSuccess(data))];
            case 2:
                _a.sent();
                return [4 /*yield*/, effects_1.put({ type: userTypes_1.NOTIFY_USER, payload: { postId: action.payload, isLike: false } })];
            case 3:
                _a.sent();
                return [3 /*break*/, 6];
            case 4:
                error_6 = _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.dislikePostFailure(error_6.message))];
            case 5:
                _a.sent();
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}
exports.dislikePostWorker = dislikePostWorker;
function watchDislikePost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.DISLIKE_POST, dislikePostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function showUserPostWorker(action) {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.put({ type: userTypes_1.FETCH_USERS, payload: { type: 'post', elementId: action.payload.postId, isLike: action.payload.isLike } })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
exports.showUserPostWorker = showUserPostWorker;
function watchShowUserPost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.SHOW_USER_BY_POST, showUserPostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function showCommentsPostWorker(action) {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.put({ type: commentTypes_1.FETCH_COMMENTS, payload: action.payload })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
exports.showCommentsPostWorker = showCommentsPostWorker;
function watchShowCommentsPost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.SET_EXPANDED_POST, showCommentsPostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function sendSharedPostWorker(action) {
    var error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 5]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].post, '/api/users/shareEmail', webApiHelper_1.getFetchBody({ request: action.payload }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.sendSharedPostSuccess())];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                error_7 = _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.sendSharedPostFailure(error_7.message))];
            case 4:
                _a.sent();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}
exports.sendSharedPostWorker = sendSharedPostWorker;
function watchSendSharedPost() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(postTypes_1.SEND_SHARED_POST, sendSharedPostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function postSagas() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.all([
                    watchFetchPosts(),
                    watchAddPost(),
                    watchEditPost(),
                    watchDeletePost(),
                    watchLikePost(),
                    watchDislikePost(),
                    watchShowUserPost(),
                    watchShowCommentsPost(),
                    watchSendSharedPost()
                ])];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
exports["default"] = postSagas;
;
