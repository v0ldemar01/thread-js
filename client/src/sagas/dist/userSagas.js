"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.changePasswordWorker = exports.resetPasswordWorker = exports.notifyUserWorker = exports.editUserWorker = void 0;
var axios_1 = require("axios");
var effects_1 = require("redux-saga/effects");
var userTypes_1 = require("../actions/types/userTypes");
var mainActions_1 = require("../actions/mainActions");
var userActions_1 = require("../actions/userActions");
var postActions_1 = require("../actions/postActions");
var webApiHelper_1 = require("../helpers/webApiHelper");
var setToken = function (token) { return localStorage.setItem('token', token); };
function loadUserWorker() {
    var data, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, 7, 9]);
                return [4 /*yield*/, effects_1.put(mainActions_1.showLoader())];
            case 1:
                _a.sent();
                return [4 /*yield*/, effects_1.call(axios_1["default"].get, "/api/auth/user", webApiHelper_1.getFetchHeaders({}))];
            case 2:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(userActions_1.loadUserSuccess(data))];
            case 3:
                _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.fetchPosts({}))];
            case 4:
                _a.sent();
                return [3 /*break*/, 9];
            case 5:
                error_1 = _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.loadUserFailure(error_1.message))];
            case 6:
                _a.sent();
                return [3 /*break*/, 9];
            case 7: return [4 /*yield*/, effects_1.put(mainActions_1.hideLoader())];
            case 8:
                _a.sent();
                return [7 /*endfinally*/];
            case 9: return [2 /*return*/];
        }
    });
}
function watchLoadUser() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.LOAD_USER, loadUserWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function loginUserWorker(action) {
    var data, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, 7, 9]);
                return [4 /*yield*/, effects_1.put(mainActions_1.showLoader())];
            case 1:
                _a.sent();
                return [4 /*yield*/, effects_1.call(axios_1["default"].post, "/api/auth/login", action.payload)];
            case 2:
                data = (_a.sent()).data;
                setToken(data.token);
                return [4 /*yield*/, effects_1.put(userActions_1.loginSuccess(data.user))];
            case 3:
                _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.fetchPosts({}))];
            case 4:
                _a.sent();
                return [3 /*break*/, 9];
            case 5:
                error_2 = _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.loginFailure(error_2.message))];
            case 6:
                _a.sent();
                return [3 /*break*/, 9];
            case 7: return [4 /*yield*/, effects_1.put(mainActions_1.hideLoader())];
            case 8:
                _a.sent();
                return [7 /*endfinally*/];
            case 9: return [2 /*return*/];
        }
    });
}
function watchLoginUser() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.LOGIN_INIT, loginUserWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function registerUserWorker(action) {
    var data, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 8]);
                return [4 /*yield*/, effects_1.put(mainActions_1.showLoader())];
            case 1:
                _a.sent();
                return [4 /*yield*/, effects_1.call(axios_1["default"].post, "/api/auth/register", action.payload)];
            case 2:
                data = (_a.sent()).data;
                setToken(data.token);
                return [4 /*yield*/, effects_1.put(userActions_1.registerSuccess(data.user))];
            case 3:
                _a.sent();
                return [4 /*yield*/, effects_1.put(postActions_1.fetchPosts({}))];
            case 4:
                _a.sent();
                return [4 /*yield*/, effects_1.put(mainActions_1.hideLoader())];
            case 5:
                _a.sent();
                return [3 /*break*/, 8];
            case 6:
                error_3 = _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.registerFailure(error_3.message))];
            case 7:
                _a.sent();
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}
function watchRegisterUser() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.REGISTER_INIT, registerUserWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function fetchUsersWorker(action) {
    var _a, type, elementId, isLike, data, error_4;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 3, , 5]);
                _a = action.payload, type = _a.type, elementId = _a.elementId, isLike = _a.isLike;
                return [4 /*yield*/, effects_1.call(axios_1["default"].get, "/api/" + type + "s/info/" + elementId + "?" + webApiHelper_1.buildURLQuery({ isLike: isLike }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_b.sent()).data;
                return [4 /*yield*/, effects_1.put(userActions_1.fetchUsersSuccess(data))];
            case 2:
                _b.sent();
                return [3 /*break*/, 5];
            case 3:
                error_4 = _b.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.fetchUsersFailure(error_4.message))];
            case 4:
                _b.sent();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}
function watchFetchUsers() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.FETCH_USERS, fetchUsersWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function editUserWorker(action) {
    var id, updatedUser, data, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = action.payload.id;
                updatedUser = __assign({}, action.payload);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 6]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/users/" + id, webApiHelper_1.getFetchBody({ request: updatedUser }), webApiHelper_1.getFetchHeaders({}))];
            case 2:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(userActions_1.editUserSuccess(data[0]))];
            case 3:
                _a.sent();
                return [3 /*break*/, 6];
            case 4:
                error_5 = _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.editUserFailure(error_5.message))];
            case 5:
                _a.sent();
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}
exports.editUserWorker = editUserWorker;
function watchEditUser() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.EDIT_USER, editUserWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function notifyUserWorker(action) {
    var error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 5]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].post, "/api/users/email", webApiHelper_1.getFetchBody({ request: action.payload }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.notifyUserSuccess())];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                error_6 = _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.notifyUserFailure(error_6.message))];
            case 4:
                _a.sent();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}
exports.notifyUserWorker = notifyUserWorker;
function watchNotifyUser() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.NOTIFY_USER, notifyUserWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function resetPasswordWorker(action) {
    var error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 5]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].post, "/api/auth/forgotPassword", webApiHelper_1.getFetchBody({ request: action.payload }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.resetPasswordSuccess())];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                error_7 = _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.resetPasswordFailure(error_7.message))];
            case 4:
                _a.sent();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}
exports.resetPasswordWorker = resetPasswordWorker;
function watchResetPassword() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.RESET_PASSWORD, resetPasswordWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function changePasswordWorker(action) {
    var token, newUser, data, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                token = action.payload.token;
                newUser = action.payload.newUser;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 6]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/auth/changePassword/" + token, webApiHelper_1.getFetchBody({ request: newUser }), webApiHelper_1.getFetchHeaders({}))];
            case 2:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(userActions_1.changePasswordSuccess(data))];
            case 3:
                _a.sent();
                return [3 /*break*/, 6];
            case 4:
                error_8 = _a.sent();
                return [4 /*yield*/, effects_1.put(userActions_1.changePasswordFailure(error_8.message))];
            case 5:
                _a.sent();
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}
exports.changePasswordWorker = changePasswordWorker;
function watchChangePassword() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(userTypes_1.CHANGE_PASSWORD, changePasswordWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function userSagas() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.all([
                    watchLoadUser(),
                    watchLoginUser(),
                    watchRegisterUser(),
                    watchFetchUsers(),
                    watchEditUser(),
                    watchNotifyUser(),
                    watchResetPassword(),
                    watchChangePassword()
                ])];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
exports["default"] = userSagas;
;
