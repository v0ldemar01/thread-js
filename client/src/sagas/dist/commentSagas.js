"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.showUserPostWorker = exports.dislikeCommentWorker = exports.likeCommentWorker = exports.deleteCommentWorker = exports.editCommentWorker = void 0;
var effects_1 = require("redux-saga/effects");
var axios_1 = require("axios");
var commentTypes_1 = require("../actions/types/commentTypes");
var commentActions_1 = require("../actions/commentActions");
var userTypes_1 = require("../actions/types/userTypes");
var webApiHelper_1 = require("../helpers/webApiHelper");
function fetchCommentsWorker(action) {
    var data, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 5]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].get, "/api/comments?" + webApiHelper_1.buildURLQuery({ postId: action.payload }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(commentActions_1.fetchCommentsSuccess(data))];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                error_1 = _a.sent();
                return [4 /*yield*/, effects_1.put(commentActions_1.fetchCommentsFailure(error_1.message))];
            case 4:
                _a.sent();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}
function watchFetchComments() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(commentTypes_1.FETCH_COMMENTS, fetchCommentsWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function addCommentWorker(action) {
    var data, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 5]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].post, "/api/comments/", webApiHelper_1.getFetchBody({ request: action.payload }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(commentActions_1.addCommentSuccess(data))];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                error_2 = _a.sent();
                return [4 /*yield*/, effects_1.put(commentActions_1.addCommentFailure(error_2.message))];
            case 4:
                _a.sent();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}
function watchAddComment() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(commentTypes_1.ADD_COMMENT, addCommentWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function editCommentWorker(action) {
    var id, updatedUser, data, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = action.payload.id;
                updatedUser = __assign({}, action.payload);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 5, , 7]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/comments/" + id, webApiHelper_1.getFetchBody({ request: updatedUser }), webApiHelper_1.getFetchHeaders({}))];
            case 2:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(commentActions_1.editCommentSuccess(data))];
            case 3:
                _a.sent();
                return [4 /*yield*/, effects_1.put(commentActions_1.cancelEditedComment())];
            case 4:
                _a.sent();
                return [3 /*break*/, 7];
            case 5:
                error_3 = _a.sent();
                return [4 /*yield*/, effects_1.put(commentActions_1.editCommentFailure(error_3.message))];
            case 6:
                _a.sent();
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}
exports.editCommentWorker = editCommentWorker;
function watchEditComment() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(commentTypes_1.EDIT_COMMENT, editCommentWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function deleteCommentWorker(action) {
    var error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 4]);
                // const {data} = yield call(axios.delete, `/api/comments/${action.payload}`, getFetchHeaders({}));
                // yield put(deletePostSuccess(data));
                return [4 /*yield*/, effects_1.put(commentActions_1.deleteCommentSuccess(action.payload))];
            case 1:
                // const {data} = yield call(axios.delete, `/api/comments/${action.payload}`, getFetchHeaders({}));
                // yield put(deletePostSuccess(data));
                _a.sent();
                return [3 /*break*/, 4];
            case 2:
                error_4 = _a.sent();
                return [4 /*yield*/, effects_1.put(commentActions_1.deleteCommentFailure(error_4.message))];
            case 3:
                _a.sent();
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}
exports.deleteCommentWorker = deleteCommentWorker;
function watchDeleteComment() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(commentTypes_1.DELETE_COMMENT, deleteCommentWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function likeCommentWorker(action) {
    var data, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 6]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/comments/react", webApiHelper_1.getFetchBody({ request: { commentId: action.payload, isLike: true } }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(commentActions_1.likeCommentSuccess(data))];
            case 2:
                _a.sent();
                return [4 /*yield*/, effects_1.put({ type: userTypes_1.NOTIFY_USER, payload: { commentId: action.payload, isLike: true } })];
            case 3:
                _a.sent();
                return [3 /*break*/, 6];
            case 4:
                error_5 = _a.sent();
                return [4 /*yield*/, effects_1.put(commentActions_1.likeCommentFailure(error_5.message))];
            case 5:
                _a.sent();
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}
exports.likeCommentWorker = likeCommentWorker;
function watchLikeComment() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(commentTypes_1.LIKE_COMMENT, likeCommentWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function dislikeCommentWorker(action) {
    var data, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 6]);
                return [4 /*yield*/, effects_1.call(axios_1["default"].put, "/api/comments/react", webApiHelper_1.getFetchBody({ request: { commentId: action.payload, isDisLike: true } }), webApiHelper_1.getFetchHeaders({}))];
            case 1:
                data = (_a.sent()).data;
                return [4 /*yield*/, effects_1.put(commentActions_1.dislikeCommentSuccess(data))];
            case 2:
                _a.sent();
                return [4 /*yield*/, effects_1.put({ type: userTypes_1.NOTIFY_USER, payload: { commentId: action.payload, isLike: false } })];
            case 3:
                _a.sent();
                return [3 /*break*/, 6];
            case 4:
                error_6 = _a.sent();
                return [4 /*yield*/, effects_1.put(commentActions_1.dislikeCommentFailure(error_6.message))];
            case 5:
                _a.sent();
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}
exports.dislikeCommentWorker = dislikeCommentWorker;
function watchDislikeComment() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(commentTypes_1.DISLIKE_COMMENT, dislikeCommentWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function showUserPostWorker(action) {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.put({ type: userTypes_1.FETCH_USERS, payload: { type: 'comment', elementId: action.payload.commentId, isLike: action.payload.isLike } })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
exports.showUserPostWorker = showUserPostWorker;
function watchShowUserComment() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.takeEvery(commentTypes_1.SHOW_USER_BY_COMMENT, showUserPostWorker)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
function commentSagas() {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, effects_1.all([
                    watchFetchComments(),
                    watchAddComment(),
                    watchEditComment(),
                    watchDeleteComment(),
                    watchLikeComment(),
                    watchDislikeComment(),
                    watchShowUserComment()
                ])];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}
exports["default"] = commentSagas;
;
