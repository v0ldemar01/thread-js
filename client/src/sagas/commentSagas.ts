import { call, put, takeEvery, all } from 'redux-saga/effects';
import axios from 'axios';
import { 
  FETCH_COMMENTS,
  ADD_COMMENT,
  EDIT_COMMENT,
  DELETE_COMMENT,
  LIKE_COMMENT,
  DISLIKE_COMMENT,
  SHOW_USER_BY_COMMENT,
  IFetchCommentsAction,
  IAddCommentAction,
  IEditCommentAction,
  IDeleteCommentAction,
  ILikeCommentAction,
  IDisLikeCommentAction,
  IShowUserAction
} from '../actions/types/commentTypes';
import {
  fetchCommentsSuccess,
  fetchCommentsFailure,
  addCommentSuccess,
  addCommentFailure,  
  cancelEditedComment,  
  editCommentSuccess,
  editCommentFailure,  
  deleteCommentSuccess,
  deleteCommentFailure,
  likeCommentSuccess,
  likeCommentFailure,
	dislikeCommentSuccess,
	dislikeCommentFailure
} from '../actions/commentActions';
import { FETCH_USERS, NOTIFY_USER } from '../actions/types/userTypes';
import { getFetchHeaders, getFetchBody, buildURLQuery } from '../helpers/webApiHelper';

function* fetchCommentsWorker(action: IFetchCommentsAction) {
	try {			
    const {data} = yield call(axios.get, `/api/comments?${buildURLQuery({postId: action.payload})}`, getFetchHeaders({}));	
    yield put(fetchCommentsSuccess(data));	
	} catch (error) {
		yield put(fetchCommentsFailure(error.message));	
	}
}

function* watchFetchComments() {
	yield takeEvery(FETCH_COMMENTS, fetchCommentsWorker);
}

function* addCommentWorker(action: IAddCommentAction) {
	try {
    const {data} = yield call(axios.post, `/api/comments/`, getFetchBody({request: action.payload}), getFetchHeaders({}));
		yield put(addCommentSuccess(data));    	
	} catch (error) {    
    yield put(addCommentFailure(error.message));		
	}
}

function* watchAddComment() {
	yield takeEvery(ADD_COMMENT, addCommentWorker);
}

export function* editCommentWorker(action: IEditCommentAction) {
  const id = action.payload.id;
	const updatedUser = { ...action.payload };	
	try {
		const {data} = yield call(axios.put, `/api/comments/${id}`, getFetchBody({ request: updatedUser }), getFetchHeaders({}));
    yield put(editCommentSuccess(data));	    
    yield put(cancelEditedComment());
	} catch (error) {
    yield put(editCommentFailure(error.message));				
	}
}

function* watchEditComment() {
	yield takeEvery(EDIT_COMMENT, editCommentWorker);
}

export function* deleteCommentWorker(action: IDeleteCommentAction) {
	try {
		// const {data} = yield call(axios.delete, `/api/comments/${action.payload}`, getFetchHeaders({}));
    // yield put(deletePostSuccess(data));
    yield put(deleteCommentSuccess(action.payload));
	} catch (error) {
		yield put(deleteCommentFailure(error.message));
	}
}

function* watchDeleteComment() {
	yield takeEvery(DELETE_COMMENT, deleteCommentWorker);
}

export function* likeCommentWorker(action: ILikeCommentAction) {
	try {
		const {data} = yield call(axios.put, `/api/comments/react`, getFetchBody({request: {commentId: action.payload, isLike: true}}), getFetchHeaders({}));
    yield put(likeCommentSuccess(data));
		yield put({type: NOTIFY_USER, payload: {commentId: action.payload, isLike: true}});
	} catch (error) {
		yield put(likeCommentFailure(error.message));
	}
}

function* watchLikeComment() {
	yield takeEvery(LIKE_COMMENT, likeCommentWorker);
}

export function* dislikeCommentWorker(action: IDisLikeCommentAction) {
	try {
		const {data} = yield call(axios.put, `/api/comments/react`, getFetchBody({request: {commentId: action.payload, isDisLike: true}}), getFetchHeaders({}));
    yield put(dislikeCommentSuccess(data));
		yield put({type: NOTIFY_USER, payload: {commentId: action.payload, isLike: false}});
	} catch (error) {
		yield put(dislikeCommentFailure(error.message));
	}
}

function* watchDislikeComment() {
	yield takeEvery(DISLIKE_COMMENT, dislikeCommentWorker);
}

export function* showUserPostWorker(action: IShowUserAction) {
	yield put({type: FETCH_USERS, payload: {type: 'comment', elementId: action.payload.commentId, isLike: action.payload.isLike as boolean}});
}

function* watchShowUserComment() {
	yield takeEvery(SHOW_USER_BY_COMMENT, showUserPostWorker);
}

export default function* commentSagas() {
	yield all([
		watchFetchComments(),
		watchAddComment(),
		watchEditComment(),
		watchDeleteComment(),
		watchLikeComment(),
		watchDislikeComment(),
    watchShowUserComment()
	]);
};