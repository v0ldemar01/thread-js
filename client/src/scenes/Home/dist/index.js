"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var connected_react_router_1 = require("connected-react-router");
var Routing_1 = require("../../containers/Routing");
var store_1 = require("../../store");
var history_1 = require("../../history");
var Home = function () { return (react_1["default"].createElement(react_1["default"].StrictMode, null,
    react_1["default"].createElement(react_redux_1.Provider, { store: store_1["default"] },
        react_1["default"].createElement(connected_react_router_1.ConnectedRouter, { history: history_1["default"] },
            react_1["default"].createElement(Routing_1["default"], null))))); };
exports["default"] = Home;
