import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import Routing from '../../containers/Routing';
import store  from '../../store';
import history from '../../history';

const Home: React.FunctionComponent = () => (
  <React.StrictMode>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Routing />
      </ConnectedRouter>
    </Provider>
  </React.StrictMode>  
);

export default Home;
