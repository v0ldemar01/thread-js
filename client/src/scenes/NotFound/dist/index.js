"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var semantic_ui_react_1 = require("semantic-ui-react");
var NotFound = function () { return (react_1["default"].createElement(semantic_ui_react_1.Header, { as: "h2", icon: true, textAlign: "center", style: { marginTop: 50 } },
    react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "frown", circular: true }),
    react_1["default"].createElement(semantic_ui_react_1.Header.Content, null,
        react_1["default"].createElement("div", null, "404 Not Found"),
        'Go to ',
        react_1["default"].createElement(react_router_dom_1.NavLink, { to: "/" }, "Home"),
        ' page'))); };
exports["default"] = NotFound;
