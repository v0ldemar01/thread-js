import IHeader from '../models/header';

export const buildURLQuery = (obj: any) =>
	Object.entries(obj)
		.map(pair => pair.map((element: any) => encodeURIComponent(element)).join('='))
		.join('&');

export const getFetchHeaders = (args: any) => {
  const headers = {} as IHeader;
  if (!args.attachment) {
    headers['Content-Type'] = 'application/json';
    headers.Accept = 'application/json';
  }
  const token = localStorage.getItem('token');
  if (token && !args.skipAuthorization) {
    headers.Authorization = `Bearer ${token}`;
  }
  return { headers };
}

export const getFetchBody = (args: any) => {
  let body;
  if (args.attachment) {
    if (args.type === 'GET') {
      throw new Error('GET request does not support attachments.');
    }
    const formData = new FormData();
    formData.append('image', args.attachment);
    body = formData;
  } else if (args.request) {
    if (args.type === 'GET') {
      throw new Error('GET request does not support request body.');
    }
    body = JSON.stringify(args.request);
  }
  return body;
}

