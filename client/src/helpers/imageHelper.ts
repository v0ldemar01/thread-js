import IIMage from "../models/image";

export const getUserImgLink = (image: IIMage | null) => (image
  ? image.link
  : 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png');
