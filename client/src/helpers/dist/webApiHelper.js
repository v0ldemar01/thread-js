"use strict";
exports.__esModule = true;
exports.getFetchBody = exports.getFetchHeaders = exports.buildURLQuery = void 0;
exports.buildURLQuery = function (obj) {
    return Object.entries(obj)
        .map(function (pair) { return pair.map(function (element) { return encodeURIComponent(element); }).join('='); })
        .join('&');
};
exports.getFetchHeaders = function (args) {
    var headers = {};
    if (!args.attachment) {
        headers['Content-Type'] = 'application/json';
        headers.Accept = 'application/json';
    }
    var token = localStorage.getItem('token');
    if (token && !args.skipAuthorization) {
        headers.Authorization = "Bearer " + token;
    }
    return { headers: headers };
};
exports.getFetchBody = function (args) {
    var body;
    if (args.attachment) {
        if (args.type === 'GET') {
            throw new Error('GET request does not support attachments.');
        }
        var formData = new FormData();
        formData.append('image', args.attachment);
        body = formData;
    }
    else if (args.request) {
        if (args.type === 'GET') {
            throw new Error('GET request does not support request body.');
        }
        body = JSON.stringify(args.request);
    }
    return body;
};
