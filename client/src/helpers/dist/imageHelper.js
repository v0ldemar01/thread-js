"use strict";
exports.__esModule = true;
exports.getUserImgLink = void 0;
exports.getUserImgLink = function (image) { return (image
    ? image.link
    : 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png'); };
