"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.throwIfResponseFailed = throwIfResponseFailed;
exports["default"] = callWebApi;

var queryString = _interopRequireWildcard(require("src"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function getFetchUrl(args) {
  return args.endpoint + (args.query ? "?".concat(queryString.stringify(args.query)) : '');
}

function getFetchArgs(args) {
  var headers = {};

  if (!args.attachment) {
    headers['Content-Type'] = 'application/json';
    headers.Accept = 'application/json';
  }

  var token = localStorage.getItem('token');

  if (token && !args.skipAuthorization) {
    headers.Authorization = "Bearer ".concat(token);
  }

  var body;

  if (args.attachment) {
    if (args.type === 'GET') {
      throw new Error('GET request does not support attachments.');
    }

    var formData = new FormData();
    formData.append('image', args.attachment);
    body = formData;
  } else if (args.request) {
    if (args.type === 'GET') {
      throw new Error('GET request does not support request body.');
    }

    body = JSON.stringify(args.request);
  }

  return _objectSpread({
    method: args.type,
    headers: headers,
    signal: args.ct
  }, args.request === 'GET' ? {} : {
    body: body
  });
}

function throwIfResponseFailed(res) {
  var parsedException;
  return regeneratorRuntime.async(function throwIfResponseFailed$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          if (res.ok) {
            _context.next = 11;
            break;
          }

          parsedException = 'Something went wrong with request!';
          _context.prev = 2;
          _context.next = 5;
          return regeneratorRuntime.awrap(res.json());

        case 5:
          parsedException = _context.sent;
          _context.next = 10;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](2);

        case 10:
          throw parsedException;

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[2, 8]]);
}

function callWebApi(args) {
  var res;
  return regeneratorRuntime.async(function callWebApi$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(fetch(getFetchUrl(args), getFetchArgs(args)));

        case 2:
          res = _context2.sent;
          _context2.next = 5;
          return regeneratorRuntime.awrap(throwIfResponseFailed(res));

        case 5:
          return _context2.abrupt("return", res);

        case 6:
        case "end":
          return _context2.stop();
      }
    }
  });
}