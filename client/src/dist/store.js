"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.history = void 0;
var redux_1 = require("redux");
var connected_react_router_1 = require("connected-react-router");
var redux_thunk_1 = require("redux-thunk");
var history_1 = require("history");
var redux_devtools_extension_1 = require("redux-devtools-extension");
var reducer_1 = require("./containers/Thread/reducer");
var reducer_2 = require("./containers/Profile/reducer");
exports.history = history_1.createBrowserHistory();
var initialState = {};
var middlewares = [
    redux_thunk_1["default"],
    connected_react_router_1.routerMiddleware(exports.history)
];
var composedEnhancers = redux_1.compose(redux_1.applyMiddleware.apply(void 0, middlewares), redux_devtools_extension_1.composeWithDevTools());
var reducers = {
    posts: reducer_1["default"],
    profile: reducer_2["default"]
};
var rootReducer = redux_1.combineReducers(__assign({ router: connected_react_router_1.connectRouter(exports.history) }, reducers));
var store = redux_1.createStore(rootReducer, initialState, composedEnhancers);
exports["default"] = store;
