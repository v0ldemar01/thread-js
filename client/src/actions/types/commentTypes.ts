import IHttpError from '../../models/error';
import IComment from '../../models/comment';
import ICommentReact from '../../models/commentReact';

export const FETCH_COMMENTS = 'FETCH_COMMENTS';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';
export const ADD_COMMENT = 'ADD_COMMENT';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';
export const SET_EDITED_COMMENT = 'SET_EDITED_COMMENT';
export const CANCEL_EDITED_COMMENT = 'CANCEL_EDITED_COMMENT';
export const EDIT_COMMENT = 'EDIT_COMMENT';
export const EDIT_COMMENT_SUCCESS = 'EDIT_COMMENT_SUCCESS';
export const EDIT_COMMENT_FAILURE = 'EDIT_COMMENT_FAILURE';
export const DELETE_COMMENT = 'DELETE_COMMENT';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_FAILURE = 'DELETE_COMMENT_FAILURE';
export const LIKE_COMMENT = 'LIKE_COMMENT';
export const LIKE_COMMENT_SUCCESS = 'LIKE_COMMENT_SUCCESS';
export const LIKE_COMMENT_FAILURE = 'LIKE_COMMENT_FAILURE';
export const DISLIKE_COMMENT = 'DISLIKE_COMMENT';
export const DISLIKE_COMMENT_SUCCESS = 'DISLIKE_COMMENT_SUCCESS';
export const DISLIKE_COMMENT_FAILURE = 'DISLIKE_COMMENT_FAILURE';
export const SHOW_USER_BY_COMMENT = 'SHOW_USER_BY_COMMENT';
export const HIDE_USER_BY_COMMENT = 'HIDE_USER_BY_COMMENT';

export interface IFetchCommentsAction {
  type: typeof FETCH_COMMENTS; 
  payload: string; 
}

export interface IFetchCommentsSuccessAction {
  type: typeof FETCH_COMMENTS_SUCCESS;
  payload: IComment[];
}

export interface IFetchCommentsFailureAction {
  type: typeof FETCH_COMMENTS_FAILURE;
  payload: IHttpError;
}

export interface IAddCommentAction {
  type: typeof ADD_COMMENT;
  payload: IComment;
}

export interface IAddCommentSuccessAction {
  type: typeof ADD_COMMENT_SUCCESS;
  payload: IComment;
}

export interface IAddCommentFailureAction {
  type: typeof ADD_COMMENT_FAILURE;
  payload: IHttpError;
}

export interface ISetEditedCommentAction {
  type: typeof SET_EDITED_COMMENT;
  payload: IComment;
}

export interface ICancelEditedCommentAction {
  type: typeof CANCEL_EDITED_COMMENT;
}

export interface IEditCommentAction {
  type: typeof EDIT_COMMENT;
  payload: IComment;
}

export interface IEditCommentSuccessAction {
  type: typeof EDIT_COMMENT_SUCCESS;
  payload: IComment;
}

export interface IEditCommentFailureAction {
  type: typeof EDIT_COMMENT_FAILURE;
  payload: IHttpError;
}

export interface IDeleteCommentAction {
  type: typeof DELETE_COMMENT;
  payload: string;
}

export interface IDeleteCommentSuccessAction {
  type: typeof DELETE_COMMENT_SUCCESS;
  payload: string;
}

export interface IDeleteCommentFailureAction {
  type: typeof DELETE_COMMENT_FAILURE;
  payload: IHttpError;
}

export interface ILikeCommentAction {
  type: typeof LIKE_COMMENT;
  payload: ICommentReact;
}

export interface ILikeCommentSuccessAction {
  type: typeof LIKE_COMMENT_SUCCESS;
  payload: IComment;
}

export interface ILikeCommentFailureAction {
  type: typeof LIKE_COMMENT_FAILURE;
  payload: IHttpError;
}

export interface IDisLikeCommentAction {
  type: typeof DISLIKE_COMMENT;
  payload: ICommentReact;
}

export interface IDisLikeCommentSuccessAction {
  type: typeof DISLIKE_COMMENT_SUCCESS;
  payload: IComment;
}

export interface IDisLikeCommentFailureAction {
  type: typeof DISLIKE_COMMENT_FAILURE;
  payload: IHttpError;
}

export interface IShowUserAction {
  type: typeof SHOW_USER_BY_COMMENT;
  payload: ICommentReact;
}

export interface IHideUserAction {
  type: typeof HIDE_USER_BY_COMMENT;
  payload: IComment;
}

export type CommentActionTypes = 
  IFetchCommentsAction |
  IFetchCommentsSuccessAction |
  IFetchCommentsFailureAction |
  IAddCommentAction |
  IAddCommentSuccessAction |
  IAddCommentFailureAction |
  ISetEditedCommentAction |
  ICancelEditedCommentAction |
  IEditCommentAction |
  IEditCommentSuccessAction |
  IEditCommentFailureAction |
  IDeleteCommentAction |
  IDeleteCommentSuccessAction |
  IDeleteCommentFailureAction | 
  ILikeCommentAction |
  ILikeCommentSuccessAction | 
  ILikeCommentFailureAction |
  IDisLikeCommentAction |
  IDisLikeCommentSuccessAction | 
  IDisLikeCommentFailureAction |
  IShowUserAction |
  IHideUserAction;
  