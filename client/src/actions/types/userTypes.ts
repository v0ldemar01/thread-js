import IUser from '../../models/user';
import IHttpError from '../../models/error';
import IInfoReact from '../../models/infoReact';
import INotify from '../../models/notify';
import IChangePassword from '../../models/changePassword';

export const LOAD_USER = 'LOAD_USER';
export const LOAD_USER_SUCCESS = 'LOAD_USER_SUCCESS';
export const LOAD_USER_FAILURE = 'LOAD_USER_FAILURE';
export const LOGIN_INIT = 'LOGIN_INIT';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const REGISTER_INIT = 'REGISTER_INIT';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';
export const SET_EDITED_USER = 'SET_EDITED_POST';
export const CANCEL_EDITED_USER = 'CANCEL_EDITED_POST';
export const EDIT_USER = 'EDIT_USER';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';
export const EDIT_USER_FAILURE = 'EDIT_USER_FAILURE';
export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';
export const SEND_EMAIL = 'SEND_EMAIL';
export const SEND_EMAIL_SUCCESS = 'SEND_EMAIL_SUCCESS';
export const SEND_EMAIL_FAILURE = 'SEND_EMAIL_FAILURE';
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_FAILURE = 'RESET_PASSWORD_FAILURE';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAILURE = 'CHANGE_PASSWORD_FAILURE';
export const NOTIFY_USER = 'NOTIFY_USER';
export const NOTIFY_USER_SUCCESS = 'NOTIFY_USER_SUCCESS';
export const NOTIFY_USER_FAILURE = 'NOTIFY_USER_FAILURE';

export interface ILoadUserAction {
  type: typeof LOAD_USER; 
}

export interface ILoginInitAction {
  type: typeof LOGIN_INIT;
  payload: IUser;
}

export interface IRegisterInitAction {
  type: typeof REGISTER_INIT;
  payload: IUser;
}

export interface ILoadUserSuccessAction {
  type: typeof LOAD_USER_SUCCESS;
  payload: IUser;
}

export interface ILoginSuccessAction {
  type: typeof LOGIN_SUCCESS;
  payload: IUser;
}

export interface IRegisterSuccessAction {
  type: typeof REGISTER_SUCCESS;
  payload: IUser;
}

export interface ILoadUserFailureAction {
  type: typeof LOAD_USER_FAILURE;
  payload: IHttpError;
}

export interface ILoginFailureAction {
  type: typeof LOGIN_FAILURE;
  payload: IHttpError;
}

export interface IRegisterFailureAction {
  type: typeof REGISTER_FAILURE;
  payload: IHttpError;
}

export interface ILogOutUserAction {
  type: typeof LOGOUT_USER;  
}

export interface ISetEditUserAction {
  type: typeof SET_EDITED_USER;
  payload: IUser;
}
export interface ICancelEditUserAction {
  type: typeof CANCEL_EDITED_USER;  
}

export interface IEditUserAction {
  type: typeof EDIT_USER;
  payload: IUser;
}

export interface IEditUserSuccessAction {
  type: typeof EDIT_USER_SUCCESS;
  payload: IUser;
}

export interface IEditUserFailureAction {
  type: typeof EDIT_USER_FAILURE;
  payload: IHttpError;
}

export interface IFetchUsersAction {
  type: typeof FETCH_USERS;
  payload: IInfoReact;
}

export interface IFetchUsersSuccessAction {
  type: typeof FETCH_USERS_SUCCESS;
  payload: IUser[];
}

export interface IFetchUsersFailureAction {
  type: typeof FETCH_USERS_FAILURE;
  payload: IHttpError;
}

export interface ISendEmailAction {
  type: typeof SEND_EMAIL;
  payload: IUser;
}

export interface ISendEmailSuccessAction {
  type: typeof SEND_EMAIL_SUCCESS;
}

export interface ISendEmailFailureAction {
  type: typeof SEND_EMAIL_FAILURE;
  payload: IHttpError;
}

export interface IResetPasswordAction {
  type: typeof RESET_PASSWORD;
  payload: IUser;
}

export interface IResetPasswordSuccessAction {
  type: typeof RESET_PASSWORD_SUCCESS;
}

export interface IResetPasswordFailureAction {
  type: typeof RESET_PASSWORD_FAILURE;
  payload: IHttpError;
}

export interface IChangePasswordAction {
  type: typeof CHANGE_PASSWORD;
  payload: IChangePassword;
}

export interface IChangePasswordSuccessAction {
  type: typeof CHANGE_PASSWORD_SUCCESS;
  payload: IUser;
}

export interface IChangePasswordFailureAction {
  type: typeof CHANGE_PASSWORD_FAILURE;
  payload: IHttpError;
}

export interface INotifyUserAction {
  type: typeof NOTIFY_USER;
  payload: INotify;
}

export interface INotifyUserSuccessAction {
  type: typeof NOTIFY_USER_SUCCESS;
}

export interface INotifyUserFailureAction {
  type: typeof NOTIFY_USER_FAILURE;
  payload: IHttpError;
}

export type UserActionTypes =  
  ILoadUserAction |
  ILoginInitAction | 
  IRegisterInitAction |
  ILoadUserSuccessAction |
  ILoginSuccessAction |
  IRegisterSuccessAction | 
  ILoadUserFailureAction |
  ILoginFailureAction | 
  IRegisterFailureAction | 
  ILogOutUserAction | 
  ISetEditUserAction |
  ICancelEditUserAction |
  IEditUserAction |
  IEditUserSuccessAction |
  IEditUserFailureAction |
  IFetchUsersAction |
  IFetchUsersSuccessAction |
  IFetchUsersFailureAction |
  ISendEmailAction | 
  ISendEmailSuccessAction |
  ISendEmailFailureAction |
  IResetPasswordAction |
  IResetPasswordSuccessAction |
  IResetPasswordFailureAction |
  IChangePasswordAction |
  IChangePasswordSuccessAction |
  IChangePasswordFailureAction | 
  INotifyUserAction |
  INotifyUserSuccessAction |
  INotifyUserFailureAction;
  