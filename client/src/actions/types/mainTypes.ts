export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';

interface IShowLoaderAction {
  type: typeof SHOW_LOADER  
}

interface IHideLoaderAction {
  type: typeof HIDE_LOADER  
}

export type MainActionTypes = 
  IShowLoaderAction |
  IHideLoaderAction;