import IHttpError from '../../models/error';
import IPost from '../../models/post';
import IPostReact from '../../models/postReact';
import IFilterPosts from '../../models/postFilter';
import ISharedPostUser from '../../models/shareEmailUser';

export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FILTER_SUCCESS = 'FETCH_POSTS_FILTER_SUCCESS';
export const CLEAR_POSTS = 'CLEAR_POSTS';
export const CLEAR_POSTS_FILTER = 'CLEAR_POSTS_FILTER';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const ADD_POST = 'ADD_POST';
export const ADD_POST_SUCCESS = 'ADD_POST_SUCCESS';
export const ADD_POST_FAILURE = 'ADD_POST_FAILURE';
export const SET_EDITED_POST = 'SET_EDITED_POST';
export const CANCEL_EDITED_POST = 'CANCEL_EDITED_POST';
export const SET_EXPANDED_POST = 'SET_EXPANDED_POST';
export const CANCEL_EXPANDED_POST = 'CANCEL_EXPANDED_POST';
export const EDIT_POST = 'EDIT_POST';
export const EDIT_POST_SUCCESS = 'EDIT_POST_SUCCESS';
export const EDIT_POST_FAILURE = 'EDIT_POST_FAILURE';
export const DELETE_POST = 'DELETE_POST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';
export const LIKE_POST = 'LIKE_POST';
export const LIKE_POST_SUCCESS = 'LIKE_POST_SUCCESS';
export const LIKE_POST_FAILURE = 'LIKE_POST_FAILURE';
export const DISLIKE_POST = 'DISLIKE_POST';
export const DISLIKE_POST_SUCCESS = 'DISLIKE_POST_SUCCESS';
export const DISLIKE_POST_FAILURE = 'DISLIKE_POST_FAILURE';
export const SHOW_USER_BY_POST = 'SHOW_USER_BY_POST';
export const HIDE_USER_BY_POST = 'HIDE_USER_BY_POST';
export const SEND_SHARED_POST = 'SEND_SHARED_POST';
export const SEND_SHARED_POST_SUCCESS = 'SEND_SHARED_POST_SUCCESS';
export const SEND_SHARED_POST_FAILURE = 'SEND_SHARED_POST_FAILURE';

export const SET_ALL_POSTS = 'POST_ACTION:SET_ALL_POSTS';

export interface IFetchPostsAction {
  type: typeof FETCH_POSTS;
  payload: IFilterPosts;
}

export interface IFetchPostsSuccessAction {
  type: typeof FETCH_POSTS_SUCCESS;
  payload: IPost[];
}

export interface IFetchPostsFilterSuccessAction {
  type: typeof FETCH_POSTS_FILTER_SUCCESS;
  payload: IPost[];
}

export interface IClearPostsAction {
  type: typeof CLEAR_POSTS;  
}

export interface IClearPostsFilterAction {
  type: typeof CLEAR_POSTS_FILTER;  
}

export interface IFetchPostsFailureAction {
  type: typeof FETCH_POSTS_FAILURE;
  payload: IHttpError;
}

export interface IAddPostAction {
  type: typeof ADD_POST;
  payload: IPost;
}

export interface IAddPostSuccessAction {
  type: typeof ADD_POST_SUCCESS;
  payload: IPost;
}

export interface IAddPostFailureAction {
  type: typeof ADD_POST_FAILURE;
  payload: IHttpError;
}

export interface ISetEditedPost {
  type: typeof SET_EDITED_POST;
  payload: string;
}

export interface ICancelEditedPost {
  type: typeof CANCEL_EDITED_POST;  
}

export interface ISetExpandedPost {
  type: typeof SET_EXPANDED_POST;
  payload: string;
}

export interface ICancelExpandedPost {
  type: typeof CANCEL_EXPANDED_POST  
}

export interface IEditPostAction {
  type: typeof EDIT_POST;
  payload: IPost;
}

export interface IEditPostSuccessAction {
  type: typeof EDIT_POST_SUCCESS;
  payload: IPost;
}

export interface IEditPostFailureAction {
  type: typeof EDIT_POST_FAILURE;
  payload: IHttpError;
}

export interface IDeletePostAction {
  type: typeof DELETE_POST;
  payload: string;
}

export interface IDeletePostSuccessAction {
  type: typeof DELETE_POST_SUCCESS;
  payload: string;
}

export interface IDeletePostFailureAction {
  type: typeof DELETE_POST_FAILURE;
  payload: IHttpError;
}

export interface ILikePostAction {
  type: typeof LIKE_POST;
  payload: IPostReact;
}

export interface ILikePostSuccessAction {
  type: typeof LIKE_POST_SUCCESS;
  payload: IPost;
}

export interface ILikePostFailureAction {
  type: typeof LIKE_POST_FAILURE;
  payload: IHttpError;
}

export interface IDisLikePostAction {
  type: typeof DISLIKE_POST;
  payload: IPostReact;
}

export interface IDisLikePostSuccessAction {
  type: typeof DISLIKE_POST_SUCCESS;
  payload: IPost;
}

export interface IDisLikePostFailureAction {
  type: typeof DISLIKE_POST_FAILURE;
  payload: IHttpError;
}

export interface IShowUserAction {
  type: typeof SHOW_USER_BY_POST;
  payload: IPostReact;
}

export interface IHideUserAction {
  type: typeof HIDE_USER_BY_POST;  
}

export interface ISendSharedPostAction {
  type: typeof SEND_SHARED_POST;
  payload: ISharedPostUser;
}

export interface ISendSharedPostSuccessAction {
  type: typeof SEND_SHARED_POST_SUCCESS;
}

export interface ISendSharedPostFailureAction {
  type: typeof SEND_SHARED_POST_FAILURE;
  payload: IHttpError;
}

export type PostActionTypes = 
  IFetchPostsAction |
  IFetchPostsSuccessAction |
  IFetchPostsFilterSuccessAction |
  IClearPostsAction | 
  IClearPostsFilterAction |
  IFetchPostsFailureAction |
  IAddPostAction |
  IAddPostSuccessAction |
  IAddPostFailureAction |
  ISetEditedPost |
  ICancelEditedPost |
  ISetExpandedPost |
  ICancelExpandedPost |
  IEditPostAction |
  IEditPostSuccessAction |
  IEditPostFailureAction |
  IDeletePostAction |
  IDeletePostSuccessAction |
  IDeletePostFailureAction | 
  ILikePostAction |
  ILikePostSuccessAction | 
  ILikePostFailureAction |
  IDisLikePostAction | 
  IDisLikePostSuccessAction | 
  IDisLikePostFailureAction |
  IShowUserAction |
  IHideUserAction |
  ISendSharedPostAction |
  ISendSharedPostSuccessAction |
  ISendSharedPostFailureAction;
  