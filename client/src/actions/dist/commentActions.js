"use strict";
exports.__esModule = true;
exports.hideUserComment = exports.showUserComment = exports.dislikeCommentFailure = exports.dislikeCommentSuccess = exports.dislikeComment = exports.likeCommentFailure = exports.likeCommentSuccess = exports.likeComment = exports.deleteCommentFailure = exports.deleteCommentSuccess = exports.deleteComment = exports.editCommentFailure = exports.editCommentSuccess = exports.editComment = exports.cancelEditedComment = exports.setEditedComment = exports.addCommentFailure = exports.addCommentSuccess = exports.addComment = exports.fetchCommentsFailure = exports.fetchCommentsSuccess = exports.fetchComments = void 0;
var commentTypes_1 = require("./types/commentTypes");
exports.fetchComments = function (postId) { return ({
    type: commentTypes_1.FETCH_COMMENTS,
    payload: postId
}); };
exports.fetchCommentsSuccess = function (comments) { return ({
    type: commentTypes_1.FETCH_COMMENTS_SUCCESS,
    payload: comments
}); };
exports.fetchCommentsFailure = function (error) { return ({
    type: commentTypes_1.FETCH_COMMENTS_FAILURE,
    payload: error
}); };
exports.addComment = function (comment) { return ({
    type: commentTypes_1.ADD_COMMENT,
    payload: comment
}); };
exports.addCommentSuccess = function (comment) { return ({
    type: commentTypes_1.ADD_COMMENT_SUCCESS,
    payload: comment
}); };
exports.addCommentFailure = function (error) { return ({
    type: commentTypes_1.ADD_COMMENT_FAILURE,
    payload: error
}); };
exports.setEditedComment = function (comment) { return ({
    type: commentTypes_1.SET_EDITED_COMMENT,
    payload: comment
}); };
exports.cancelEditedComment = function () { return ({
    type: commentTypes_1.CANCEL_EDITED_COMMENT
}); };
exports.editComment = function (comment) { return ({
    type: commentTypes_1.EDIT_COMMENT,
    payload: comment
}); };
exports.editCommentSuccess = function (comment) { return ({
    type: commentTypes_1.EDIT_COMMENT_SUCCESS,
    payload: comment
}); };
exports.editCommentFailure = function (error) { return ({
    type: commentTypes_1.EDIT_COMMENT_FAILURE,
    payload: error
}); };
exports.deleteComment = function (id) { return ({
    type: commentTypes_1.DELETE_COMMENT,
    payload: id
}); };
exports.deleteCommentSuccess = function (id) { return ({
    type: commentTypes_1.DELETE_COMMENT_SUCCESS,
    payload: id
}); };
exports.deleteCommentFailure = function (error) { return ({
    type: commentTypes_1.DELETE_COMMENT_FAILURE,
    payload: error
}); };
exports.likeComment = function (react) { return ({
    type: commentTypes_1.LIKE_COMMENT,
    payload: react
}); };
exports.likeCommentSuccess = function (comment) { return ({
    type: commentTypes_1.LIKE_COMMENT_SUCCESS,
    payload: comment
}); };
exports.likeCommentFailure = function (error) { return ({
    type: commentTypes_1.LIKE_COMMENT_FAILURE,
    payload: error
}); };
exports.dislikeComment = function (react) { return ({
    type: commentTypes_1.DISLIKE_COMMENT,
    payload: react
}); };
exports.dislikeCommentSuccess = function (comment) { return ({
    type: commentTypes_1.DISLIKE_COMMENT_SUCCESS,
    payload: comment
}); };
exports.dislikeCommentFailure = function (error) { return ({
    type: commentTypes_1.DISLIKE_COMMENT_FAILURE,
    payload: error
}); };
exports.showUserComment = function (react) { return ({
    type: commentTypes_1.SHOW_USER_BY_COMMENT,
    payload: react
}); };
exports.hideUserComment = function (comment) { return ({
    type: commentTypes_1.HIDE_USER_BY_COMMENT,
    payload: comment
}); };
