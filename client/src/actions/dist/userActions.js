"use strict";
exports.__esModule = true;
exports.notifyUserFailure = exports.notifyUserSuccess = exports.notifyUser = exports.changePasswordFailure = exports.changePasswordSuccess = exports.changePassword = exports.resetPasswordFailure = exports.resetPasswordSuccess = exports.resetPassword = exports.editUserFailure = exports.editUserSuccess = exports.editUser = exports.cancelEditUser = exports.setEditUser = exports.sendEmailFailure = exports.sendEmailSuccess = exports.sendEmail = exports.fetchUsersFailure = exports.fetchUsersSuccess = exports.fetchUsers = exports.logoutUser = exports.registerFailure = exports.registerSuccess = exports.registerInit = exports.loginFailure = exports.loginSuccess = exports.loginInit = exports.loadUserFailure = exports.loadUserSuccess = exports.loadUser = void 0;
var userTypes_1 = require("./types/userTypes");
exports.loadUser = function () { return ({
    type: userTypes_1.LOAD_USER
}); };
exports.loadUserSuccess = function (user) { return ({
    type: userTypes_1.LOAD_USER_SUCCESS,
    payload: user
}); };
exports.loadUserFailure = function (error) { return ({
    type: userTypes_1.LOAD_USER_FAILURE,
    payload: error
}); };
exports.loginInit = function (user) { return ({
    type: userTypes_1.LOGIN_INIT,
    payload: user
}); };
exports.loginSuccess = function (user) { return ({
    type: userTypes_1.LOGIN_SUCCESS,
    payload: user
}); };
exports.loginFailure = function (error) { return ({
    type: userTypes_1.LOGIN_FAILURE,
    payload: error
}); };
exports.registerInit = function (user) { return ({
    type: userTypes_1.REGISTER_INIT,
    payload: user
}); };
exports.registerSuccess = function (user) { return ({
    type: userTypes_1.REGISTER_SUCCESS,
    payload: user
}); };
exports.registerFailure = function (error) { return ({
    type: userTypes_1.REGISTER_FAILURE,
    payload: error
}); };
exports.logoutUser = function () { return ({
    type: userTypes_1.LOGOUT_USER
}); };
exports.fetchUsers = function (element) { return ({
    type: userTypes_1.FETCH_USERS,
    payload: element
}); };
exports.fetchUsersSuccess = function (users) { return ({
    type: userTypes_1.FETCH_USERS_SUCCESS,
    payload: users
}); };
exports.fetchUsersFailure = function (error) { return ({
    type: userTypes_1.FETCH_USERS_FAILURE,
    payload: error
}); };
exports.sendEmail = function (user) { return ({
    type: userTypes_1.SEND_EMAIL,
    payload: user
}); };
exports.sendEmailSuccess = function () { return ({
    type: userTypes_1.SEND_EMAIL_SUCCESS
}); };
exports.sendEmailFailure = function (error) { return ({
    type: userTypes_1.SEND_EMAIL_FAILURE,
    payload: error
}); };
exports.setEditUser = function (user) { return ({
    type: userTypes_1.SET_EDITED_USER,
    payload: user
}); };
exports.cancelEditUser = function () { return ({
    type: userTypes_1.CANCEL_EDITED_USER
}); };
exports.editUser = function (user) { return ({
    type: userTypes_1.EDIT_USER,
    payload: user
}); };
exports.editUserSuccess = function (user) { return ({
    type: userTypes_1.EDIT_USER_SUCCESS,
    payload: user
}); };
exports.editUserFailure = function (error) { return ({
    type: userTypes_1.EDIT_USER_FAILURE,
    payload: error
}); };
exports.resetPassword = function (user) { return ({
    type: userTypes_1.RESET_PASSWORD,
    payload: user
}); };
exports.resetPasswordSuccess = function () { return ({
    type: userTypes_1.RESET_PASSWORD_SUCCESS
}); };
exports.resetPasswordFailure = function (error) { return ({
    type: userTypes_1.RESET_PASSWORD_FAILURE,
    payload: error
}); };
exports.changePassword = function (element) { return ({
    type: userTypes_1.CHANGE_PASSWORD,
    payload: element
}); };
exports.changePasswordSuccess = function (user) { return ({
    type: userTypes_1.CHANGE_PASSWORD_SUCCESS,
    payload: user
}); };
exports.changePasswordFailure = function (error) { return ({
    type: userTypes_1.CHANGE_PASSWORD_FAILURE,
    payload: error
}); };
exports.notifyUser = function (notify) { return ({
    type: userTypes_1.NOTIFY_USER,
    payload: notify
}); };
exports.notifyUserSuccess = function () { return ({
    type: userTypes_1.NOTIFY_USER_SUCCESS
}); };
exports.notifyUserFailure = function (error) { return ({
    type: userTypes_1.NOTIFY_USER_FAILURE,
    payload: error
}); };
