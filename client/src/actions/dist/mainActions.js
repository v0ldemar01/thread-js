"use strict";
exports.__esModule = true;
exports.hideLoader = exports.showLoader = void 0;
var mainTypes_1 = require("./types/mainTypes");
exports.showLoader = function () { return ({
    type: mainTypes_1.SHOW_LOADER
}); };
exports.hideLoader = function () { return ({
    type: mainTypes_1.HIDE_LOADER
}); };
