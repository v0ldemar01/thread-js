"use strict";
exports.__esModule = true;
exports.sendSharedPostFailure = exports.sendSharedPostSuccess = exports.sendSharedPost = exports.hideUserPost = exports.showUserPost = exports.dislikePostFailure = exports.dislikePostSuccess = exports.dislikePost = exports.likePostFailure = exports.likePostSuccess = exports.likePost = exports.deletePostFailure = exports.deletePostSuccess = exports.deletePost = exports.editPostFailure = exports.editPostSuccess = exports.editPost = exports.cancelExpandedPost = exports.cancelEditedPost = exports.setExpandedPost = exports.setEditedPost = exports.addPostFailure = exports.addPostSuccess = exports.addPost = exports.fetchPostsFailure = exports.clearPostsFilter = exports.clearPosts = exports.fetchPostsFilterSuccess = exports.fetchPostsSuccess = exports.fetchPosts = void 0;
var postTypes_1 = require("./types/postTypes");
exports.fetchPosts = function (filter) { return ({
    type: postTypes_1.FETCH_POSTS,
    payload: filter
}); };
exports.fetchPostsSuccess = function (posts) { return ({
    type: postTypes_1.FETCH_POSTS_SUCCESS,
    payload: posts
}); };
exports.fetchPostsFilterSuccess = function (posts) { return ({
    type: postTypes_1.FETCH_POSTS_FILTER_SUCCESS,
    payload: posts
}); };
exports.clearPosts = function () { return ({
    type: postTypes_1.CLEAR_POSTS
}); };
exports.clearPostsFilter = function () { return ({
    type: postTypes_1.CLEAR_POSTS_FILTER
}); };
exports.fetchPostsFailure = function (error) { return ({
    type: postTypes_1.FETCH_POSTS_FAILURE,
    payload: error
}); };
exports.addPost = function (post) { return ({
    type: postTypes_1.ADD_POST,
    payload: post
}); };
exports.addPostSuccess = function (post) { return ({
    type: postTypes_1.ADD_POST_SUCCESS,
    payload: post
}); };
exports.addPostFailure = function (error) { return ({
    type: postTypes_1.ADD_POST_FAILURE,
    payload: error
}); };
exports.setEditedPost = function (id) { return ({
    type: postTypes_1.SET_EDITED_POST,
    payload: id
}); };
exports.setExpandedPost = function (id) { return ({
    type: postTypes_1.SET_EXPANDED_POST,
    payload: id
}); };
exports.cancelEditedPost = function () { return ({
    type: postTypes_1.CANCEL_EDITED_POST
}); };
exports.cancelExpandedPost = function () { return ({
    type: postTypes_1.CANCEL_EXPANDED_POST
}); };
exports.editPost = function (post) { return ({
    type: postTypes_1.EDIT_POST,
    payload: post
}); };
exports.editPostSuccess = function (post) { return ({
    type: postTypes_1.EDIT_POST_SUCCESS,
    payload: post
}); };
exports.editPostFailure = function (error) { return ({
    type: postTypes_1.EDIT_POST_FAILURE,
    payload: error
}); };
exports.deletePost = function (id) { return ({
    type: postTypes_1.DELETE_POST,
    payload: id
}); };
exports.deletePostSuccess = function (id) { return ({
    type: postTypes_1.DELETE_POST_SUCCESS,
    payload: id
}); };
exports.deletePostFailure = function (error) { return ({
    type: postTypes_1.DELETE_POST_FAILURE,
    payload: error
}); };
exports.likePost = function (react) { return ({
    type: postTypes_1.LIKE_POST,
    payload: react
}); };
exports.likePostSuccess = function (post) { return ({
    type: postTypes_1.LIKE_POST_SUCCESS,
    payload: post
}); };
exports.likePostFailure = function (error) { return ({
    type: postTypes_1.LIKE_POST_FAILURE,
    payload: error
}); };
exports.dislikePost = function (react) { return ({
    type: postTypes_1.DISLIKE_POST,
    payload: react
}); };
exports.dislikePostSuccess = function (post) { return ({
    type: postTypes_1.DISLIKE_POST_SUCCESS,
    payload: post
}); };
exports.dislikePostFailure = function (error) { return ({
    type: postTypes_1.DISLIKE_POST_FAILURE,
    payload: error
}); };
exports.showUserPost = function (react) { return ({
    type: postTypes_1.SHOW_USER_BY_POST,
    payload: react
}); };
exports.hideUserPost = function () { return ({
    type: postTypes_1.HIDE_USER_BY_POST
}); };
exports.sendSharedPost = function (share) { return ({
    type: postTypes_1.SEND_SHARED_POST,
    payload: share
}); };
exports.sendSharedPostSuccess = function () { return ({
    type: postTypes_1.SEND_SHARED_POST_SUCCESS
}); };
exports.sendSharedPostFailure = function (error) { return ({
    type: postTypes_1.SEND_SHARED_POST_FAILURE,
    payload: error
}); };
