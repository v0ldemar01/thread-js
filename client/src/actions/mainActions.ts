import { 
  SHOW_LOADER,
  HIDE_LOADER,
  MainActionTypes
} from './types/mainTypes';

export const showLoader = (): MainActionTypes => ({
  type: SHOW_LOADER
});

export const hideLoader = (): MainActionTypes => ({
  type: HIDE_LOADER
});