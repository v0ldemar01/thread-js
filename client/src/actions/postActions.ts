import { 
  FETCH_POSTS,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FILTER_SUCCESS,
  CLEAR_POSTS,
  CLEAR_POSTS_FILTER,
  FETCH_POSTS_FAILURE,
  ADD_POST,
  ADD_POST_SUCCESS,
  ADD_POST_FAILURE,
  SET_EDITED_POST,
  CANCEL_EDITED_POST,
  SET_EXPANDED_POST,
  CANCEL_EXPANDED_POST,
  EDIT_POST,
  EDIT_POST_SUCCESS,
  EDIT_POST_FAILURE,
  DELETE_POST,
  DELETE_POST_SUCCESS,
  DELETE_POST_FAILURE,
  LIKE_POST,
  LIKE_POST_SUCCESS,
  LIKE_POST_FAILURE,
  DISLIKE_POST,
  DISLIKE_POST_SUCCESS,
  DISLIKE_POST_FAILURE,
  SHOW_USER_BY_POST,
  HIDE_USER_BY_POST,
  SEND_SHARED_POST,
  SEND_SHARED_POST_SUCCESS,
  SEND_SHARED_POST_FAILURE,
  PostActionTypes
} from './types/postTypes';
import IPost from '../models/post';
import IPostReact from '../models/postReact';
import IFilterPosts from '../models/postFilter';
import IHttpError from '../models/error';
import IShareEmailUser from '../models/shareEmailUser';

export const fetchPosts = (filter: IFilterPosts): PostActionTypes => ({
  type: FETCH_POSTS,
  payload: filter
});

export const fetchPostsSuccess = (posts: IPost[]): PostActionTypes => ({
  type: FETCH_POSTS_SUCCESS,
  payload: posts
});

export const fetchPostsFilterSuccess = (posts: IPost[]): PostActionTypes => ({
  type: FETCH_POSTS_FILTER_SUCCESS,
  payload: posts
});

export const clearPosts = (): PostActionTypes => ({
  type: CLEAR_POSTS  
});

export const clearPostsFilter = (): PostActionTypes => ({
  type: CLEAR_POSTS_FILTER
});


export const fetchPostsFailure = (error: IHttpError): PostActionTypes => ({
  type: FETCH_POSTS_FAILURE,
  payload: error
});

export const addPost = (post: IPost): PostActionTypes => ({
  type: ADD_POST,
  payload: post
});

export const addPostSuccess = (post: IPost): PostActionTypes => ({
  type: ADD_POST_SUCCESS,
  payload: post
});

export const addPostFailure = (error: IHttpError): PostActionTypes => ({
  type: ADD_POST_FAILURE,
  payload: error
});

export const setEditedPost = (id: string): PostActionTypes => ({
  type: SET_EDITED_POST,
  payload: id
});

export const setExpandedPost = (id: string): PostActionTypes => ({
  type: SET_EXPANDED_POST,
  payload: id
});

export const cancelEditedPost = (): PostActionTypes => ({
  type: CANCEL_EDITED_POST  
});

export const cancelExpandedPost = (): PostActionTypes => ({
  type: CANCEL_EXPANDED_POST  
});

export const editPost = (post: IPost): PostActionTypes => ({
  type: EDIT_POST,
  payload: post
});

export const editPostSuccess = (post: IPost): PostActionTypes => ({
  type: EDIT_POST_SUCCESS,
  payload: post
});

export const editPostFailure = (error: IHttpError): PostActionTypes => ({
  type: EDIT_POST_FAILURE,
  payload: error
});

export const deletePost = (id: string): PostActionTypes => ({
  type: DELETE_POST,
  payload: id
});

export const deletePostSuccess = (id: string): PostActionTypes => ({
  type: DELETE_POST_SUCCESS,
  payload: id
});

export const deletePostFailure = (error: IHttpError): PostActionTypes => ({
  type: DELETE_POST_FAILURE,
  payload: error
});

export const likePost = (react: IPostReact): PostActionTypes => ({
  type: LIKE_POST,
  payload: react
});

export const likePostSuccess = (post: IPost): PostActionTypes => ({
  type: LIKE_POST_SUCCESS,
  payload: post
});

export const likePostFailure = (error: IHttpError): PostActionTypes => ({
  type: LIKE_POST_FAILURE,
  payload: error
});

export const dislikePost = (react: IPostReact): PostActionTypes => ({
  type: DISLIKE_POST,
  payload: react
});

export const dislikePostSuccess = (post: IPost): PostActionTypes => ({
  type: DISLIKE_POST_SUCCESS,
  payload: post
});

export const dislikePostFailure = (error: IHttpError): PostActionTypes => ({
  type: DISLIKE_POST_FAILURE,
  payload: error
});

export const showUserPost = (react: IPostReact): PostActionTypes => ({
  type: SHOW_USER_BY_POST,
  payload: react
});

export const hideUserPost = (): PostActionTypes => ({
  type: HIDE_USER_BY_POST
});

export const sendSharedPost = (share: IShareEmailUser): PostActionTypes => ({
  type: SEND_SHARED_POST,
  payload: share
});

export const sendSharedPostSuccess = (): PostActionTypes => ({
  type: SEND_SHARED_POST_SUCCESS
});

export const sendSharedPostFailure = (error: IHttpError): PostActionTypes => ({
  type: SEND_SHARED_POST_FAILURE,
  payload: error
});

