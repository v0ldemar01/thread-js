import { 
  LOAD_USER,
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILURE,
  LOGIN_INIT,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  REGISTER_INIT,
  REGISTER_SUCCESS,
  REGISTER_FAILURE, 
  LOGOUT_USER,  
  SET_EDITED_USER,
  CANCEL_EDITED_USER,
  EDIT_USER,
  EDIT_USER_SUCCESS,
  EDIT_USER_FAILURE,
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  SEND_EMAIL,
  SEND_EMAIL_SUCCESS,
  SEND_EMAIL_FAILURE,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE,
  NOTIFY_USER,
  NOTIFY_USER_SUCCESS,
  NOTIFY_USER_FAILURE,
  UserActionTypes,
} from './types/userTypes';
import IUser from '../models/user';
import IHttpError from '../models/error';
import IInfoReact from '../models/infoReact';
import INotify from '../models/notify';
import IChangePassword from '../models/changePassword';

export const loadUser = (): UserActionTypes => ({
  type: LOAD_USER  
});

export const loadUserSuccess = (user: IUser): UserActionTypes => ({
  type: LOAD_USER_SUCCESS,
  payload: user
});

export const loadUserFailure = (error: IHttpError): UserActionTypes => ({
  type: LOAD_USER_FAILURE,
  payload: error
});

export const loginInit = (user: IUser): UserActionTypes => ({
  type: LOGIN_INIT,
  payload: user
});

export const loginSuccess = (user: IUser): UserActionTypes => ({
  type: LOGIN_SUCCESS,
  payload: user
});

export const loginFailure = (error: IHttpError): UserActionTypes => ({
  type: LOGIN_FAILURE,
  payload: error
});

export const registerInit = (user: IUser): UserActionTypes => ({
  type: REGISTER_INIT,
  payload: user
});

export const registerSuccess = (user: IUser): UserActionTypes => ({
  type: REGISTER_SUCCESS,
  payload: user
});

export const registerFailure = (error: IHttpError): UserActionTypes => ({
  type: REGISTER_FAILURE,
  payload: error
});

export const logoutUser = (): UserActionTypes => ({
  type: LOGOUT_USER,  
});

export const fetchUsers = (element: IInfoReact): UserActionTypes => ({
  type: FETCH_USERS,
  payload: element
});

export const fetchUsersSuccess = (users: IUser[]): UserActionTypes => ({
  type: FETCH_USERS_SUCCESS,
  payload: users
});

export const fetchUsersFailure = (error: IHttpError): UserActionTypes => ({
  type: FETCH_USERS_FAILURE,
  payload: error
});

export const sendEmail = (user: IUser): UserActionTypes => ({
  type: SEND_EMAIL,
  payload: user
});

export const sendEmailSuccess = (): UserActionTypes => ({
  type: SEND_EMAIL_SUCCESS 
});

export const sendEmailFailure = (error: IHttpError): UserActionTypes => ({
  type: SEND_EMAIL_FAILURE,
  payload: error
});

export const setEditUser = (user: IUser): UserActionTypes => ({
  type: SET_EDITED_USER,
  payload: user
});

export const cancelEditUser = (): UserActionTypes => ({
  type: CANCEL_EDITED_USER  
});

export const editUser = (user: IUser): UserActionTypes => ({
  type: EDIT_USER,
  payload: user
});

export const editUserSuccess = (user: IUser): UserActionTypes => ({
  type: EDIT_USER_SUCCESS,
  payload: user 
});

export const editUserFailure = (error: IHttpError): UserActionTypes => ({
  type: EDIT_USER_FAILURE,
  payload: error
});

export const resetPassword = (user: IUser): UserActionTypes => ({
  type: RESET_PASSWORD,
  payload: user
});

export const resetPasswordSuccess = (): UserActionTypes => ({
  type: RESET_PASSWORD_SUCCESS  
});

export const resetPasswordFailure = (error: IHttpError): UserActionTypes => ({
  type: RESET_PASSWORD_FAILURE,
  payload: error
});

export const changePassword = (element: IChangePassword): UserActionTypes => ({
  type: CHANGE_PASSWORD,
  payload: element
});

export const changePasswordSuccess = (user: IUser): UserActionTypes => ({
  type: CHANGE_PASSWORD_SUCCESS,
  payload: user 
});

export const changePasswordFailure = (error: IHttpError): UserActionTypes => ({
  type: CHANGE_PASSWORD_FAILURE,
  payload: error
});

export const notifyUser = (notify: INotify): UserActionTypes => ({
  type: NOTIFY_USER,
  payload: notify
});

export const notifyUserSuccess = (): UserActionTypes => ({
  type: NOTIFY_USER_SUCCESS   
});

export const notifyUserFailure = (error: IHttpError): UserActionTypes => ({
  type: NOTIFY_USER_FAILURE,
  payload: error
});





