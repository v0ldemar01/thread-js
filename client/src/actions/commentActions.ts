import { 
  FETCH_COMMENTS,
  FETCH_COMMENTS_SUCCESS,
  FETCH_COMMENTS_FAILURE,
  ADD_COMMENT,
  ADD_COMMENT_SUCCESS,
  ADD_COMMENT_FAILURE,
  SET_EDITED_COMMENT,
  CANCEL_EDITED_COMMENT,
  EDIT_COMMENT,
  EDIT_COMMENT_SUCCESS,
  EDIT_COMMENT_FAILURE,
  DELETE_COMMENT,
  DELETE_COMMENT_SUCCESS,
  DELETE_COMMENT_FAILURE,
  LIKE_COMMENT,
  LIKE_COMMENT_SUCCESS,
  LIKE_COMMENT_FAILURE,
  DISLIKE_COMMENT,
  DISLIKE_COMMENT_SUCCESS,
  DISLIKE_COMMENT_FAILURE,
  SHOW_USER_BY_COMMENT,
  HIDE_USER_BY_COMMENT,
  CommentActionTypes
} from './types/commentTypes';
import IComment from '../models/comment';
import ICommentReact from '../models/commentReact';
import IHttpError from '../models/error';

export const fetchComments = (postId: string): CommentActionTypes => ({
  type: FETCH_COMMENTS,
  payload: postId
});

export const fetchCommentsSuccess = (comments: IComment[]): CommentActionTypes => ({
  type: FETCH_COMMENTS_SUCCESS,
  payload: comments
});

export const fetchCommentsFailure = (error: IHttpError): CommentActionTypes => ({
  type: FETCH_COMMENTS_FAILURE,
  payload: error
});

export const addComment = (comment: IComment): CommentActionTypes => ({
  type: ADD_COMMENT,
  payload: comment
});

export const addCommentSuccess = (comment: IComment): CommentActionTypes => ({
  type: ADD_COMMENT_SUCCESS,
  payload: comment
});

export const addCommentFailure = (error: IHttpError): CommentActionTypes => ({
  type: ADD_COMMENT_FAILURE,
  payload: error
});

export const setEditedComment = (comment: IComment): CommentActionTypes => ({
  type: SET_EDITED_COMMENT,
  payload: comment
});

export const cancelEditedComment = (): CommentActionTypes => ({
  type: CANCEL_EDITED_COMMENT
});

export const editComment = (comment: IComment): CommentActionTypes => ({
  type: EDIT_COMMENT,
  payload: comment
});

export const editCommentSuccess = (comment: IComment): CommentActionTypes => ({
  type: EDIT_COMMENT_SUCCESS,
  payload: comment
});

export const editCommentFailure = (error: IHttpError): CommentActionTypes => ({
  type: EDIT_COMMENT_FAILURE,
  payload: error
});

export const deleteComment = (id: string): CommentActionTypes => ({
  type: DELETE_COMMENT,
  payload: id
});

export const deleteCommentSuccess = (id: string): CommentActionTypes => ({
  type: DELETE_COMMENT_SUCCESS,
  payload: id
});

export const deleteCommentFailure = (error: IHttpError): CommentActionTypes => ({
  type: DELETE_COMMENT_FAILURE,
  payload: error
});

export const likeComment = (react: ICommentReact): CommentActionTypes => ({
  type: LIKE_COMMENT,
  payload: react
});

export const likeCommentSuccess = (comment: IComment): CommentActionTypes => ({
  type: LIKE_COMMENT_SUCCESS,
  payload: comment
});

export const likeCommentFailure = (error: IHttpError): CommentActionTypes => ({
  type: LIKE_COMMENT_FAILURE,
  payload: error
});

export const dislikeComment = (react: ICommentReact): CommentActionTypes => ({
  type: DISLIKE_COMMENT,
  payload: react
});

export const dislikeCommentSuccess = (comment: IComment): CommentActionTypes => ({
  type: DISLIKE_COMMENT_SUCCESS,
  payload: comment
});

export const dislikeCommentFailure = (error: IHttpError): CommentActionTypes => ({
  type: DISLIKE_COMMENT_FAILURE,
  payload: error
});

export const showUserComment = (react: ICommentReact): CommentActionTypes => ({
  type: SHOW_USER_BY_COMMENT,
  payload: react
});

export const hideUserComment = (comment: IComment): CommentActionTypes => ({
  type: HIDE_USER_BY_COMMENT,
  payload: comment
});

