import React from 'react';
import { getUserImgLink } from '../../helpers/imageHelper';
import moment from 'moment';
import ICommentProps from '../../models/modelProps/commentProps';
import { Comment as CommentUI, Icon, Image } from 'semantic-ui-react';

import styles from './styles.module.scss';
import IIMage from '../../models/image';

const Comment: React.FunctionComponent<ICommentProps> = ({
  comment: {
    id,
    body,
    createdAt,
    user,
    likeCount,
    dislikeCount
  },
  userId,
  toggleEditedComment,
  deleteComment,
  likeComment,
  dislikeComment,
  toggleShowUser
}: ICommentProps) => {
  const isOneLike = JSON.parse(likeCount) !== 0;
  const isOneDisLike = JSON.parse(dislikeCount) !== 0;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image as IIMage)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          {isOneLike && (
            <CommentUI.Action>
              <Image
                circular
                size="mini"
                style={{ width: '12px' }}
                src={getUserImgLink(user.image ? user.image : null)}
                onClick={() => toggleShowUser({commentId: id, isLike: true})}
              />
            </CommentUI.Action>
          )}
          <CommentUI.Action onClick={() => likeComment(id)}>
            <Icon name="thumbs up" style={{ marginRight: '0.75em' }} />
            {likeCount}
          </CommentUI.Action>
          {isOneDisLike && (
            <CommentUI.Action>
              <Image
                circular
                size="mini"
                style={{ width: '12px' }}
                src={getUserImgLink(user.image ? user.image : null)}
                onClick={() => toggleShowUser({commentId: id, isLike: false})}
              />
            </CommentUI.Action>
          )}
          <CommentUI.Action onClick={() => dislikeComment(id)}>
            <Icon name="thumbs down" style={{ marginRight: '0.75em' }} />
            {dislikeCount}
          </CommentUI.Action>
          <CommentUI.Action>
            Reply
          </CommentUI.Action>
          {user.id === userId
          && (
            <>
              <CommentUI.Action onClick={() => toggleEditedComment(id)}>
                Edit
              </CommentUI.Action>
              <CommentUI.Action onClick={() => deleteComment(id)}>
                Delete
              </CommentUI.Action>
            </>
          )}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

export default Comment;
