"use strict";
exports.__esModule = true;
var react_1 = require("react");
var imageHelper_1 = require("../../helpers/imageHelper");
var moment_1 = require("moment");
var semantic_ui_react_1 = require("semantic-ui-react");
var styles_module_scss_1 = require("./styles.module.scss");
var Comment = function (_a) {
    var _b = _a.comment, id = _b.id, body = _b.body, createdAt = _b.createdAt, user = _b.user, likeCount = _b.likeCount, dislikeCount = _b.dislikeCount, userId = _a.userId, toggleEditedComment = _a.toggleEditedComment, deleteComment = _a.deleteComment, likeComment = _a.likeComment, dislikeComment = _a.dislikeComment, toggleShowUser = _a.toggleShowUser;
    var isOneLike = JSON.parse(likeCount) !== 0;
    var isOneDisLike = JSON.parse(dislikeCount) !== 0;
    return (react_1["default"].createElement(semantic_ui_react_1.Comment, { className: styles_module_scss_1["default"].comment },
        react_1["default"].createElement(semantic_ui_react_1.Comment.Avatar, { src: imageHelper_1.getUserImgLink(user.image) }),
        react_1["default"].createElement(semantic_ui_react_1.Comment.Content, null,
            react_1["default"].createElement(semantic_ui_react_1.Comment.Author, { as: "a" }, user.username),
            react_1["default"].createElement(semantic_ui_react_1.Comment.Metadata, null, moment_1["default"](createdAt).fromNow()),
            react_1["default"].createElement(semantic_ui_react_1.Comment.Text, null, body),
            react_1["default"].createElement(semantic_ui_react_1.Comment.Actions, null,
                isOneLike && (react_1["default"].createElement(semantic_ui_react_1.Comment.Action, null,
                    react_1["default"].createElement(semantic_ui_react_1.Image, { circular: true, size: "mini", style: { width: '12px' }, src: imageHelper_1.getUserImgLink(user.image ? user.image : null), onClick: function () { return toggleShowUser({ commentId: id, isLike: true }); } }))),
                react_1["default"].createElement(semantic_ui_react_1.Comment.Action, { onClick: function () { return likeComment(id); } },
                    react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "thumbs up", style: { marginRight: '0.75em' } }),
                    likeCount),
                isOneDisLike && (react_1["default"].createElement(semantic_ui_react_1.Comment.Action, null,
                    react_1["default"].createElement(semantic_ui_react_1.Image, { circular: true, size: "mini", style: { width: '12px' }, src: imageHelper_1.getUserImgLink(user.image ? user.image : null), onClick: function () { return toggleShowUser({ commentId: id, isLike: false }); } }))),
                react_1["default"].createElement(semantic_ui_react_1.Comment.Action, { onClick: function () { return dislikeComment(id); } },
                    react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "thumbs down", style: { marginRight: '0.75em' } }),
                    dislikeCount),
                react_1["default"].createElement(semantic_ui_react_1.Comment.Action, null, "Reply"),
                user.id === userId
                    && (react_1["default"].createElement(react_1["default"].Fragment, null,
                        react_1["default"].createElement(semantic_ui_react_1.Comment.Action, { onClick: function () { return toggleEditedComment(id); } }, "Edit"),
                        react_1["default"].createElement(semantic_ui_react_1.Comment.Action, { onClick: function () { return deleteComment(id); } }, "Delete")))))));
};
exports["default"] = Comment;
