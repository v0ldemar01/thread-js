import React from 'react';
import { Card, Image, Label, Icon, Button } from 'semantic-ui-react';
import { getUserImgLink } from '../../helpers/imageHelper';
import moment from 'moment';
import IPostProps from '../../models/modelProps/postProps';

import styles from './styles.module.scss';

const Post: React.FunctionComponent<IPostProps>  = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleShowUser,
  toggleEditedPost,
  deletePost,
  sharePost,
  userId
}: IPostProps) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt
  } = post;
  const dateCreated = moment(createdAt).fromNow();
  const dateUpdated = moment(updatedAt).fromNow();
  const isOneLike = JSON.parse(likeCount) !== 0;
  const isOneDisLike = JSON.parse(dislikeCount) !== 0;
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {dateCreated}
          </span>
          <br />
          {dateCreated !== dateUpdated
            && (
              <>
                {' '}
                <span className="date">
                  updated by
                  {' '}
                  {user.username}
                  {' - '}
                  {dateUpdated}
                </span>
              </>
            )}

        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        {isOneLike && (
          <Image
            avatar
            size="mini"
            style={{ width: '20px', cursor: 'pointer' }}
            src={getUserImgLink(image ? image : null)}
            onClick={() => toggleShowUser({postId: id, isLike: true})}
          />
        )}
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        {isOneDisLike && (
          <Image
            avatar
            size="mini"
            style={{ width: '20px', cursor: 'pointer' }}
            src={getUserImgLink(image ? image : null)}
            onClick={() => toggleShowUser({postId: id, isLike: false})}
          />
        )}
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {user.id === userId && toggleEditedPost && deletePost
          && (
            <Button.Group floated="right" size="mini">
              <Button
                color="purple"
                type="submit"
                onClick={() => toggleEditedPost(id)}
              >
                Edit
              </Button>
              <Button.Or />
              <Button
                onClick={() => deletePost(id)}
                negative
              >
                Delete
              </Button>
            </Button.Group>
          )}
      </Card.Content>
    </Card>
  );
};

export default Post;
