"use strict";
exports.__esModule = true;
var react_1 = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var imageHelper_1 = require("../../helpers/imageHelper");
var moment_1 = require("moment");
var styles_module_scss_1 = require("./styles.module.scss");
var Post = function (_a) {
    var post = _a.post, likePost = _a.likePost, dislikePost = _a.dislikePost, toggleExpandedPost = _a.toggleExpandedPost, toggleShowUser = _a.toggleShowUser, toggleEditedPost = _a.toggleEditedPost, deletePost = _a.deletePost, sharePost = _a.sharePost, userId = _a.userId;
    var id = post.id, image = post.image, body = post.body, user = post.user, likeCount = post.likeCount, dislikeCount = post.dislikeCount, commentCount = post.commentCount, createdAt = post.createdAt, updatedAt = post.updatedAt;
    var dateCreated = moment_1["default"](createdAt).fromNow();
    var dateUpdated = moment_1["default"](updatedAt).fromNow();
    var isOneLike = JSON.parse(likeCount) !== 0;
    var isOneDisLike = JSON.parse(dislikeCount) !== 0;
    return (react_1["default"].createElement(semantic_ui_react_1.Card, { style: { width: '100%' } },
        image && react_1["default"].createElement(semantic_ui_react_1.Image, { src: image.link, wrapped: true, ui: false }),
        react_1["default"].createElement(semantic_ui_react_1.Card.Content, null,
            react_1["default"].createElement(semantic_ui_react_1.Card.Meta, null,
                react_1["default"].createElement("span", { className: "date" },
                    "posted by",
                    ' ',
                    user.username,
                    ' - ',
                    dateCreated),
                react_1["default"].createElement("br", null),
                dateCreated !== dateUpdated
                    && (react_1["default"].createElement(react_1["default"].Fragment, null,
                        ' ',
                        react_1["default"].createElement("span", { className: "date" },
                            "updated by",
                            ' ',
                            user.username,
                            ' - ',
                            dateUpdated)))),
            react_1["default"].createElement(semantic_ui_react_1.Card.Description, null, body)),
        react_1["default"].createElement(semantic_ui_react_1.Card.Content, { extra: true },
            isOneLike && (react_1["default"].createElement(semantic_ui_react_1.Image, { avatar: true, size: "mini", style: { width: '20px', cursor: 'pointer' }, src: imageHelper_1.getUserImgLink(image ? image : null), onClick: function () { return toggleShowUser({ postId: id, isLike: true }); } })),
            react_1["default"].createElement(semantic_ui_react_1.Label, { basic: true, size: "small", as: "a", className: styles_module_scss_1["default"].toolbarBtn, onClick: function () { return likePost(id); } },
                react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "thumbs up" }),
                likeCount),
            isOneDisLike && (react_1["default"].createElement(semantic_ui_react_1.Image, { avatar: true, size: "mini", style: { width: '20px', cursor: 'pointer' }, src: imageHelper_1.getUserImgLink(image ? image : null), onClick: function () { return toggleShowUser({ postId: id, isLike: false }); } })),
            react_1["default"].createElement(semantic_ui_react_1.Label, { basic: true, size: "small", as: "a", className: styles_module_scss_1["default"].toolbarBtn, onClick: function () { return dislikePost(id); } },
                react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "thumbs down" }),
                dislikeCount),
            react_1["default"].createElement(semantic_ui_react_1.Label, { basic: true, size: "small", as: "a", className: styles_module_scss_1["default"].toolbarBtn, onClick: function () { return toggleExpandedPost(id); } },
                react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "comment" }),
                commentCount),
            react_1["default"].createElement(semantic_ui_react_1.Label, { basic: true, size: "small", as: "a", className: styles_module_scss_1["default"].toolbarBtn, onClick: function () { return sharePost(id); } },
                react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "share alternate" })),
            user.id === userId && toggleEditedPost && deletePost
                && (react_1["default"].createElement(semantic_ui_react_1.Button.Group, { floated: "right", size: "mini" },
                    react_1["default"].createElement(semantic_ui_react_1.Button, { color: "purple", type: "submit", onClick: function () { return toggleEditedPost(id); } }, "Edit"),
                    react_1["default"].createElement(semantic_ui_react_1.Button.Or, null),
                    react_1["default"].createElement(semantic_ui_react_1.Button, { onClick: function () { return deletePost(id); }, negative: true }, "Delete"))))));
};
exports["default"] = Post;
