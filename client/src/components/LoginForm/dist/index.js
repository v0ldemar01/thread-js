"use strict";
exports.__esModule = true;
var react_1 = require("react");
var validator_1 = require("validator");
var semantic_ui_react_1 = require("semantic-ui-react");
var LoginForm = function (_a) {
    var login = _a.login;
    var _b = react_1.useState(''), email = _b[0], setEmail = _b[1];
    var _c = react_1.useState(''), password = _c[0], setPassword = _c[1];
    var _d = react_1.useState(false), isLoading = _d[0], setIsLoading = _d[1];
    var _e = react_1.useState(true), isEmailValid = _e[0], setIsEmailValid = _e[1];
    var _f = react_1.useState(true), isPasswordValid = _f[0], setIsPasswordValid = _f[1];
    var emailChanged = function (data) {
        setEmail(data);
        setIsEmailValid(true);
    };
    var passwordChanged = function (data) {
        setPassword(data);
        setIsPasswordValid(true);
    };
    var handleLoginClick = function () {
        var isValid = isEmailValid && isPasswordValid;
        if (!isValid || isLoading) {
            return;
        }
        setIsLoading(true);
        login({ email: email, password: password });
        setIsLoading(false);
    };
    return (react_1["default"].createElement(semantic_ui_react_1.Form, { name: "loginForm", size: "large", onSubmit: handleLoginClick },
        react_1["default"].createElement(semantic_ui_react_1.Segment, null,
            react_1["default"].createElement(semantic_ui_react_1.Form.Input, { fluid: true, icon: "at", iconPosition: "left", placeholder: "Email", type: "email", error: !isEmailValid, onChange: function (event) { return emailChanged(event.target.value); }, onBlur: function () { return setIsEmailValid(validator_1["default"].isEmail(email)); } }),
            react_1["default"].createElement(semantic_ui_react_1.Form.Input, { fluid: true, icon: "lock", iconPosition: "left", placeholder: "Password", type: "password", error: !isPasswordValid, onChange: function (event) { return passwordChanged(event.target.value); }, onBlur: function () { return setIsPasswordValid(Boolean(password)); } }),
            react_1["default"].createElement(semantic_ui_react_1.Button, { type: "submit", color: "teal", fluid: true, size: "large", loading: isLoading, primary: true }, "Login"))));
};
exports["default"] = LoginForm;
