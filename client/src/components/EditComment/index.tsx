import React, { useState } from 'react';
import IEditCommentProps from '../../models/modelProps/editCommentProps';
import { Form, Button, Segment } from 'semantic-ui-react';

const EditComment: React.FunctionComponent<IEditCommentProps> = ({
  comment,
  editComment,
  toggleEditedComment
}: IEditCommentProps) => {
  const [body, setBody] = useState(comment.body);

  const handleEditComment = async () => {
    if (!body) {
      return;
    }
    await editComment({id: comment.id, body });
    setBody('');
    toggleEditedComment();
  };
  return (
    <Segment>
      <Form onSubmit={handleEditComment}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder={comment.body}
          onChange={ev => setBody(ev.target.value)}
        />
        <Button position="left" type="submit" color="blue" icon="edit" content="Confirm changes" />
      </Form>
    </Segment>
  );
};

export default EditComment;
