"use strict";
exports.__esModule = true;
exports.Logo = void 0;
var react_1 = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var styles_module_scss_1 = require("./styles.module.scss");
exports.Logo = function () { return (react_1["default"].createElement(semantic_ui_react_1.Header, { as: "h2", color: "grey", className: styles_module_scss_1["default"].logoWrapper },
    react_1["default"].createElement(semantic_ui_react_1.Image, { circular: true, src: "http://s1.iconbird.com/ico/2013/8/428/w256h2561377930292cattied.png" }),
    ' ',
    "Thread")); };
exports["default"] = exports.Logo;
