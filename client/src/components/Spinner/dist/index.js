"use strict";
exports.__esModule = true;
var react_1 = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var Spinner = function () { return (react_1["default"].createElement(semantic_ui_react_1.Dimmer, { active: true, inverted: true },
    react_1["default"].createElement(semantic_ui_react_1.Loader, { size: "massive", inverted: true }))); };
exports["default"] = Spinner;
