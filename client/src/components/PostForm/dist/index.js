"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var react_1 = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var styles_module_scss_1 = require("./styles.module.scss");
var PostForm = function (_a) {
    var actionPost = _a.actionPost, uploadImage = _a.uploadImage, post = _a.post;
    var _b = react_1.useState(post ? post.body : ''), body = _b[0], setBody = _b[1];
    var _c = react_1.useState({}), image = _c[0], setImage = _c[1];
    var _d = react_1.useState(false), isUploading = _d[0], setIsUploading = _d[1];
    var handlePostAction = function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            if (!body) {
                return [2 /*return*/];
            }
            actionPost({ id: post === null || post === void 0 ? void 0 : post.id, imageId: image === null || image === void 0 ? void 0 : image.id, body: body });
            setBody('');
            setImage({});
            return [2 /*return*/];
        });
    }); };
    var handleUploadFile = function (selectorFiles) { return __awaiter(void 0, void 0, void 0, function () {
        var _a, id, link;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    setIsUploading(true);
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, , 3, 4]);
                    return [4 /*yield*/, uploadImage(selectorFiles[0])];
                case 2:
                    _a = _b.sent(), id = _a.id, link = _a.link;
                    setImage({ id: id, link: link });
                    return [3 /*break*/, 4];
                case 3:
                    // TODO: show error
                    setIsUploading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    return (react_1["default"].createElement(semantic_ui_react_1.Segment, null,
        react_1["default"].createElement(semantic_ui_react_1.Form, { onSubmit: handlePostAction },
            react_1["default"].createElement(semantic_ui_react_1.Form.TextArea, { name: "body", value: body, placeholder: post ? post.body : 'What about news?', onChange: function (ev) { return setBody(ev.target.value); } }),
            (image === null || image === void 0 ? void 0 : image.link) && (react_1["default"].createElement("div", { className: styles_module_scss_1["default"].imageWrapper },
                react_1["default"].createElement(semantic_ui_react_1.Image, { className: styles_module_scss_1["default"].image, src: image === null || image === void 0 ? void 0 : image.link, alt: "post" }))),
            react_1["default"].createElement(semantic_ui_react_1.Button, { color: "teal", icon: true, labelPosition: "left", as: "label", loading: isUploading, disabled: isUploading },
                react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "image" }),
                "Attach image",
                react_1["default"].createElement("input", { name: "image", type: "file", onChange: function (event) { return handleUploadFile(event.target.files); }, hidden: true })),
            react_1["default"].createElement(semantic_ui_react_1.Button, { floated: "right", color: "blue", type: "submit" }, post ? 'Edit' : 'Post'))));
};
exports["default"] = PostForm;
