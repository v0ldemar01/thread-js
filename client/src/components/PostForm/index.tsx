import React, { useState } from 'react';
import IPostFormProps from '../../models/modelProps/postFormProps';
import IIMage from '../../models/image';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const PostForm: React.FunctionComponent<IPostFormProps> = ({
  actionPost,
  uploadImage,
  post
}: IPostFormProps) => {
  const [body, setBody] = useState(post ? post.body : '');
  const [image, setImage] = useState({} as IIMage);
  const [isUploading, setIsUploading] = useState(false);

  const handlePostAction = async () => {
    if (!body) {
      return;
    }
    actionPost({ id: post?.id, imageId: image?.id, body });
    setBody('');
    setImage({} as IIMage);
  };

  const handleUploadFile = async (selectorFiles: FileList) => {
    setIsUploading(true);
    try {
      const { id, link } = await uploadImage(selectorFiles[0]);
      setImage({ id, link });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handlePostAction}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder={post ? post.body : 'What about news?'}
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.link && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.link} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={event => handleUploadFile(event.target.files as FileList)} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">{post ? 'Edit' : 'Post'}</Button>
      </Form>
    </Segment>
  );
};

export default PostForm;
