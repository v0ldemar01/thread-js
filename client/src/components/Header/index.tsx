import React from 'react';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from '../../helpers/imageHelper';
import IHeader from '../../models/modelProps/headerProps';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';
import IIMage from '../../models/image';

const Header: React.FunctionComponent<IHeader> = ({ 
  user, 
  logout 
}: IHeader) => (
  <div className={styles.headerWrp}>
    <Grid centered container columns="2">
      <Grid.Column style={{paddingBottom: 0}}>
        {user && (
          <NavLink exact to="/">
            <HeaderUI>
              <Image circular src={getUserImgLink(user.image as IIMage)} floated="left" />
              <>
                {user.username}
                <br />
                <p>{user.userStatus}</p>
              </>
            </HeaderUI>
          </NavLink>
        )}
      </Grid.Column>
      <Grid.Column textAlign="right">
        <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
          <Icon name="user circle" size="large" />
        </NavLink>
        <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={() => logout()}>
          <Icon name="log out" size="large" />
        </Button>
      </Grid.Column>
    </Grid>
  </div>
);

export default Header;
