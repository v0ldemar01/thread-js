"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var imageHelper_1 = require("../../helpers/imageHelper");
var semantic_ui_react_1 = require("semantic-ui-react");
var styles_module_scss_1 = require("./styles.module.scss");
var Header = function (_a) {
    var user = _a.user, logout = _a.logout;
    return (react_1["default"].createElement("div", { className: styles_module_scss_1["default"].headerWrp },
        react_1["default"].createElement(semantic_ui_react_1.Grid, { centered: true, container: true, columns: "2" },
            react_1["default"].createElement(semantic_ui_react_1.Grid.Column, { style: { paddingBottom: 0 } }, user && (react_1["default"].createElement(react_router_dom_1.NavLink, { exact: true, to: "/" },
                react_1["default"].createElement(semantic_ui_react_1.Header, null,
                    react_1["default"].createElement(semantic_ui_react_1.Image, { circular: true, src: imageHelper_1.getUserImgLink(user.image), floated: "left" }),
                    react_1["default"].createElement(react_1["default"].Fragment, null,
                        user.username,
                        react_1["default"].createElement("br", null),
                        react_1["default"].createElement("p", null, user.userStatus)))))),
            react_1["default"].createElement(semantic_ui_react_1.Grid.Column, { textAlign: "right" },
                react_1["default"].createElement(react_router_dom_1.NavLink, { exact: true, activeClassName: "active", to: "/profile", className: styles_module_scss_1["default"].menuBtn },
                    react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "user circle", size: "large" })),
                react_1["default"].createElement(semantic_ui_react_1.Button, { basic: true, icon: true, type: "button", className: styles_module_scss_1["default"].menuBtn + " " + styles_module_scss_1["default"].logoutBtn, onClick: function () { return logout(); } },
                    react_1["default"].createElement(semantic_ui_react_1.Icon, { name: "log out", size: "large" }))))));
};
exports["default"] = Header;
