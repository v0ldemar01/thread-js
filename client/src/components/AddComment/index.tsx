import React, { useState } from 'react';
import IAddCommentProps from '../../models/modelProps/addCommentProps'; 
import { Form, Button } from 'semantic-ui-react';

const AddComment: React.FunctionComponent<IAddCommentProps> = ({
  postId,
  addComment
}: IAddCommentProps) => {
  const [body, setBody] = useState('');

  const handleAddComment = async () => {
    if (!body) {
      return;
    }
    await addComment({ postId, body });
    setBody('');
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Post comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

export default AddComment;
