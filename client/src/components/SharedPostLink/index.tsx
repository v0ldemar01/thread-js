import React, { useState, useRef } from 'react';
import ISharedPostLinkProps from '../../models/modelProps/sharedPostLinkProps';
import { Modal, Input, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const SharedPostLink: React.FunctionComponent<ISharedPostLinkProps> = ({
  postId,
  username,
  close,
  sendSharedToEmail: send
}: ISharedPostLinkProps) => {
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState('');
  let input = useRef<Input>(null);
  const url = `${window.location.origin}/share/${postId}`;
  const copyToClipboard = (event: any) => {
    (input.current as Input).select();
    document.execCommand('copy');
    event.target.focus();
    setCopied(true);
  };

  const handleSendEmail = async () => {
    await send({ email, username, url });
    setEmail('');
  };
  return (
    <Modal open onClose={() => close()}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={url}
          ref={input}
        />
        <br />
        <Input
          icon="email"
          iconPosition="left"
          placeholder="Enter email of receiver"
          type="text"
          value={email}
          onChange={event => setEmail(event.target.value)}
        />
        <Button
          type="submit"
          content="Send to Email"
          onClick={handleSendEmail}
        />
      </Modal.Content>
    </Modal>
  );
};

export default SharedPostLink;
