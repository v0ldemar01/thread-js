"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var react_1 = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var styles_module_scss_1 = require("./styles.module.scss");
var SharedPostLink = function (_a) {
    var postId = _a.postId, username = _a.username, close = _a.close, send = _a.sendSharedToEmail;
    var _b = react_1.useState(false), copied = _b[0], setCopied = _b[1];
    var _c = react_1.useState(''), email = _c[0], setEmail = _c[1];
    var input = react_1.useRef(null);
    var url = window.location.origin + "/share/" + postId;
    var copyToClipboard = function (event) {
        input.current.select();
        document.execCommand('copy');
        event.target.focus();
        setCopied(true);
    };
    var handleSendEmail = function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, send({ email: email, username: username, url: url })];
                case 1:
                    _a.sent();
                    setEmail('');
                    return [2 /*return*/];
            }
        });
    }); };
    return (react_1["default"].createElement(semantic_ui_react_1.Modal, { open: true, onClose: function () { return close(); } },
        react_1["default"].createElement(semantic_ui_react_1.Modal.Header, { className: styles_module_scss_1["default"].header },
            react_1["default"].createElement("span", null, "Share Post"),
            copied && (react_1["default"].createElement("span", null,
                react_1["default"].createElement(semantic_ui_react_1.Icon, { color: "green", name: "copy" }),
                "Copied"))),
        react_1["default"].createElement(semantic_ui_react_1.Modal.Content, null,
            react_1["default"].createElement(semantic_ui_react_1.Input, { fluid: true, action: {
                    color: 'teal',
                    labelPosition: 'right',
                    icon: 'copy',
                    content: 'Copy',
                    onClick: copyToClipboard
                }, value: url, ref: input }),
            react_1["default"].createElement("br", null),
            react_1["default"].createElement(semantic_ui_react_1.Input, { icon: "email", iconPosition: "left", placeholder: "Enter email of receiver", type: "text", value: email, onChange: function (event) { return setEmail(event.target.value); } }),
            react_1["default"].createElement(semantic_ui_react_1.Button, { type: "submit", content: "Send to Email", onClick: handleSendEmail }))));
};
exports["default"] = SharedPostLink;
