export default [
  '/auth/login',
  '/auth/register',
  '/auth/forgotPassword',
  '/auth/changePassword'
];
