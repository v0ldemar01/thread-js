"use strict";
var _a;
exports.__esModule = true;
exports.fileSize = exports.imgurSecret = exports.imgurId = void 0;
var env_1 = require("../env");
exports.imgurId = (_a = env_1["default"].imgur, _a.imgurId), exports.imgurSecret = _a.imgurSecret;
exports.fileSize = 10000000; // ~ 10MB
