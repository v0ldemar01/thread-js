import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { secret } from './jwtConfig';
import userRepository from '../data/repositories/userRepository';
import UserCreationAttributes from '../data/modelsAttribute/user';
import { compare } from '../helpers/cryptoHelper';

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secret
};

passport.use(
  'login',
  new LocalStrategy({ usernameField: 'email' }, async (email: string, password: string, done: Function) => {
    try {
      const user = await userRepository.getByEmail(email) as UserCreationAttributes;
      if (!user) {
        return done({ status: 401, message: 'Incorrect email.' }, false);
      }

      return await compare(password, user.password)
        ? done(null, user)
        : done({ status: 401, message: 'Passwords do not match.' }, null, false);
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(
  'register',
  new LocalStrategy(
    { passReqToCallback: true },
    async ({ body: { email } } : {body: { email: string}}, username: string, password: string, done: Function) => {
      try {
        const userByEmail = await userRepository.getByEmail(email) as UserCreationAttributes;
        if (userByEmail) {
          return done({ status: 401, message: 'Email is already taken.' }, null);
        }

        return await userRepository.getByUsername(username) as UserCreationAttributes
          ? done({ status: 401, message: 'Username is already taken.' }, null)
          : done(null, { email, username, password });
      } catch (err) {
        return done(err);
      }
    }
  )
);

passport.use(new JwtStrategy(options, async ({ id }: { id: string}, done: Function) => {
  try {
    const user = await userRepository.getById(id) as UserCreationAttributes;
    return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
  } catch (err) {
    return done(err);
  }
}));
