"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var express_1 = require("express");
var path_1 = require("path");
var passport_1 = require("passport");
var http_1 = require("http");
var socket_io_1 = require("socket.io");
var cors_1 = require("cors");
var morgan_1 = require("morgan");
var connection_1 = require("./data/db/connection");
var index_1 = require("./api/routes/index");
var authorizationMiddleware_1 = require("./api/middlewares/authorizationMiddleware");
var errorHandlerMiddleware_1 = require("./api/middlewares/errorHandlerMiddleware");
var routesWhiteListConfig_1 = require("./config/routesWhiteListConfig");
var injector_1 = require("./socket/injector");
var handlers_1 = require("./socket/handlers");
var env_1 = require("./env");
require("./config/passportConfig");
var app = express_1["default"]();
var socketServer = http_1.createServer(app);
var io = new socket_io_1.Server(socketServer);
connection_1["default"]
    .authenticate()
    .then(function () {
    // eslint-disable-next-line no-console
    console.log('Connection has been established successfully.');
})["catch"](function (err) {
    // eslint-disable-next-line no-console
    console.error('Unable to connect to the database:', err);
});
app.use(cors_1["default"]());
app.use(express_1["default"].json());
app.use(express_1["default"].urlencoded({ extended: true }));
app.use(morgan_1["default"]('dev'));
app.use(passport_1["default"].initialize());
app.use(errorHandlerMiddleware_1["default"]);
handlers_1["default"](io);
app.use(injector_1["default"](io));
app.use('/api/', authorizationMiddleware_1["default"](routesWhiteListConfig_1["default"]));
index_1["default"](app);
var staticPath = path_1["default"].resolve(__dirname + "/../client/build");
app.use(express_1["default"].static(staticPath));
app.get('*', function (req, res) {
    res.write(fs_1["default"].readFileSync(__dirname + "/../client/build/index.html"));
    res.end();
});
socketServer.listen(env_1["default"].app.port, function () {
    console.log("Listen server on port " + env_1["default"].app.port);
});
