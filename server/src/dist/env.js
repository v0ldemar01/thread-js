"use strict";
exports.__esModule = true;
var dotenv_1 = require("dotenv");
dotenv_1["default"].config();
var env = {
    app: {
        port: process.env.APP_PORT,
        secret: process.env.SECRET_KEY
    },
    db: {
        database: process.env.DB_NAME,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: process.env.DB_DIALECT,
        logging: false
    },
    imgur: {
        imgurId: process.env.IMGUR_ID,
        imgurSecret: process.env.IMGUR_SECRET
    },
    email: {
        user: process.env.EMAIL_USER,
        password: process.env.EMAIL_PASSWORD
    }
};
exports["default"] = env;
