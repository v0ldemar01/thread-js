"use strict";

var _fs = _interopRequireDefault(require("fs"));

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _passport = _interopRequireDefault(require("passport"));

var _http = _interopRequireDefault(require("http"));

var _socket = _interopRequireDefault(require("socket.io"));

var _cors = _interopRequireDefault(require("cors"));

var _connection = _interopRequireDefault(require("./data/db/connection"));

var _index = _interopRequireDefault(require("./api/routes/index"));

var _authorizationMiddleware = _interopRequireDefault(require("./api/middlewares/authorizationMiddleware"));

var _errorHandlerMiddleware = _interopRequireDefault(require("./api/middlewares/errorHandlerMiddleware"));

var _routesWhiteListConfig = _interopRequireDefault(require("./config/routesWhiteListConfig"));

var _injector = _interopRequireDefault(require("./socket/injector"));

var _handlers = _interopRequireDefault(require("./socket/handlers"));

var _env = _interopRequireDefault(require("./env"));

require("./config/passportConfig");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])();

var socketServer = _http["default"].Server(app);

var io = (0, _socket["default"])(socketServer);

_connection["default"].authenticate().then(function () {
  // eslint-disable-next-line no-console
  console.log('Connection has been established successfully.');
})["catch"](function (err) {
  // eslint-disable-next-line no-console
  console.error('Unable to connect to the database:', err);
});

io.on('connection', _handlers["default"]);
app.use((0, _cors["default"])());
app.use(_express["default"].json());
app.use(_express["default"].urlencoded({
  extended: true
}));
app.use(_passport["default"].initialize());
app.use((0, _injector["default"])(io));
app.use('/api/', (0, _authorizationMiddleware["default"])(_routesWhiteListConfig["default"]));
(0, _index["default"])(app, io);

var staticPath = _path["default"].resolve("".concat(__dirname, "/../client/build"));

app.use(_express["default"]["static"](staticPath));
app.get('*', function (req, res) {
  res.write(_fs["default"].readFileSync("".concat(__dirname, "/../client/build/index.html")));
  res.end();
});
app.use(_errorHandlerMiddleware["default"]);
app.listen(_env["default"].app.port, function () {
  // eslint-disable-next-line no-console
  console.log("Server listening on port ".concat(_env["default"].app.port, "!"));
});
socketServer.listen(_env["default"].app.socketPort);