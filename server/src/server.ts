import fs from 'fs';
import express, {Request, Response} from 'express';
import path from 'path';
import passport from 'passport';
import { createServer } from 'http';
import { Server } from 'socket.io';
import cors from 'cors';
import morgan from 'morgan';
import sequelize from './data/db/connection';
import routes from './api/routes/index';
import authorizationMiddleware from './api/middlewares/authorizationMiddleware';
import errorHandlerMiddleware from './api/middlewares/errorHandlerMiddleware';
import routesWhiteList from './config/routesWhiteListConfig';
import socketInjector from './socket/injector';
import socketHandlers from './socket/handlers';
import env from './env';
import './config/passportConfig';

const app = express();
const socketServer = createServer(app);
const io = new Server(socketServer);

sequelize
  .authenticate()
  .then(() => {
    // eslint-disable-next-line no-console
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error('Unable to connect to the database:', err);
  });

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(passport.initialize());
app.use(errorHandlerMiddleware);

socketHandlers(io);
app.use(socketInjector(io));
app.use('/api/', authorizationMiddleware(routesWhiteList));
routes(app);
const staticPath = path.resolve(`${__dirname}/../client/build`);
app.use(express.static(staticPath));

app.get('*', (req: Request, res: Response) => {
  res.write(fs.readFileSync(`${__dirname}/../client/build/index.html`));
  res.end();
});

socketServer.listen(env.app.port, () => {
  console.log(`Listen server on port ${env.app.port}`);
});
