import { Model, Optional } from 'sequelize';

interface ICommentReactionModelAttribute extends Model {
  id?: string;
  isLike: boolean;
  isDisLike: boolean;
  userId?: string;
  commentId?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export default interface CommentReactionCreationAttributes extends Optional<ICommentReactionModelAttribute, 'id'> {}