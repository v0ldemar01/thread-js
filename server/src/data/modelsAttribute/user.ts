import { Model, Optional } from 'sequelize';

export interface IUser {
  email: string;
  username: string;
  password: string;
  avatar?: string;
  userStatus?: string;
  resetPasswordToken?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IUserModelAttribute extends IUser, Model {
  id?: string;
}

export default interface UserCreationAttributes extends Optional<IUserModelAttribute, 'id'> {}