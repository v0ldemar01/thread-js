import UserCreationAttributes from '../modelsAttribute/user';
import ImageCreationAttributes from '../modelsAttribute/image';
import PostCreationAttributes from '../modelsAttribute/post';
import PostReactionCreationAttributes from '../modelsAttribute/postReaction';
import CommentCreationAttributes from '../modelsAttribute/comment';
import CommentReactionCreationAttributes from '../modelsAttribute/commentReaction';

export type sequelizeModels =
  UserCreationAttributes |
  ImageCreationAttributes |
  PostCreationAttributes |
  PostReactionCreationAttributes |
  CommentCreationAttributes |
  CommentReactionCreationAttributes;