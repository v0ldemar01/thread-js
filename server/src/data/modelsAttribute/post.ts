import { Model, Optional } from 'sequelize';

interface IPostModelAttribute extends Model {
  id?: string;
  body: string;
  userId?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export default interface PostCreationAttributes extends Optional<IPostModelAttribute, 'id'> {}