import UserCreationAttributes from '../modelsAttribute/user';
import ImageCreationAttributes from '../modelsAttribute/image';
import PostCreationAttributes from '../modelsAttribute/post';
import PostReactionCreationAttributes from '../modelsAttribute/postReaction';
import CommentCreationAttributes from '../modelsAttribute/post';
import CommentReactionCreationAttributes from '../modelsAttribute/postReaction';
import { ModelCtor } from 'sequelize/types';

export default interface IModels {
  User: ModelCtor<UserCreationAttributes>,
  Image: ModelCtor<ImageCreationAttributes>,
  Post: ModelCtor<PostCreationAttributes>,
  PostReaction: ModelCtor<PostReactionCreationAttributes>,
  Comment: ModelCtor<CommentCreationAttributes>,
  CommentReaction: ModelCtor<CommentReactionCreationAttributes>
}