import { Model, Optional } from 'sequelize';

interface ICommentModelAttribute extends Model {
  id?: string;
  body: string;
  userId?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export default interface CommentCreationAttributes extends Optional<ICommentModelAttribute, 'id'> {}