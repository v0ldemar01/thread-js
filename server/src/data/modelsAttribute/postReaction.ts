import { Model, Optional } from 'sequelize';

interface IPostReactionModelAttribute extends Model {
  id?: string;
  isLike: boolean;
  isDisLike: boolean;
  userId?: string;
  postId?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export default interface PostReactionCreationAttributes extends Optional<IPostReactionModelAttribute, 'id'> {}