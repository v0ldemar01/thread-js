import { Model, Optional } from 'sequelize';

interface IImageModelAttribute extends Model {
  id?: string;
  link: string;
  deleteHash: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export default interface ImageCreationAttributes extends Optional<IImageModelAttribute, 'id'> {}