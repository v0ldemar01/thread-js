"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _require = require('../../helpers/cryptoHelper'),
    encryptSync = _require.encryptSync;

var hash = function hash(password) {
  return encryptSync(password);
};

var now = new Date();
exports.usersSeed = [{
  email: 'demo@demo.com',
  username: 'demo',
  password: hash('demo')
}, {
  email: 'gbottoms1@arizona.edu',
  username: 'jhon',
  password: hash('pxlxvUyyUjE')
}, {
  email: 'cclears2@state.gov',
  username: 'alex',
  password: hash('ioyLdS9Mdgj')
}, {
  email: 'htie3@chronoengine.com',
  username: 'kivi',
  password: hash('twn50kl')
}, {
  email: 'bbirmingham4@guardian.co.uk',
  username: 'avocado',
  password: hash('0naQBpP9')
}].map(function (user) {
  return _objectSpread({}, user, {
    createdAt: now,
    updatedAt: now
  });
}); // Do not add more images than the number of users.

exports.userImagesSeed = [{
  link: 'https://i.imgur.com/1Y2S7aU.png',
  deleteHash: 'APx0ofUQBidKc1P'
}, {
  link: 'https://i.imgur.com/iefpTkf.jpg',
  deleteHash: 'qWVnICWmbmqwGre'
}, {
  link: 'https://i.imgur.com/hsakp5k.jpg',
  deleteHash: '3cpoGBuOsXIe0o3'
}, {
  link: 'https://i.imgur.com/4TLD3P5.png',
  deleteHash: 'D6su1go8XvKpP4B'
}].map(function (image) {
  return _objectSpread({}, image, {
    createdAt: now,
    updatedAt: now
  });
});