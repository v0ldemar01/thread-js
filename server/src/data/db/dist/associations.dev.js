"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _modelsAttribute = _interopRequireDefault(require("../modelsAttribute"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(models) {
  var User = models.User,
      Post = models.Post,
      PostReaction = models.PostReaction,
      CommentReaction = models.CommentReaction,
      Comment = models.Comment,
      Image = models.Image;
  Image.hasOne(User);
  Image.hasOne(Post);
  User.hasMany(Post);
  User.hasMany(Comment);
  User.hasMany(PostReaction);
  User.hasMany(CommentReaction);
  User.belongsTo(Image);
  Post.belongsTo(Image);
  Post.belongsTo(User);
  Post.hasMany(PostReaction);
  Post.hasMany(Comment);
  Comment.belongsTo(User);
  Comment.belongsTo(Post);
  Comment.hasMany(CommentReaction);
  PostReaction.belongsTo(Post);
  PostReaction.belongsTo(User);
  CommentReaction.belongsTo(Comment);
  CommentReaction.belongsTo(User);
};

exports["default"] = _default;