import Sequelize from 'sequelize';
import { PostReactionModel, PostModel, UserModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId: string, postId: string) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getReactionUsers(postId: string, isLike: string) {
    const where = {};
    Object.assign(where, {
      id: {
        [Sequelize.Op.in]: Sequelize.literal(
          `(SELECT "postReactions"."userId" FROM "postReactions" WHERE "postReactions"."postId" = '${postId}' AND `
          + `"postReactions"."${JSON.parse(isLike) ? 'isLike' : 'isDisLike'}" = '${true}')`
        )
      }
    });
    return UserModel.findAll({
      where
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
