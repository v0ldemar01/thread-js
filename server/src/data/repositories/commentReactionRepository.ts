import Sequelize from 'sequelize';
import { CommentReactionModel, CommentModel, UserModel } from '../models';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId: string, commentId: string) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getReactionUsers(commentId: string, isLike: string) {
    const where = {};
    Object.assign(where, {
      id: {
        [Sequelize.Op.in]: Sequelize.literal(
          '(SELECT "commentReactions"."userId" FROM "commentReactions" WHERE '
          + `"commentReactions"."commentId" = '${commentId}' AND `
          + `"commentReactions"."${JSON.parse(isLike) ? 'isLike' : 'isDisLike'}" = '${true}')`
        )
      }
    });
    return UserModel.findAll({
      where
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
