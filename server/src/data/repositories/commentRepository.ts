import Sequelize from 'sequelize';
import { CommentModel, CommentReactionModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = (bool: boolean) => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
  async getComments(postId: string) {
    return this.model.findAll({
      group: [
        'comment.id',
        'comment.postId',
        'user.id',
        'user->image.id'
      ],
      where: { postId },
      attributes: {
        include: [
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(true))), 'likeCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: [{
          model: ImageModel,
          attributes: ['id', 'link']
        }]
      },
      {
        model: CommentReactionModel,
        attributes: [],
        duplicating: false
      }]
    });
  }

  getCommentById(id: string) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(true))), 'likeCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: [{
          model: ImageModel,
          attributes: ['id', 'link']
        }]
      },
      {
        model: CommentReactionModel,
        attributes: [],
        duplicating: false
      }]
    });
  }
  getUserById(id: string) {
    return this.model.findOne({
      where: { id },
      attributes: ['userId']
    });
  }
}

export default new CommentRepository(CommentModel);

