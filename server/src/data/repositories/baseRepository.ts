import { ModelCtor } from 'sequelize';
import { sequelizeModels } from '../modelsAttribute/combine';

export default class BaseRepository {
  constructor(readonly model: ModelCtor<sequelizeModels>) {}

  getAll() {
    return this.model.findAll();
  }

  getById(id: string) {
    return this.model.findByPk(id);
  }

  create(data: sequelizeModels) {
    return this.model.create(data);
  }

  async updateById(id: string, data: sequelizeModels) {
    const result = await this.model.update(data, {
      where: { id },
      returning: true
    });

    return result[1];
  }

  deleteById(id: string) {
    return this.model.destroy({
      where: { id }
    });
  }
}
