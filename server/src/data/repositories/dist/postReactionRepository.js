"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var sequelize_1 = require("sequelize");
var index_1 = require("../models/index");
var baseRepository_1 = require("./baseRepository");
var PostReactionRepository = /** @class */ (function (_super) {
    __extends(PostReactionRepository, _super);
    function PostReactionRepository() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PostReactionRepository.prototype.getPostReaction = function (userId, postId) {
        return this.model.findOne({
            group: [
                'postReaction.id',
                'post.id'
            ],
            where: { userId: userId, postId: postId },
            include: [{
                    model: index_1.PostModel,
                    attributes: ['id', 'userId']
                }]
        });
    };
    PostReactionRepository.prototype.getReactionUsers = function (postId, isLike) {
        var _a;
        var where = {};
        Object.assign(where, {
            id: (_a = {},
                _a[sequelize_1["default"].Op["in"]] = sequelize_1["default"].literal("(SELECT \"postReactions\".\"userId\" FROM \"postReactions\" WHERE \"postReactions\".\"postId\" = '" + postId + "' AND "
                    + ("\"postReactions\".\"" + (JSON.parse(isLike) ? 'isLike' : 'isDisLike') + "\" = '" + true + "')")),
                _a)
        });
        return index_1.UserModel.findAll({
            where: where
        });
    };
    return PostReactionRepository;
}(baseRepository_1["default"]));
exports["default"] = new PostReactionRepository(index_1.PostReactionModel);
