"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var sequelize_1 = require("sequelize");
var models_1 = require("../models");
var baseRepository_1 = require("./baseRepository");
var CommentReactionRepository = /** @class */ (function (_super) {
    __extends(CommentReactionRepository, _super);
    function CommentReactionRepository() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CommentReactionRepository.prototype.getCommentReaction = function (userId, commentId) {
        return this.model.findOne({
            group: [
                'commentReaction.id',
                'comment.id'
            ],
            where: { userId: userId, commentId: commentId },
            include: [{
                    model: models_1.CommentModel,
                    attributes: ['id', 'userId']
                }]
        });
    };
    CommentReactionRepository.prototype.getReactionUsers = function (commentId, isLike) {
        var _a;
        var where = {};
        Object.assign(where, {
            id: (_a = {},
                _a[sequelize_1["default"].Op["in"]] = sequelize_1["default"].literal('(SELECT "commentReactions"."userId" FROM "commentReactions" WHERE '
                    + ("\"commentReactions\".\"commentId\" = '" + commentId + "' AND ")
                    + ("\"commentReactions\".\"" + (JSON.parse(isLike) ? 'isLike' : 'isDisLike') + "\" = '" + true + "')")),
                _a)
        });
        return models_1.UserModel.findAll({
            where: where
        });
    };
    return CommentReactionRepository;
}(baseRepository_1["default"]));
exports["default"] = new CommentReactionRepository(models_1.CommentReactionModel);
