import Sequelize from 'sequelize';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import IFilter from '../../models/filter';
import BaseRepository from './baseRepository';

const likeCase = (bool: boolean) => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter: IFilter) {
    const {
      from: offset = '0',
      count: limit = '10',
      userId,
      include,
      additional
    } = filter;
    const where = {};
    if (userId) {
      if (include && JSON.parse(include)) {
        Object.assign(where, { userId });
      } else if (!additional) {
        Object.assign(where, { userId: { [Sequelize.Op.not]: userId } });
      } else if (additional === 'like') {
        Object.assign(where, {
          id: {
            [Sequelize.Op.in]: Sequelize.literal(
              `(SELECT "postReactions"."postId" FROM "postReactions" WHERE "postReactions"."userId" = '${userId}')`
            )
          }
        });
      }
    } 
    return this.model.findAll({
      where,
      attributes: {
        include: [
          [Sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(true))), 'likeCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'email'],
        include: [{
          model: ImageModel,
          attributes: ['id', 'link']
        }]
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset: JSON.parse(offset as string),
      limit: JSON.parse(limit as string)
    });
  }

  getPostById(id: string) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [Sequelize.literal(`
                    (SELECT COUNT(*)
                    FROM "comments" as "comment"
                    WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(true))), 'likeCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        include: [{
          model: UserModel,
          attributes: ['id', 'username', 'email'],
          include: [{
            model: ImageModel,
            attributes: ['id', 'link']
          }]
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'email'],
        include: [{
          model: ImageModel,
          attributes: ['id', 'link']
        }]
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
  getUserById(id: string) {
    return this.model.findOne({
      where: { id },
      attributes: ['userId']
    });
  }
}

export default new PostRepository(PostModel);
