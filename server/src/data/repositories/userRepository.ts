import { UserModel, ImageModel } from '../models';
import UserCreationAttributes from '../modelsAttribute/user';
import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  addUser(user: UserCreationAttributes) {
    return this.create(user);
  }

  getByEmail(email: string) {
    return this.model.findOne({ where: { email } });
  }

  getByUsername(username: string) {
    return this.model.findOne({ where: { username } });
  }

  getByResetToken(token: string) {
    return this.model.findOne({ where: { resetPasswordToken: token } });
  }

  getUserById(id: string) {
    return this.model.findOne({
      group: [
        'user.id',
        'image.id'
      ],
      where: { id },
      include: {
        model: ImageModel,
        attributes: ['id', 'link']
      }
    });
  }
}

export default new UserRepository(UserModel);
