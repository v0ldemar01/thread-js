"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable no-console */
var _require = require('sequelize'),
    QueryTypes = _require.QueryTypes;

var _require2 = require('../seed-data/usersSeed'),
    usersSeed = _require2.usersSeed,
    userImagesSeed = _require2.userImagesSeed;

var _require3 = require('../seed-data/postsSeed'),
    postsSeed = _require3.postsSeed,
    postImagesSeed = _require3.postImagesSeed;

var commentsSeed = require('../seed-data/commentsSeed');

var randomIndex = function randomIndex(length) {
  return Math.floor(Math.random() * length);
};

var mapLinks = function mapLinks(images) {
  return images.map(function (x) {
    return "'".concat(x.link, "'");
  }).join(',');
};

module.exports = {
  up: function up(queryInterface) {
    var options, userImagesQuery, userImages, postImagesQuery, postImages, usersMappedSeed, users, postsMappedSeed, posts, commentsMappedSeed, postReactionsMappedSeed;
    return regeneratorRuntime.async(function up$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            options = {
              type: QueryTypes.SELECT
            }; // Add images.

            _context.next = 4;
            return regeneratorRuntime.awrap(queryInterface.bulkInsert('images', userImagesSeed.concat(postImagesSeed), {}));

          case 4:
            userImagesQuery = "SELECT id FROM \"images\" WHERE link IN (".concat(mapLinks(userImagesSeed), ");");
            _context.next = 7;
            return regeneratorRuntime.awrap(queryInterface.sequelize.query(userImagesQuery, options));

          case 7:
            userImages = _context.sent;
            postImagesQuery = "SELECT id FROM \"images\" WHERE link IN (".concat(mapLinks(postImagesSeed), ");");
            _context.next = 11;
            return regeneratorRuntime.awrap(queryInterface.sequelize.query(postImagesQuery, options));

          case 11:
            postImages = _context.sent;
            // Add users.
            usersMappedSeed = usersSeed.map(function (user, i) {
              return _objectSpread({}, user, {
                imageId: userImages[i] ? userImages[i].id : null
              });
            });
            _context.next = 15;
            return regeneratorRuntime.awrap(queryInterface.bulkInsert('users', usersMappedSeed, {}));

          case 15:
            _context.next = 17;
            return regeneratorRuntime.awrap(queryInterface.sequelize.query('SELECT id FROM "users";', options));

          case 17:
            users = _context.sent;
            // Add posts.
            postsMappedSeed = postsSeed.map(function (post, i) {
              return _objectSpread({}, post, {
                userId: users[randomIndex(users.length)].id,
                imageId: postImages[i] ? postImages[i].id : null
              });
            });
            _context.next = 21;
            return regeneratorRuntime.awrap(queryInterface.bulkInsert('posts', postsMappedSeed, {}));

          case 21:
            _context.next = 23;
            return regeneratorRuntime.awrap(queryInterface.sequelize.query('SELECT id FROM "posts";', options));

          case 23:
            posts = _context.sent;
            // Add comments.
            commentsMappedSeed = commentsSeed.map(function (comment) {
              return _objectSpread({}, comment, {
                userId: users[randomIndex(users.length)].id,
                postId: posts[randomIndex(posts.length)].id
              });
            });
            _context.next = 27;
            return regeneratorRuntime.awrap(queryInterface.bulkInsert('comments', commentsMappedSeed, {}));

          case 27:
            // Add post reactions.
            postReactionsMappedSeed = users.map(function (user) {
              return {
                isLike: true,
                createdAt: new Date(),
                updatedAt: new Date(),
                userId: user.id,
                postId: posts[randomIndex(posts.length)].id
              };
            });
            _context.next = 30;
            return regeneratorRuntime.awrap(queryInterface.bulkInsert('postReactions', postReactionsMappedSeed, {}));

          case 30:
            _context.next = 35;
            break;

          case 32:
            _context.prev = 32;
            _context.t0 = _context["catch"](0);
            console.log("Seeding error: ".concat(_context.t0));

          case 35:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[0, 32]]);
  },
  down: function down(queryInterface) {
    return regeneratorRuntime.async(function down$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return regeneratorRuntime.awrap(queryInterface.bulkDelete('postReactions', null, {}));

          case 3:
            _context2.next = 5;
            return regeneratorRuntime.awrap(queryInterface.bulkDelete('comments', null, {}));

          case 5:
            _context2.next = 7;
            return regeneratorRuntime.awrap(queryInterface.bulkDelete('posts', null, {}));

          case 7:
            _context2.next = 9;
            return regeneratorRuntime.awrap(queryInterface.bulkDelete('users', null, {}));

          case 9:
            _context2.next = 11;
            return regeneratorRuntime.awrap(queryInterface.bulkDelete('images', null, {}));

          case 11:
            _context2.next = 16;
            break;

          case 13:
            _context2.prev = 13;
            _context2.t0 = _context2["catch"](0);
            console.log("Seeding error: ".concat(_context2.t0));

          case 16:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[0, 13]]);
  }
};