import orm from '../db/connection';
import associate from '../db/associations';
import User from './user';
import Post from './post';
import PostReaction from './postReaction';
import Comment from './comment';
import CommentReaction from './commentReaction';
import Image from './image';

const UserWrap = User(orm);
const PostWrap = Post(orm);
const PostReactionWrap = PostReaction(orm);
const CommentWrap = Comment(orm);
const CommentReactionWrap = CommentReaction(orm);
const ImageWrap = Image(orm);

associate({
  User: UserWrap,
  Post: PostWrap,
  PostReaction: PostReactionWrap,
  CommentReaction: CommentReactionWrap,
  Comment: CommentWrap,
  Image: ImageWrap
});

export {
  UserWrap as UserModel,
  PostWrap as PostModel,
  PostReactionWrap as PostReactionModel,
  CommentReactionWrap as CommentReactionModel,
  CommentWrap as CommentModel,
  ImageWrap as ImageModel
};
