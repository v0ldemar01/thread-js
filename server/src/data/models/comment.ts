import { Sequelize, DataTypes } from 'sequelize';
import CommentCreationAttributes from '../modelsAttribute/comment';

export default (orm: Sequelize)  => {
  const Comment = orm.define<CommentCreationAttributes>('comment', {
    body: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Comment;
};
