import { Sequelize, DataTypes } from 'sequelize';
import PostCreationAttributes from '../modelsAttribute/post';

export default (orm: Sequelize) => {
  const Post = orm.define<PostCreationAttributes>('post', {
    body: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Post;
};
