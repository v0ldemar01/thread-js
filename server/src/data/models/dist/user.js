"use strict";
exports.__esModule = true;
var sequelize_1 = require("sequelize");
exports["default"] = (function (orm) {
    var User = orm.define('user', {
        email: {
            allowNull: false,
            type: sequelize_1.DataTypes.STRING
        },
        username: {
            allowNull: false,
            type: sequelize_1.DataTypes.STRING,
            unique: true
        },
        password: {
            allowNull: false,
            type: sequelize_1.DataTypes.STRING,
            unique: true
        },
        userStatus: {
            allowNull: true,
            type: sequelize_1.DataTypes.STRING
        },
        resetPasswordToken: {
            allowNull: true,
            type: sequelize_1.DataTypes.STRING
        },
        createdAt: sequelize_1.DataTypes.DATE,
        updatedAt: sequelize_1.DataTypes.DATE
    }, {});
    return User;
});
