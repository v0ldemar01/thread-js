"use strict";
exports.__esModule = true;
var sequelize_1 = require("sequelize");
exports["default"] = (function (orm) {
    var Post = orm.define('post', {
        body: {
            allowNull: false,
            type: sequelize_1.DataTypes.TEXT
        },
        createdAt: sequelize_1.DataTypes.DATE,
        updatedAt: sequelize_1.DataTypes.DATE
    }, {});
    return Post;
});
