"use strict";
exports.__esModule = true;
var sequelize_1 = require("sequelize");
exports["default"] = (function (orm) {
    var PostReaction = orm.define('postReaction', {
        isLike: {
            allowNull: false,
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false
        },
        isDisLike: {
            allowNull: false,
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false
        },
        createdAt: sequelize_1.DataTypes.DATE,
        updatedAt: sequelize_1.DataTypes.DATE
    }, {});
    return PostReaction;
});
