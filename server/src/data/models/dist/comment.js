"use strict";
exports.__esModule = true;
var sequelize_1 = require("sequelize");
exports["default"] = (function (orm) {
    var Comment = orm.define('comment', {
        body: {
            allowNull: false,
            type: sequelize_1.DataTypes.TEXT
        },
        createdAt: sequelize_1.DataTypes.DATE,
        updatedAt: sequelize_1.DataTypes.DATE
    }, {});
    return Comment;
});
