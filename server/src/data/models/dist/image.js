"use strict";
exports.__esModule = true;
var sequelize_1 = require("sequelize");
exports["default"] = (function (orm) {
    var Image = orm.define('image', {
        link: {
            allowNull: false,
            type: sequelize_1.DataTypes.STRING
        },
        deleteHash: {
            allowNull: false,
            type: sequelize_1.DataTypes.STRING
        },
        createdAt: sequelize_1.DataTypes.DATE,
        updatedAt: sequelize_1.DataTypes.DATE
    }, {});
    return Image;
});
