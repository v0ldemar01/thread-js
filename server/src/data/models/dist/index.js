"use strict";
exports.__esModule = true;
exports.ImageModel = exports.CommentModel = exports.CommentReactionModel = exports.PostReactionModel = exports.PostModel = exports.UserModel = void 0;
var connection_1 = require("../db/connection");
var associations_1 = require("../db/associations");
var user_1 = require("./user");
var post_1 = require("./post");
var postReaction_1 = require("./postReaction");
var comment_1 = require("./comment");
var commentReaction_1 = require("./commentReaction");
var image_1 = require("./image");
var UserWrap = user_1["default"](connection_1["default"]);
exports.UserModel = UserWrap;
var PostWrap = post_1["default"](connection_1["default"]);
exports.PostModel = PostWrap;
var PostReactionWrap = postReaction_1["default"](connection_1["default"]);
exports.PostReactionModel = PostReactionWrap;
var CommentWrap = comment_1["default"](connection_1["default"]);
exports.CommentModel = CommentWrap;
var CommentReactionWrap = commentReaction_1["default"](connection_1["default"]);
exports.CommentReactionModel = CommentReactionWrap;
var ImageWrap = image_1["default"](connection_1["default"]);
exports.ImageModel = ImageWrap;
associations_1["default"]({
    User: UserWrap,
    Post: PostWrap,
    PostReaction: PostReactionWrap,
    CommentReaction: CommentReactionWrap,
    Comment: CommentWrap,
    Image: ImageWrap
});
