import { Sequelize, DataTypes } from 'sequelize';
import ImageCreationAttributes from '../modelsAttribute/image';

export default (orm: Sequelize) => {
  const Image = orm.define<ImageCreationAttributes>('image', {
    link: {
      allowNull: false,
      type: DataTypes.STRING
    },
    deleteHash: {
      allowNull: false,
      type: DataTypes.STRING
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Image;
};
