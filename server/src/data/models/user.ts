import { Sequelize, DataTypes } from 'sequelize';
import UserCreationAttributes from '../modelsAttribute/user';

export default (orm: Sequelize) => {
  const User = orm.define<UserCreationAttributes>('user', {
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    userStatus: {
      allowNull: true,
      type: DataTypes.STRING
    },
    resetPasswordToken: {
      allowNull: true,
      type: DataTypes.STRING
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
