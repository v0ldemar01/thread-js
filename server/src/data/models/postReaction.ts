import { Sequelize, DataTypes } from 'sequelize';
import PostReactionCreationAttributes from '../modelsAttribute/postReaction';

export default (orm: Sequelize) => {
  const PostReaction = orm.define<PostReactionCreationAttributes>('postReaction', {
    isLike: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isDisLike: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return PostReaction;
};
