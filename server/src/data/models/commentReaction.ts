import { Sequelize, DataTypes } from 'sequelize';
import CommentReactionCreationAttributes from '../modelsAttribute/commentReaction';

export default (orm: Sequelize) => {
  const CommentReaction = orm.define<CommentReactionCreationAttributes>('commentReaction', {
    isLike: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isDisLike: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});
  return CommentReaction;
};
