import { QueryInterface, DataTypes } from 'sequelize';

export default {
  up: (queryInterface: QueryInterface) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'imageId', {
        type: DataTypes.UUID,
        references: {
          model: 'images',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('posts', 'imageId', {
        type: DataTypes.UUID,
        references: {
          model: 'images',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('posts', 'userId', {
        type: DataTypes.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('postReactions', 'userId', {
        type: DataTypes.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('postReactions', 'postId', {
        type: DataTypes.UUID,
        references: {
          model: 'posts',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('commentReactions', 'userId', {
        type: DataTypes.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('commentReactions', 'commentId', {
        type: DataTypes.UUID,
        references: {
          model: 'comments',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('comments', 'userId', {
        type: DataTypes.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('comments', 'postId', {
        type: DataTypes.UUID,
        references: {
          model: 'posts',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction })
    ])),

  down: (queryInterface: QueryInterface) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'imageId', { transaction }),
      queryInterface.removeColumn('posts', 'imageId', { transaction }),
      queryInterface.removeColumn('posts', 'userId', { transaction }),
      queryInterface.removeColumn('postReactions', 'userId', { transaction }),
      queryInterface.removeColumn('postReactions', 'postId', { transaction }),
      queryInterface.removeColumn('commentReactions', 'userId', { transaction }),
      queryInterface.removeColumn('commentReactions', 'commentId', { transaction }),
      queryInterface.removeColumn('comments', 'userId', { transaction }),
      queryInterface.removeColumn('comments', 'postId', { transaction })
    ]))
};
