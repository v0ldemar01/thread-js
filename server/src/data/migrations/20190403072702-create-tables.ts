import { QueryInterface, DataTypes, literal } from 'sequelize';

export default {
  up: (queryInterface: QueryInterface) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        email: {
          allowNull: false,
          type: DataTypes.STRING,
          unique: true
        },
        username: {
          allowNull: false,
          type: DataTypes.STRING,
          unique: true
        },
        password: {
          allowNull: false,
          type: DataTypes.STRING
        },
        userStatus: {
          allowNull: true,
          type: DataTypes.STRING
        },
        resetPasswordToken: {
          allowNull: true,
          type: DataTypes.STRING
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
      }, { transaction }),
      queryInterface.createTable('posts', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        body: {
          allowNull: false,
          type: DataTypes.TEXT
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
      }, { transaction }),
      queryInterface.createTable('comments', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        body: {
          allowNull: false,
          type: DataTypes.TEXT
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
      }, { transaction }),
      queryInterface.createTable('postReactions', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        isLike: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false
        },
        isDisLike: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
      }, { transaction }),
      queryInterface.createTable('commentReactions', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        isLike: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false
        },
        isDisLike: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
      }, { transaction }),
      queryInterface.createTable('images', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        link: {
          allowNull: false,
          type: DataTypes.STRING
        },
        deleteHash: {
          allowNull: false,
          type: DataTypes.STRING
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
      }, { transaction })
    ]))),

  down: (queryInterface: QueryInterface) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('users', { transaction }),
      queryInterface.dropTable('posts', { transaction }),
      queryInterface.dropTable('comments', { transaction }),
      queryInterface.dropTable('postReactions', { transaction }),
      queryInterface.dropTable('commentReactions', { transaction }),
      queryInterface.dropTable('images', { transaction })
    ]))
};
