"use strict";
exports.__esModule = true;
var sequelize_1 = require("sequelize");
exports["default"] = {
    up: function (queryInterface) { return queryInterface.sequelize
        .transaction(function (transaction) { return Promise.all([
        queryInterface.addColumn('users', 'imageId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'images',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('posts', 'imageId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'images',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('posts', 'userId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'users',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('postReactions', 'userId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'users',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('postReactions', 'postId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'posts',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('commentReactions', 'userId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'users',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('commentReactions', 'commentId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'comments',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('comments', 'userId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'users',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction }),
        queryInterface.addColumn('comments', 'postId', {
            type: sequelize_1.DataTypes.UUID,
            references: {
                model: 'posts',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }, { transaction: transaction })
    ]); }); },
    down: function (queryInterface) { return queryInterface.sequelize
        .transaction(function (transaction) { return Promise.all([
        queryInterface.removeColumn('users', 'imageId', { transaction: transaction }),
        queryInterface.removeColumn('posts', 'imageId', { transaction: transaction }),
        queryInterface.removeColumn('posts', 'userId', { transaction: transaction }),
        queryInterface.removeColumn('postReactions', 'userId', { transaction: transaction }),
        queryInterface.removeColumn('postReactions', 'postId', { transaction: transaction }),
        queryInterface.removeColumn('commentReactions', 'userId', { transaction: transaction }),
        queryInterface.removeColumn('commentReactions', 'commentId', { transaction: transaction }),
        queryInterface.removeColumn('comments', 'userId', { transaction: transaction }),
        queryInterface.removeColumn('comments', 'postId', { transaction: transaction })
    ]); }); }
};
