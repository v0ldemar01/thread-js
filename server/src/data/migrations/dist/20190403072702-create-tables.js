"use strict";
exports.__esModule = true;
var sequelize_1 = require("sequelize");
exports["default"] = {
    up: function (queryInterface) { return queryInterface.sequelize
        .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
        .then(function () { return queryInterface.sequelize.transaction(function (transaction) { return Promise.all([
        queryInterface.createTable('users', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: sequelize_1.DataTypes.UUID,
                defaultValue: sequelize_1.literal('gen_random_uuid()')
            },
            email: {
                allowNull: false,
                type: sequelize_1.DataTypes.STRING,
                unique: true
            },
            username: {
                allowNull: false,
                type: sequelize_1.DataTypes.STRING,
                unique: true
            },
            password: {
                allowNull: false,
                type: sequelize_1.DataTypes.STRING
            },
            userStatus: {
                allowNull: true,
                type: sequelize_1.DataTypes.STRING
            },
            resetPasswordToken: {
                allowNull: true,
                type: sequelize_1.DataTypes.STRING
            },
            createdAt: sequelize_1.DataTypes.DATE,
            updatedAt: sequelize_1.DataTypes.DATE
        }, { transaction: transaction }),
        queryInterface.createTable('posts', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: sequelize_1.DataTypes.UUID,
                defaultValue: sequelize_1.literal('gen_random_uuid()')
            },
            body: {
                allowNull: false,
                type: sequelize_1.DataTypes.TEXT
            },
            createdAt: sequelize_1.DataTypes.DATE,
            updatedAt: sequelize_1.DataTypes.DATE
        }, { transaction: transaction }),
        queryInterface.createTable('comments', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: sequelize_1.DataTypes.UUID,
                defaultValue: sequelize_1.literal('gen_random_uuid()')
            },
            body: {
                allowNull: false,
                type: sequelize_1.DataTypes.TEXT
            },
            createdAt: sequelize_1.DataTypes.DATE,
            updatedAt: sequelize_1.DataTypes.DATE
        }, { transaction: transaction }),
        queryInterface.createTable('postReactions', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: sequelize_1.DataTypes.UUID,
                defaultValue: sequelize_1.literal('gen_random_uuid()')
            },
            isLike: {
                allowNull: false,
                type: sequelize_1.DataTypes.BOOLEAN,
                defaultValue: false
            },
            isDisLike: {
                allowNull: false,
                type: sequelize_1.DataTypes.BOOLEAN,
                defaultValue: false
            },
            createdAt: sequelize_1.DataTypes.DATE,
            updatedAt: sequelize_1.DataTypes.DATE
        }, { transaction: transaction }),
        queryInterface.createTable('commentReactions', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: sequelize_1.DataTypes.UUID,
                defaultValue: sequelize_1.literal('gen_random_uuid()')
            },
            isLike: {
                allowNull: false,
                type: sequelize_1.DataTypes.BOOLEAN,
                defaultValue: false
            },
            isDisLike: {
                allowNull: false,
                type: sequelize_1.DataTypes.BOOLEAN,
                defaultValue: false
            },
            createdAt: sequelize_1.DataTypes.DATE,
            updatedAt: sequelize_1.DataTypes.DATE
        }, { transaction: transaction }),
        queryInterface.createTable('images', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: sequelize_1.DataTypes.UUID,
                defaultValue: sequelize_1.literal('gen_random_uuid()')
            },
            link: {
                allowNull: false,
                type: sequelize_1.DataTypes.STRING
            },
            deleteHash: {
                allowNull: false,
                type: sequelize_1.DataTypes.STRING
            },
            createdAt: sequelize_1.DataTypes.DATE,
            updatedAt: sequelize_1.DataTypes.DATE
        }, { transaction: transaction })
    ]); }); }); },
    down: function (queryInterface) { return queryInterface.sequelize
        .transaction(function (transaction) { return Promise.all([
        queryInterface.dropTable('users', { transaction: transaction }),
        queryInterface.dropTable('posts', { transaction: transaction }),
        queryInterface.dropTable('comments', { transaction: transaction }),
        queryInterface.dropTable('postReactions', { transaction: transaction }),
        queryInterface.dropTable('commentReactions', { transaction: transaction }),
        queryInterface.dropTable('images', { transaction: transaction })
    ]); }); }
};
