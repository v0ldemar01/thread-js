"use strict";
exports.__esModule = true;
exports["default"] = (function (io) { return function (req, res, next) {
    req.io = io;
    next();
}; });
