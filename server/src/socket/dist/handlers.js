"use strict";
exports.__esModule = true;
exports["default"] = (function (io) {
    io.on('connection', function (socket) {
        console.log(socket.id + " is connected");
        socket.on('disconnect', function () {
            console.log(socket.id + " is disconnected");
        });
    });
});
