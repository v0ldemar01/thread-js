import { Response, NextFunction } from 'express';
import IExtRequest from '../models/request'
import IO from 'socket.io';

export default (io: IO.Server) => (req: IExtRequest, res: Response, next: NextFunction) => {
  req.io = io;
  next();
};
