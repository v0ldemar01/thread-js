"use strict";
exports.__esModule = true;
exports.REGEXP_EMAIL = exports.REGEXP_PHONE = exports.HTTP_STATUS_ERROR_BAD_REQUEST = void 0;
exports.HTTP_STATUS_ERROR_BAD_REQUEST = 400;
exports.REGEXP_PHONE = /^\+380(\d{9})$/;
exports.REGEXP_EMAIL = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail.com$/;
