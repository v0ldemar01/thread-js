import { Router, Request, Response, NextFunction  } from 'express';
import IExtRequest from '../../models/request';
import UserCreationAttributes from '../../data/modelsAttribute/user';
import IO from 'socket.io';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req: IExtRequest, res: Response, next: NextFunction) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req: IExtRequest, res: Response, next: NextFunction) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .get('/info/:id', (req: IExtRequest, res: Response, next: NextFunction) => postService.getReactionUsers(req.params.id, req.query)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req: IExtRequest, res: Response, next: NextFunction) => postService.create((req.user as UserCreationAttributes).id as string, req.body)
    .then(post => {
      (req.io as IO.Server).emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req: IExtRequest, res: Response, next: NextFunction) => postService.setReaction((req.user as UserCreationAttributes).id as string, req.body)
    .then(post => {
      if (post && (post.id !== (req.user as UserCreationAttributes).id)) {
        // notify a user if someone (not himself) liked his post
        (req.io as IO.Server).to(post.id as string).emit('like', 'Your post was liked!');
      }
      return res.send(post);
    })
    .catch(next))
  .put('/:id', (req: IExtRequest, res: Response, next: NextFunction) => postService.update(req.params.id, req.body)
    .then(post => {
      (req.io as IO.Server).emit('editing_post', post); // notify all users that the post was edited
      return res.send(post);
    })
    .catch(next));

export default router;
