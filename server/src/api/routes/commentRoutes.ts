import { Router, Response, NextFunction  } from 'express';
import IExtRequest from '../../models/request';
import UserCreationAttributes from '../../data/modelsAttribute/user';
import MessageCreationAttributes from '../../data/modelsAttribute/user';
import IO from 'socket.io';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/', (req: IExtRequest, res: Response, next: NextFunction) => commentService.getComments(req.query)
    .then(comment => res.send(comment))
    .catch(next))
  .get('/:id', (req: IExtRequest, res: Response, next: NextFunction) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .get('/info/:id', (req: IExtRequest, res: Response, next: NextFunction) => commentService.getReactionUsers(req.params.id, req.query)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req: IExtRequest, res: Response, next: NextFunction) => commentService.create((req.user as UserCreationAttributes).id as string, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req: IExtRequest, res: Response, next: NextFunction) => commentService.setReaction((req.user as UserCreationAttributes).id as string, req.body)
    .then(comment => {
      if (comment && (comment.id !== (req.user as UserCreationAttributes).id)) {
        // notify a user if someone (not himself) liked his post
        (req.io as IO.Server).to(comment.id as string).emit('like', 'Your comment was liked!');
      }
      return res.send(comment);
    })
    .catch(next))
  .put('/:id', (req: IExtRequest, res: Response, next: NextFunction) => commentService.update(req.params.id, req.body)
    .then(post => res.send(post))
    .catch(next));

export default router;
