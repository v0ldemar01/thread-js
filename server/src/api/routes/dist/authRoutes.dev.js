"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var authService = _interopRequireWildcard(require("../services/authService"));

var userService = _interopRequireWildcard(require("../services/userService"));

var _authenticationMiddleware = _interopRequireDefault(require("../middlewares/authenticationMiddleware"));

var _registrationMiddleware = _interopRequireDefault(require("../middlewares/registrationMiddleware"));

var _jwtMiddleware = _interopRequireDefault(require("../middlewares/jwtMiddleware"));

var _userMiddleware = _interopRequireDefault(require("../middlewares/userMiddleware"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var router = (0, _express.Router)(); // user added to the request (req.user) in a strategy, see passport config

router.post('/login', _authenticationMiddleware["default"], function (req, res, next) {
  return authService.login(req.user).then(function (data) {
    return res.send(data);
  })["catch"](next);
}).post('/register', _registrationMiddleware["default"], function (req, res, next) {
  return authService.register(req.user).then(function (data) {
    return res.send(data);
  })["catch"](next);
}).get('/user', _jwtMiddleware["default"], function (req, res, next) {
  return userService.getUserById(req.user.id).then(function (data) {
    return res.send(data);
  })["catch"](next);
}).post('/forgotPassword', _userMiddleware["default"], function (req, res, next) {
  return authService.sendEmail(req.body).then(function (data) {
    return res.send(data);
  })["catch"](next);
}).put('/changePassword/:token', function (req, res, next) {
  return authService.changePassword(req.params.token, req.body).then(function (data) {
    return res.send(data);
  })["catch"](next);
});
var _default = router;
exports["default"] = _default;