"use strict";
exports.__esModule = true;
var express_1 = require("express");
var userService = require("../services/userService");
var userMiddleware_1 = require("../middlewares/userMiddleware");
var router = express_1.Router();
// user added to the request (req.user) in a strategy, see passport config
router
    .get('/:id', function (req, res, next) { return userService.getUserById(req.params.id)
    .then(function (data) { return res.send(data); })["catch"](next); })
    .put('/:id', userMiddleware_1["default"], function (req, res, next) { return userService.update(req.params.id, req.body)
    .then(function (post) { return res.send(post); })["catch"](next); })
    .post('/email', function (req, res, next) { return userService.NotifyByEmail(req.user.username, req.body)
    .then(function (data) { return res.send(data); })["catch"](next); })
    .post('/shareEmail', userMiddleware_1["default"], function (req, res, next) { return userService.sendSharedToEmail(req.body)
    .then(function (data) { return res.send(data); })["catch"](next); });
exports["default"] = router;
