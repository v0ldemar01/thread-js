"use strict";
exports.__esModule = true;
var express_1 = require("express");
var commentService = require("../services/commentService");
var router = express_1.Router();
router
    .get('/', function (req, res, next) { return commentService.getComments(req.query)
    .then(function (comment) { return res.send(comment); })["catch"](next); })
    .get('/:id', function (req, res, next) { return commentService.getCommentById(req.params.id)
    .then(function (comment) { return res.send(comment); })["catch"](next); })
    .get('/info/:id', function (req, res, next) { return commentService.getReactionUsers(req.params.id, req.query)
    .then(function (comment) { return res.send(comment); })["catch"](next); })
    .post('/', function (req, res, next) { return commentService.create(req.user.id, req.body)
    .then(function (comment) { return res.send(comment); })["catch"](next); })
    .put('/react', function (req, res, next) { return commentService.setReaction(req.user.id, req.body)
    .then(function (comment) {
    if (comment && (comment.id !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        req.io.to(comment.id).emit('like', 'Your comment was liked!');
    }
    return res.send(comment);
})["catch"](next); })
    .put('/:id', function (req, res, next) { return commentService.update(req.params.id, req.body)
    .then(function (post) { return res.send(post); })["catch"](next); });
exports["default"] = router;
