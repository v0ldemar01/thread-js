"use strict";
exports.__esModule = true;
var express_1 = require("express");
var postService = require("../services/postService");
var router = express_1.Router();
router
    .get('/', function (req, res, next) { return postService.getPosts(req.query)
    .then(function (posts) { return res.send(posts); })["catch"](next); })
    .get('/:id', function (req, res, next) { return postService.getPostById(req.params.id)
    .then(function (post) { return res.send(post); })["catch"](next); })
    .get('/info/:id', function (req, res, next) { return postService.getReactionUsers(req.params.id, req.query)
    .then(function (post) { return res.send(post); })["catch"](next); })
    .post('/', function (req, res, next) { return postService.create(req.user.id, req.body)
    .then(function (post) {
    req.io.emit('new_post', post); // notify all users that a new post was created
    return res.send(post);
})["catch"](next); })
    .put('/react', function (req, res, next) { return postService.setReaction(req.user.id, req.body)
    .then(function (post) {
    if (post && (post.id !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        req.io.to(post.id).emit('like', 'Your post was liked!');
    }
    return res.send(post);
})["catch"](next); })
    .put('/:id', function (req, res, next) { return postService.update(req.params.id, req.body)
    .then(function (post) {
    req.io.emit('editing_post', post); // notify all users that the post was edited
    return res.send(post);
})["catch"](next); });
exports["default"] = router;
