"use strict";
exports.__esModule = true;
var express_1 = require("express");
var authService = require("../services/authService");
var userService = require("../services/userService");
var authenticationMiddleware_1 = require("../middlewares/authenticationMiddleware");
var registrationMiddleware_1 = require("../middlewares/registrationMiddleware");
var jwtMiddleware_1 = require("../middlewares/jwtMiddleware");
var userMiddleware_1 = require("../middlewares/userMiddleware");
var router = express_1.Router();
// user added to the request (req.user) in a strategy, see passport config
router
    .post('/login', authenticationMiddleware_1["default"], function (req, res, next) { return authService.login(req.user)
    .then(function (data) { return res.send(data); })["catch"](next); })
    .post('/register', registrationMiddleware_1["default"], function (req, res, next) { return authService.register(req.user)
    .then(function (data) { return res.send(data); })["catch"](next); })
    .get('/user', jwtMiddleware_1["default"], function (req, res, next) { return userService.getUserById(req.user.id)
    .then(function (data) { return res.send(data); })["catch"](next); })
    .post('/forgotPassword', userMiddleware_1["default"], function (req, res, next) { return authService.sendEmail(req.body)
    .then(function (data) { return res.send(data); })["catch"](next); })
    .put('/changePassword/:token', function (req, res, next) { return authService.changePassword(req.params.token, req.body)
    .then(function (data) { return res.send(data); })["catch"](next); });
exports["default"] = router;
