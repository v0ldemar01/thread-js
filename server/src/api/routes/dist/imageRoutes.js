"use strict";
exports.__esModule = true;
var express_1 = require("express");
var imageService = require("../services/imageService");
var imageMiddleware_1 = require("../middlewares/imageMiddleware");
var router = express_1.Router();
router
    .post('/', imageMiddleware_1["default"], function (req, res, next) { return imageService.upload(req.file)
    .then(function (image) { return res.send(image); })["catch"](next); });
exports["default"] = router;
