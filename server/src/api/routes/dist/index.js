"use strict";
exports.__esModule = true;
var authRoutes_1 = require("./authRoutes");
var userRoutes_1 = require("./userRoutes");
var postRoutes_1 = require("./postRoutes");
var commentRoutes_1 = require("./commentRoutes");
var imageRoutes_1 = require("./imageRoutes");
// register all routes
exports["default"] = (function (app) {
    app.use('/api/auth', authRoutes_1["default"]);
    app.use('/api/users', userRoutes_1["default"]);
    app.use('/api/posts', postRoutes_1["default"]);
    app.use('/api/comments', commentRoutes_1["default"]);
    app.use('/api/images', imageRoutes_1["default"]);
});
