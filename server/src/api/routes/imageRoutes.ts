import { Router, Response, NextFunction  } from 'express';
import IExtRequest from '../../models/request';
import * as imageService from '../services/imageService';
import imageMiddleware from '../middlewares/imageMiddleware';

const router = Router();

router
  .post('/', imageMiddleware, (req: IExtRequest, res: Response, next: NextFunction) => imageService.upload(req.file)
    .then(image => res.send(image))
    .catch(next));

export default router;
