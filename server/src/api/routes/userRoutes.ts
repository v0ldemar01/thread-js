import { Router, Response, NextFunction  } from 'express';
import IExtRequest from '../../models/request';
import * as userService from '../services/userService';
import userMiddleware from '../middlewares/userMiddleware';
import UserCreationAttributes from '../../data/modelsAttribute/user';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .get('/:id', (req: IExtRequest, res: Response, next: NextFunction) => userService.getUserById(req.params.id)
    .then(data => res.send(data))
    .catch(next))
  .put('/:id', userMiddleware, (req: IExtRequest, res: Response, next: NextFunction) => userService.update(req.params.id, req.body)
    .then(post => res.send(post))
    .catch(next))
  .post('/email', (req: IExtRequest, res: Response, next: NextFunction) => userService.NotifyByEmail((req.user as UserCreationAttributes).username as string, req.body)
    .then(data => res.send(data))
    .catch(next))
  .post('/shareEmail', userMiddleware, (req: IExtRequest, res: Response, next: NextFunction) => userService.sendSharedToEmail(req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
