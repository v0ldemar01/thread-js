import { Router, Response, NextFunction  } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import IExtRequest from '../../models/request';
import UserCreationAttributes from '../../data/modelsAttribute/user';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';
import userMiddleware from '../middlewares/userMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req: IExtRequest, res: Response, next: NextFunction) => authService.login(req.user as UserCreationAttributes)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req: IExtRequest, res: Response, next: NextFunction) => authService.register(req.user as UserCreationAttributes)
    .then(data => res.send(data))
    .catch(next))
  .get('/user', jwtMiddleware, (req: IExtRequest, res: Response, next: NextFunction) => userService.getUserById((req.user as UserCreationAttributes).id as string)
    .then(data => res.send(data))
    .catch(next))
  .post('/forgotPassword', userMiddleware, (req: IExtRequest, res: Response, next: NextFunction) => authService.sendEmail(req.body)
    .then(data => res.send(data))
    .catch(next))
  .put('/changePassword/:token', (req: IExtRequest, res: Response, next: NextFunction) => authService.changePassword(req.params.token, req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
