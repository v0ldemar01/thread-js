import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';
import CommentCreationAttributes from '../../data/modelsAttribute/comment';
import CommentReactionCreationAttributes from '../../data/modelsAttribute/commentReaction';
import IReaction from '../../models/reaction';
import IFilter from '../../models/filter';
import IReactionString from '../../models/reactionString';

export const create = async (userId: string, comment: CommentCreationAttributes) => {
  const newComment = await commentRepository.create({
    ...comment,
    userId
  });
  return commentRepository.getCommentById(newComment.id as string);
}

export const getComments = (filter: IFilter) => commentRepository.getComments(filter.postId as string);

export const getCommentById = (id: string) => commentRepository.getCommentById(id);

export const update = async (commentId: string, comment: CommentCreationAttributes) => {
  await commentRepository.updateById(commentId, comment);
  return commentRepository.getCommentById(commentId as string);
}

export const setReaction = async (userId: string, { commentId, isLike, isDisLike }: CommentReactionCreationAttributes) => {
  // define the callback for future use as a promise
  const switchPostReaction = async (react: IReaction) => {
    if ((isLike && react.isDisLike) || (isDisLike && react.isLike)) {
      await commentReactionRepository.deleteById(react.id as string);
      await commentReactionRepository.create({ userId, commentId, isLike, isDisLike } as CommentReactionCreationAttributes);
      return 'switch';
    }
  };
  const updateOrDelete = (react: IReaction) => (
    (isLike && react.isLike) || (isDisLike && react.isDisLike)
      ? commentReactionRepository.deleteById(react.id as string)
      : switchPostReaction(react)
  );
  const reaction = await commentReactionRepository.getCommentReaction(userId as string, commentId as string);

  reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike, isDisLike } as CommentReactionCreationAttributes);

  // the result is an integer when an entity is deleted
  const thisPost = await commentRepository.getCommentById(commentId as string);
  return thisPost;
};

export const getReactionUsers = (commentId: string, reaction: IReactionString) => 
  commentReactionRepository.getReactionUsers(commentId, reaction.isLike as string);
