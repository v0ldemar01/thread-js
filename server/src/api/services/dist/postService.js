"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.getReactionUsers = exports.setReaction = exports.update = exports.create = exports.getPostById = exports.getPosts = void 0;
var postRepository_1 = require("../../data/repositories/postRepository");
var postReactionRepository_1 = require("../../data/repositories/postReactionRepository");
exports.getPosts = function (filter) { return postRepository_1["default"].getPosts(filter); };
exports.getPostById = function (id) { return postRepository_1["default"].getPostById(id); };
exports.create = function (userId, post) { return __awaiter(void 0, void 0, void 0, function () {
    var newPost;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, postRepository_1["default"].create(__assign(__assign({}, post), { userId: userId }))];
            case 1:
                newPost = _a.sent();
                return [2 /*return*/, postRepository_1["default"].getPostById(newPost.id)];
        }
    });
}); };
exports.update = function (postId, post) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, postRepository_1["default"].updateById(postId, post)];
            case 1:
                _a.sent();
                return [2 /*return*/, postRepository_1["default"].getPostById(postId)];
        }
    });
}); };
exports.setReaction = function (userId, _a) {
    var postId = _a.postId, isLike = _a.isLike, isDisLike = _a.isDisLike;
    return __awaiter(void 0, void 0, void 0, function () {
        var switchPostReaction, updateOrDelete, reaction, _b, thisPost;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    switchPostReaction = function (react) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!((isLike && react.isDisLike) || (isDisLike && react.isLike))) return [3 /*break*/, 3];
                                    return [4 /*yield*/, postReactionRepository_1["default"].deleteById(react.id)];
                                case 1:
                                    _a.sent();
                                    return [4 /*yield*/, postReactionRepository_1["default"].create({ userId: userId, postId: postId, isLike: isLike, isDisLike: isDisLike })];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); };
                    updateOrDelete = function (react) { return ((isLike && react.isLike) || (isDisLike && react.isDisLike)
                        ? postReactionRepository_1["default"].deleteById(react.id)
                        : switchPostReaction(react)); };
                    return [4 /*yield*/, postReactionRepository_1["default"].getPostReaction(userId, postId)];
                case 1:
                    reaction = _c.sent();
                    if (!reaction) return [3 /*break*/, 3];
                    return [4 /*yield*/, updateOrDelete(reaction)];
                case 2:
                    _b = _c.sent();
                    return [3 /*break*/, 5];
                case 3: return [4 /*yield*/, postReactionRepository_1["default"].create({ userId: userId, postId: postId, isLike: isLike, isDisLike: isDisLike })];
                case 4:
                    _b = _c.sent();
                    _c.label = 5;
                case 5:
                    _b;
                    return [4 /*yield*/, postRepository_1["default"].getPostById(postId)];
                case 6:
                    thisPost = _c.sent();
                    return [2 /*return*/, thisPost];
            }
        });
    });
};
exports.getReactionUsers = function (postId, reaction) { return postReactionRepository_1["default"].getReactionUsers(postId, reaction.isLike); };
