"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.changePassword = exports.sendEmail = exports.register = exports.login = void 0;

var _tokenHelper = require("../../helpers/tokenHelper");

var _cryptoHelper = require("../../helpers/cryptoHelper");

var _userRepository = _interopRequireDefault(require("../../data/repositories/userRepository"));

var _resetTokenHelper = require("../../helpers/resetTokenHelper");

var _mail = require("../../utils/mail");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var login = function login(_ref) {
  var id;
  return regeneratorRuntime.async(function login$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          id = _ref.id;
          _context.t0 = (0, _tokenHelper.createToken)({
            id: id
          });
          _context.next = 4;
          return regeneratorRuntime.awrap(_userRepository["default"].getUserById(id));

        case 4:
          _context.t1 = _context.sent;
          return _context.abrupt("return", {
            token: _context.t0,
            user: _context.t1
          });

        case 6:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.login = login;

var register = function register(_ref2) {
  var password, userData, newUser;
  return regeneratorRuntime.async(function register$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          password = _ref2.password, userData = _objectWithoutProperties(_ref2, ["password"]);
          _context2.t0 = regeneratorRuntime;
          _context2.t1 = _userRepository["default"];
          _context2.t2 = _objectSpread;
          _context2.t3 = {};
          _context2.t4 = userData;
          _context2.next = 8;
          return regeneratorRuntime.awrap((0, _cryptoHelper.encrypt)(password));

        case 8:
          _context2.t5 = _context2.sent;
          _context2.t6 = {
            password: _context2.t5
          };
          _context2.t7 = (0, _context2.t2)(_context2.t3, _context2.t4, _context2.t6);
          _context2.t8 = _context2.t1.addUser.call(_context2.t1, _context2.t7);
          _context2.next = 14;
          return _context2.t0.awrap.call(_context2.t0, _context2.t8);

        case 14:
          newUser = _context2.sent;
          return _context2.abrupt("return", login(newUser));

        case 16:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.register = register;

var sendEmail = function sendEmail(_ref3) {
  var email, _ref4, id, token;

  return regeneratorRuntime.async(function sendEmail$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          email = _ref3.email;
          _context3.next = 3;
          return regeneratorRuntime.awrap(_userRepository["default"].getByEmail(email));

        case 3:
          _ref4 = _context3.sent;
          id = _ref4.id;
          token = (0, _resetTokenHelper.createResetToken)();
          _context3.next = 8;
          return regeneratorRuntime.awrap(_userRepository["default"].updateById(id, {
            resetPasswordToken: token
          }));

        case 8:
          return _context3.abrupt("return", (0, _mail.sendResetMail)(email, token));

        case 9:
        case "end":
          return _context3.stop();
      }
    }
  });
};

exports.sendEmail = sendEmail;

var changePassword = function changePassword(token, _ref5) {
  var password, _ref6, id;

  return regeneratorRuntime.async(function changePassword$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          password = _ref5.password;
          _context4.next = 3;
          return regeneratorRuntime.awrap(_userRepository["default"].getByResetToken(token));

        case 3:
          _ref6 = _context4.sent;
          id = _ref6.id;
          _context4.t0 = _userRepository["default"];
          _context4.t1 = id;
          _context4.next = 9;
          return regeneratorRuntime.awrap((0, _cryptoHelper.encrypt)(password));

        case 9:
          _context4.t2 = _context4.sent;
          _context4.t3 = {
            password: _context4.t2
          };
          return _context4.abrupt("return", _context4.t0.updateById.call(_context4.t0, _context4.t1, _context4.t3));

        case 12:
        case "end":
          return _context4.stop();
      }
    }
  });
};

exports.changePassword = changePassword;