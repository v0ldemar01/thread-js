"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getReactionUsers = exports.setReaction = exports.update = exports.getCommentById = exports.getComments = exports.create = void 0;

var _commentRepository = _interopRequireDefault(require("../../data/repositories/commentRepository"));

var _commentReactionRepository = _interopRequireDefault(require("../../data/repositories/commentReactionRepository"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var create = function create(userId, comment) {
  return _commentRepository["default"].create(_objectSpread({}, comment, {
    userId: userId
  }));
};

exports.create = create;

var getComments = function getComments(filter) {
  return _commentRepository["default"].getComments(filter);
};

exports.getComments = getComments;

var getCommentById = function getCommentById(id) {
  return _commentRepository["default"].getCommentById(id);
};

exports.getCommentById = getCommentById;

var update = function update(commentId, comment) {
  return _commentRepository["default"].updateById(commentId, comment);
};

exports.update = update;

var setReaction = function setReaction(userId, _ref) {
  var commentId, isLike, isDisLike, switchPostReaction, updateOrDelete, reaction, result;
  return regeneratorRuntime.async(function setReaction$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          commentId = _ref.commentId, isLike = _ref.isLike, isDisLike = _ref.isDisLike;

          // define the callback for future use as a promise
          switchPostReaction = function switchPostReaction(react) {
            return regeneratorRuntime.async(function switchPostReaction$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!(isLike && react.isDisLike || isDisLike && react.isLike)) {
                      _context.next = 6;
                      break;
                    }

                    _context.next = 3;
                    return regeneratorRuntime.awrap(_commentReactionRepository["default"].deleteById(react.id));

                  case 3:
                    _context.next = 5;
                    return regeneratorRuntime.awrap(_commentReactionRepository["default"].create({
                      userId: userId,
                      commentId: commentId,
                      isLike: isLike,
                      isDisLike: isDisLike
                    }));

                  case 5:
                    return _context.abrupt("return", 'switch');

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            });
          };

          updateOrDelete = function updateOrDelete(react) {
            return isLike && react.isLike || isDisLike && react.isDisLike ? _commentReactionRepository["default"].deleteById(react.id) : switchPostReaction(react);
          };

          _context2.next = 5;
          return regeneratorRuntime.awrap(_commentReactionRepository["default"].getCommentReaction(userId, commentId));

        case 5:
          reaction = _context2.sent;

          if (!reaction) {
            _context2.next = 12;
            break;
          }

          _context2.next = 9;
          return regeneratorRuntime.awrap(updateOrDelete(reaction));

        case 9:
          _context2.t0 = _context2.sent;
          _context2.next = 15;
          break;

        case 12:
          _context2.next = 14;
          return regeneratorRuntime.awrap(_commentReactionRepository["default"].create({
            userId: userId,
            commentId: commentId,
            isLike: isLike,
            isDisLike: isDisLike
          }));

        case 14:
          _context2.t0 = _context2.sent;

        case 15:
          result = _context2.t0;
          return _context2.abrupt("return", Number.isInteger(result) ? {} : _typeof(result) === 'object' ? _commentReactionRepository["default"].getCommentReaction(userId, commentId) : {
            action: result
          });

        case 17:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.setReaction = setReaction;

var getReactionUsers = function getReactionUsers(commentId, isLike) {
  return _commentReactionRepository["default"].getReactionUsers(commentId, isLike);
};

exports.getReactionUsers = getReactionUsers;