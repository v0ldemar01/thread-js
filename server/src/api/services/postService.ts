import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import PostCreationAttributes from '../../data/modelsAttribute/post';
import PostReactionCreationAttributes from '../../data/modelsAttribute/postReaction';
import IReaction from '../../models/reaction';
import IFilter from '../../models/filter';
import IReactionString from '../../models/reactionString';

export const getPosts = (filter: IFilter) => postRepository.getPosts(filter);

export const getPostById = (id: string) => postRepository.getPostById(id);

export const create = async (userId: string, post: PostCreationAttributes) => {
  const newPost = await postRepository.create({
    ...post,
    userId
  });
  return postRepository.getPostById(newPost.id as string);
}

export const update = async (postId: string, post: PostCreationAttributes) => {
  await postRepository.updateById(postId, post);
  return postRepository.getPostById(postId as string);
}

export const setReaction = async (userId: string, { postId, isLike, isDisLike }: PostReactionCreationAttributes) => {
  // define the callback for future use as a promise
  const switchPostReaction = async (react: IReaction) => {
    if ((isLike && react.isDisLike) || (isDisLike && react.isLike)) {
      await postReactionRepository.deleteById(react.id as string);
      await postReactionRepository.create({ userId, postId, isLike, isDisLike } as PostReactionCreationAttributes);
    }
  };

  const updateOrDelete = (react: IReaction) => (
    (isLike && react.isLike) || (isDisLike && react.isDisLike)
      ? postReactionRepository.deleteById(react.id as string)
      : switchPostReaction(react)
  );

  const reaction = await postReactionRepository.getPostReaction(userId as string, postId as string);

  reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike, isDisLike } as PostReactionCreationAttributes);

  // the result is an integer when an entity is deleted
  const thisPost = await postRepository.getPostById(postId as string);
  return thisPost;
};

export const getReactionUsers = (postId: string, reaction: IReactionString) => postReactionRepository.getReactionUsers(postId as string, reaction.isLike as string);
