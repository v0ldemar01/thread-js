import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';
import UserCreationAttributes from '../../data/modelsAttribute/user';
import { createResetToken } from '../../helpers/resetTokenHelper';
import { sendResetMail } from '../../utils/mail';

export const login = async ({ id }: UserCreationAttributes) => ({
  token: createToken({ id: id as string }),
  user: await userRepository.getUserById(id as string)
});

export const register = async ({ password, ...userData }: UserCreationAttributes) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  } as UserCreationAttributes);
  return login(newUser as UserCreationAttributes);
};

export const sendEmail = async ({ email }: UserCreationAttributes) => {
  const { id } = (await userRepository.getByEmail(email) as UserCreationAttributes);
  const token = createResetToken();
  await userRepository.updateById(id as string, { resetPasswordToken: token } as UserCreationAttributes);
  return sendResetMail(email, token);
};

export const changePassword = async (token: string, { password }: UserCreationAttributes) => {
  const { id } = (await userRepository.getByResetToken(token) as UserCreationAttributes);
  return userRepository.updateById(id as string, { password: await encrypt(password) } as UserCreationAttributes);
};
