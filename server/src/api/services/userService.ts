import userRepository from '../../data/repositories/userRepository';
import UserCreationAttributes from '../../data/modelsAttribute/user';
import {
  sendNotifyMail,
  sendSharedPostToEmail
} from '../../utils/mail';
import { encryptSync }  from '../../helpers/cryptoHelper';
import postRepository from '../../data/repositories/postRepository';
import commentRepository from '../../data/repositories/commentRepository';
import PostReactionCreationAttributes from '../../data/modelsAttribute/postReaction';
import CommentCreationAttributes from '../../data/modelsAttribute/comment';

export const getUserById = (userId: string) => userRepository.getUserById(userId);

export const update = (userId: string, user: UserCreationAttributes) => 
  userRepository.updateById(userId, {...user, password: encryptSync(user.password)});

export const NotifyByEmail = async (username: string, { isLike, postId, commentId }: { isLike: boolean, postId: string | null, commentId: string | null}) => {
  let data: PostReactionCreationAttributes | CommentCreationAttributes;
  if (postId) {
    data = (await postRepository.getUserById(postId as string) as PostReactionCreationAttributes);
  } else {
    data = (await commentRepository.getUserById(commentId as string) as CommentCreationAttributes);
  }
  const userId = data.userId;
  const { email } = (await userRepository.getUserById(userId as string) as UserCreationAttributes);
  return sendNotifyMail(email, username, isLike, postId ? 'Post ' : 'Comment');
};

export const sendSharedToEmail = ({ email, username, url }: {email: string, username: string, url: string }) => sendSharedPostToEmail(email, username, url);

