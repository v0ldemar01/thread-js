import { Response, NextFunction } from 'express';
import IExtRequest from '../../models/request';
import IHttpError from '../../models/error';

export default (err: IHttpError, req: IExtRequest, res: Response, next: NextFunction) => {
  if (res.headersSent) { // http://expressjs.com/en/guide/error-handling.html
    next(err);
  } else {
    const { status = 500, message = '' } = err;
    res.status(status).send({ status, message });
  }
};
