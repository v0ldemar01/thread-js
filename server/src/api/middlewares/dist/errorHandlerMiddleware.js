"use strict";
exports.__esModule = true;
exports["default"] = (function (err, req, res, next) {
    if (res.headersSent) { // http://expressjs.com/en/guide/error-handling.html
        next(err);
    }
    else {
        var _a = err.status, status = _a === void 0 ? 500 : _a, _b = err.message, message = _b === void 0 ? '' : _b;
        res.status(status).send({ status: status, message: message });
    }
});
