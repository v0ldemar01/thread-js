"use strict";
exports.__esModule = true;
var passport_1 = require("passport");
exports["default"] = passport_1["default"].authenticate('jwt', { session: false });
