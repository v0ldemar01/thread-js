"use strict";
exports.__esModule = true;
var jwtMiddleware_1 = require("./jwtMiddleware");
exports["default"] = (function (routesWhiteList) {
    if (routesWhiteList === void 0) { routesWhiteList = []; }
    return function (req, res, next) { return (routesWhiteList.some(function (route) { return req.path.includes(route); })
        ? next()
        : jwtMiddleware_1["default"](req, res, next) // auth the user if requested path isn't from the white list
    ); };
});
