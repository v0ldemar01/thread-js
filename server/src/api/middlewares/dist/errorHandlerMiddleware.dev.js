"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _request = _interopRequireDefault(require("../../models/request"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(err, req, res, next) {
  if (res.headersSent) {
    // http://expressjs.com/en/guide/error-handling.html
    next(err);
  } else {
    var _err$status = err.status,
        status = _err$status === void 0 ? 500 : _err$status,
        _err$message = err.message,
        message = _err$message === void 0 ? '' : _err$message;
    res.status(status).send({
      status: status,
      message: message
    });
  }
};

exports["default"] = _default;