"use strict";
exports.__esModule = true;
var constants_1 = require("../constants");
var validateEmail = function (trialEmail) {
    if (!trialEmail.trim()) {
        throw new Error('Email field must be filled!');
    }
    var checkRegexp = trialEmail.match(constants_1.REGEXP_EMAIL);
    if (!checkRegexp) {
        throw new Error('Email must be like name@gmail.com!');
    }
};
exports["default"] = (function (req, res, next) {
    try {
        validateEmail(req.body.email);
        next();
    }
    catch (err) {
        return res.status(constants_1.HTTP_STATUS_ERROR_BAD_REQUEST)
            .json({
            error: true,
            message: err.message
        });
    }
});
