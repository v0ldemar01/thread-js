import { Response, NextFunction } from 'express';
import IExtRequest from '../../models/request';
import jwtMiddleware from './jwtMiddleware';

export default (routesWhiteList = [] as string[]) => (req: IExtRequest, res: Response, next: NextFunction) => (
  routesWhiteList.some(route => req.path.includes(route))
    ? next()
    : jwtMiddleware(req, res, next) // auth the user if requested path isn't from the white list
);