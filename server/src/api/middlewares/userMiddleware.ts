import { Response, NextFunction } from 'express';
import IExtRequest from '../../models/request';
import {
  REGEXP_EMAIL,
  HTTP_STATUS_ERROR_BAD_REQUEST
} from '../constants';


const validateEmail = (trialEmail: string) => {
  if (!trialEmail.trim()) {
    throw new Error('Email field must be filled!');
  }
  const checkRegexp = trialEmail.match(REGEXP_EMAIL);
  if (!checkRegexp) {
    throw new Error('Email must be like name@gmail.com!');
  }
};

export default (req: IExtRequest, res: Response, next: NextFunction) => {
  try {
    validateEmail(req.body.email);
    next();
  } catch (err) {
    return res.status(HTTP_STATUS_ERROR_BAD_REQUEST)
      .json({
        error: true,
        message: err.message
      });
  }
};

