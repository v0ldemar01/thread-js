export default interface IReaction {
  id?: string;
  commentId?: string;
  postId?: string;
  userId?: string;
  isLike?: boolean;
  isDisLike?: boolean;
}