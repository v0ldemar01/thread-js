export default interface IFilter {
  from?: string;
  count?: string;
  userId?: string;
  postId?: string;
  include?: string;
  additional?: string;
}