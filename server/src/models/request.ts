import { Request } from 'express';
import IO from 'socket.io';

export default interface IExtRequest extends Request {
  io?: IO.Server;
}
