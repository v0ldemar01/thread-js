"use strict";

var bcrypt = require('bcrypt');

var saltRounds = 10;

exports.encrypt = function (data) {
  return bcrypt.hash(data, saltRounds);
};

exports.encryptSync = function (data) {
  return bcrypt.hashSync(data, saltRounds);
};

exports.compare = function (data, encrypted) {
  return bcrypt.compare(data, encrypted);
};