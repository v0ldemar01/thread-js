"use strict";
exports.__esModule = true;
exports.createToken = void 0;
var jsonwebtoken_1 = require("jsonwebtoken");
var jwtConfig_1 = require("../config/jwtConfig");
exports.createToken = function (data) { return jsonwebtoken_1["default"].sign(data, jwtConfig_1.secret, { expiresIn: jwtConfig_1.expiresIn }); };
