"use strict";
exports.__esModule = true;
exports.createResetToken = void 0;
/* eslint-disable linebreak-style */
var crypto_1 = require("crypto");
exports.createResetToken = function () { return crypto_1["default"].randomBytes(20).toString('hex'); };
