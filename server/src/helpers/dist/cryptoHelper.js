"use strict";
exports.__esModule = true;
exports.compare = exports.encryptSync = exports.encrypt = void 0;
var bcrypt_1 = require("bcrypt");
var saltRounds = 10;
exports.encrypt = function (data) { return bcrypt_1["default"].hash(data, saltRounds); };
exports.encryptSync = function (data) { return bcrypt_1["default"].hashSync(data, saltRounds); };
exports.compare = function (data, encrypted) { return bcrypt_1["default"].compare(data, encrypted); };
