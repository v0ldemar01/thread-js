import nodemailer from 'nodemailer';
import env from '../env';

const transporter = nodemailer.createTransport({
  service: 'gmail',
  secure: true,
  auth: {
    user: env.email.user,
    pass: env.email.password
  }
});

export const sendResetMail = (emailTo: string, token: string) => {
  const mailOptions = {
    from: env.email.user,
    to: emailTo,
    subject: 'Link to reset password',
    text:
    'You are receiving this because you (or someone else ) have requested'
    + 'the reset of the password for your account.\n\n'
    + 'Please click on the following link, '
    + 'or paste this into your browser to complete the process within one hour of receiving it.\n\n '
    + `http://localhost:3000/reset/${token}\n\n`
    + 'If you did not request this, please ignore this email and your password will remain unchanged.\n'
  };
  return transporter.sendMail(mailOptions);
};

export const sendNotifyMail = (emailTo: string, username: string, isLike: boolean, typeNotifyAction: string) => {
  const mailOptions = {
    from: env.email.user,
    to: emailTo,
    subject: `${typeNotifyAction} action`,
    text:
    `Your ${typeNotifyAction} was ${isLike ? 'liked' : 'disliked'} by ${username}`
  };
  return transporter.sendMail(mailOptions);
};

export const sendSharedPostToEmail = (emailTo: string, username: string, url: string) => {
  const mailOptions = {
    from: env.email.user,
    to: emailTo,
    subject: 'Post action',
    text:
    `User ${username} shares you the post with link:\n\n${url}`
  };
  return transporter.sendMail(mailOptions);
};
