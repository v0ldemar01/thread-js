"use strict";
exports.__esModule = true;
exports.sendSharedPostToEmail = exports.sendNotifyMail = exports.sendResetMail = void 0;
var nodemailer_1 = require("nodemailer");
var env_1 = require("../env");
var transporter = nodemailer_1["default"].createTransport({
    service: 'gmail',
    secure: true,
    auth: {
        user: env_1["default"].email.user,
        pass: env_1["default"].email.password
    }
});
exports.sendResetMail = function (emailTo, token) {
    var mailOptions = {
        from: env_1["default"].email.user,
        to: emailTo,
        subject: 'Link to reset password',
        text: 'You are receiving this because you (or someone else ) have requested'
            + 'the reset of the password for your account.\n\n'
            + 'Please click on the following link, '
            + 'or paste this into your browser to complete the process within one hour of receiving it.\n\n '
            + ("http://localhost:3000/reset/" + token + "\n\n")
            + 'If you did not request this, please ignore this email and your password will remain unchanged.\n'
    };
    return transporter.sendMail(mailOptions);
};
exports.sendNotifyMail = function (emailTo, username, isLike, typeNotifyAction) {
    var mailOptions = {
        from: env_1["default"].email.user,
        to: emailTo,
        subject: typeNotifyAction + " action",
        text: "Your " + typeNotifyAction + " was " + (isLike ? 'liked' : 'disliked') + " by " + username
    };
    return transporter.sendMail(mailOptions);
};
exports.sendSharedPostToEmail = function (emailTo, username, url) {
    var mailOptions = {
        from: env_1["default"].email.user,
        to: emailTo,
        subject: 'Post action',
        text: "User " + username + " shares you the post with link:\n\n" + url
    };
    return transporter.sendMail(mailOptions);
};
